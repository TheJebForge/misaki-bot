package com.thejebforge.misaki.web;

import com.thejebforge.misaki.web.slots.SlotsMainView;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouterLink;

@PageTitle("Index of Misaki")
@Route("/")
public class RootRoute extends VerticalLayout {
    public RootRoute() {
        setAlignItems(Alignment.CENTER);
        setJustifyContentMode(JustifyContentMode.CENTER);
        setHeightFull();

        add(new Label("There's currently nothing here"));
        add(new Label("You meant to go to /slots?"));
        add(new RouterLink("Slots", SlotsMainView.class));
    }
}
