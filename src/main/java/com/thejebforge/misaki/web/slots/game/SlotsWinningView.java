package com.thejebforge.misaki.web.slots.game;

import com.thejebforge.misaki.bot.localisation.BaseLocalisation;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public class SlotsWinningView extends VerticalLayout {
    public SlotsWinningView(BaseLocalisation trn) {
        addClassName("roundedBox");

        setAlignItems(Alignment.START);
        setJustifyContentMode(JustifyContentMode.CENTER);
        setPadding(true);


        Span table = new Span();
        table.setClassName("winningTable");

        String content = trn.formatKey("slots.table.desc");
        content = content.replaceAll("\n", "<br>");
        content = content.replaceAll("<:KEKW:704752163485646878>", "<img class=\"smallIcon\" src=\"images/kek.png\"></img>");
        content = content.replaceAll("<:USALOV:570996636243853362>", "<img class=\"smallIcon\" src=\"images/usalov.png\"></img>");
        content = content.replaceAll("<:zeza_pokerface:794579204263116820>", "<img class=\"smallIcon\" src=\"images/zezapoker.png\"></img>");
        content = content.replaceAll("<:ZEZa_OY:794606274820964362>", "<img class=\"smallIcon\" src=\"images/zezagnome.png\"></img>");
        content = content.replaceAll("<:fleme:647828664146198539>", "<img class=\"smallIcon\" src=\"images/fleme.png\"></img>");
        content = content.replaceAll("<:n_SUPREME:811624243426230302>", "<img class=\"smallIcon\" src=\"images/supreme.png\"></img>");

        table.getElement().setProperty("innerHTML", content);

        add(table);
    }
}
