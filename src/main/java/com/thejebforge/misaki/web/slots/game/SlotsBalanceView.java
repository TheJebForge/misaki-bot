package com.thejebforge.misaki.web.slots.game;

import com.thejebforge.misaki.bot.localisation.BaseLocalisation;
import com.thejebforge.misaki.bot.types.VoiceProfile;
import com.thejebforge.misaki.dao.GuildData;
import com.thejebforge.misaki.dao.SlotsWebLogin;
import com.thejebforge.misaki.runtime.MainBotProcess;
import com.thejebforge.misaki.web.slots.SlotsGameView;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;

public class SlotsBalanceView extends VerticalLayout {
    private SlotsGameView parent;
    private SlotsWebLogin login;

    private NumberField bet;
    private TextField balance;
    private BaseLocalisation trn;

    public SlotsBalanceView(SlotsGameView parent, SlotsWebLogin login) {
        this.parent = parent;
        this.login = login;
        trn = parent.trn;



        addClassName("roundedBox");

        bet = new NumberField(trn.formatKey("slots.text.bet.key"), 5.0, event -> bet.setValue(Math.floor(Math.min(500, Math.max(5, event.getValue())))));
        bet.setStep(5);
        bet.setMin(5);
        bet.setMax(500);
        bet.setValue(Math.min(500, Math.max(5, (double) login.getPreviousBet())));
        bet.setHasControls(true);
        bet.setWidthFull();
        add(bet);

        balance = new TextField(trn.formatKey("slots.text.balance.key"));
        balance.setReadOnly(true);
        balance.setWidthFull();
        add(balance);

        setJustifyContentMode(JustifyContentMode.CENTER);
        setAlignItems(Alignment.CENTER);

        update();
    }

    public void update() {
        VoiceProfile vp = MainBotProcess.vp.getVoiceProfile(login.getUserId(), login.getGuildId());
        if(vp != null) {
            balance.setValue(trn.formatKey("slots.text.balance.value", Long.toString(vp.getVoicepoints())));
        } else {
            balance.setValue(trn.formatKey("slots.text.balance.value", Long.toString(0)));
        }
    }

    public int getBet() {
        return bet.getValue().intValue();
    }

    public void setBetEnabled(boolean state) {
        bet.setEnabled(state);
    }
}
