package com.thejebforge.misaki.web.slots;

import com.thejebforge.misaki.bot.manager.DatabaseManager;
import com.thejebforge.misaki.dao.SlotsWebLogin;
import com.thejebforge.misaki.runtime.MainBotProcess;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.InitialPageSettings;
import com.vaadin.flow.server.PageConfigurator;
import com.vaadin.flow.server.VaadinService;

import javax.servlet.http.Cookie;
import java.util.HashMap;

@PageTitle("Slots Game")
@Route("slots")
@CssImport("./styles/biggerh1.css")
public class SlotsMainView extends VerticalLayout implements PageConfigurator {
    private DatabaseManager db;

    public SlotsMainView() {
        db = MainBotProcess.db;

        setHeightFull();
        setId("mainView");
        setClassName("mainLayout");
        setAlignItems(Alignment.CENTER);
        setJustifyContentMode(JustifyContentMode.CENTER);
        setPadding(false);

        SlotsWebLogin login = retrieveLogin();

        if(login == null)
            buildLoginLayout();
        else
            buildGameLayout(login);
    }

    private SlotsWebLogin retrieveLogin() {
        Cookie[] cookies = VaadinService.getCurrentRequest().getCookies();

        if(cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("auth-cookie")) {
                    String value = cookie.getValue();
                    return db.getLoginFromCookie(value);
                }
            }
        }

        return null;
    }

    private void buildLoginLayout() {
        add(new SlotsLoginView());
    }

    private void buildGameLayout(SlotsWebLogin login) {
        add(new SlotsGameView(login));
    }

    @Override
    public void configurePage(InitialPageSettings settings) {
        HashMap<String, String> attributes = new HashMap<>();
        attributes.put("rel", "shortcut icon");
        attributes.put("type", "image/png");
        settings.addLink("icons/slots.png", attributes);
    }
}
