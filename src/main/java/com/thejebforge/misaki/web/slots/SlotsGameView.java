package com.thejebforge.misaki.web.slots;

import com.github.appreciated.css.grid.GridLayoutComponent;
import com.github.appreciated.css.grid.sizes.Flex;
import com.github.appreciated.css.grid.sizes.Length;
import com.github.appreciated.css.grid.sizes.MinMax;
import com.github.appreciated.css.grid.sizes.Repeat;
import com.github.appreciated.layout.FlexibleGridLayout;
import com.github.appreciated.layout.FluentGridLayout;
import com.github.appreciated.layout.GridLayout;
import com.thejebforge.misaki.bot.localisation.BaseLocalisation;
import com.thejebforge.misaki.bot.manager.DatabaseManager;
import com.thejebforge.misaki.dao.GuildData;
import com.thejebforge.misaki.dao.SlotsWebLogin;
import com.thejebforge.misaki.runtime.MainBotProcess;
import com.thejebforge.misaki.web.slots.game.SlotsBalanceView;
import com.thejebforge.misaki.web.slots.game.SlotsReelsView;
import com.thejebforge.misaki.web.slots.game.SlotsTotalView;
import com.thejebforge.misaki.web.slots.game.SlotsWinningView;
import com.vaadin.flow.component.dependency.JavaScript;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public class SlotsGameView extends FluentGridLayout {
    public final DatabaseManager db;

    public final SlotsReelsView reels;
    public final SlotsBalanceView balance;
    public final SlotsTotalView total;

    public final BaseLocalisation trn;
    public long totalChange = 0;

    public SlotsGameView(SlotsWebLogin login) {
        db = MainBotProcess.db;

        GuildData data = db.getGuild(login.getGuildId());
        trn = data.getTranslator();

        login.bumpDeletionTime();
        db.addLogin(login);

        reels = new SlotsReelsView(this, login);
        reels.setSizeFull();

        balance = new SlotsBalanceView(this, login);
        balance.setSizeFull();

        total = new SlotsTotalView(this, login);
        total.setSizeFull();

        //withTemplateRows(new Flex(1), new Flex(1), new Flex(1));
        withTemplateColumns(new MinMax(new Length("375px"), new Flex(1)), new MinMax(new Length("375px"), new Flex(1)), new Flex(1));
        withRowAndColumn(reels, 1, 1, 5, 4);
        withRowAndColumn(balance, 1, 4);
        withRowAndColumn(new SlotsWinningView(trn), 2, 4, 4, 4);
        withRowAndColumn(total, 4, 4);

        withPadding(true);
        withSpacing(true);


        setSizeFull();
    }
}
