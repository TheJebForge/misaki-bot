package com.thejebforge.misaki.web.slots.game;

import com.thejebforge.misaki.dao.SlotsWebLogin;
import com.thejebforge.misaki.runtime.MainBotProcess;
import com.thejebforge.misaki.web.slots.SlotsGameView;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;

public class SlotsTotalView extends VerticalLayout {
    private SlotsGameView parent;
    private SlotsWebLogin login;

    private TextField total;
    private TextField totalLogin;

    public SlotsTotalView(SlotsGameView parent, SlotsWebLogin login) {
        this.parent = parent;
        this.login = login;

        addClassName("roundedBox");

        setJustifyContentMode(JustifyContentMode.CENTER);
        setAlignItems(Alignment.CENTER);

        total = new TextField(parent.trn.formatKey("slots.text.post.key"));
        total.setReadOnly(true);
        total.setWidthFull();
        add(total);

        totalLogin = new TextField(parent.trn.formatKey("slots.text.post.total.key"));
        totalLogin.setReadOnly(true);
        totalLogin.setWidthFull();
        add(totalLogin);

        update();
    }

    public void update() {
        SlotsWebLogin lg = MainBotProcess.db.getLoginFromKey(login.getKey());

        total.setValue(parent.totalChange < 0 ? parent.trn.formatKey("slots.text.post.lost.value", Long.toString(Math.abs(parent.totalChange))) : parent.trn.formatKey("slots.text.post.won.value", Long.toString(parent.totalChange)));
        totalLogin.setValue(lg.getTotal() < 0 ? parent.trn.formatKey("slots.text.post.lost.value", Long.toString(Math.abs(lg.getTotal()))) : parent.trn.formatKey("slots.text.post.won.value", Long.toString(lg.getTotal())));
    }
}
