package com.thejebforge.misaki.web.slots;

import com.thejebforge.misaki.bot.manager.DatabaseManager;
import com.thejebforge.misaki.bot.utils.KeyGen;
import com.thejebforge.misaki.dao.SlotsWebLogin;
import com.thejebforge.misaki.runtime.MainBotProcess;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Page;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.server.VaadinService;

import javax.servlet.http.Cookie;

public class SlotsLoginView extends VerticalLayout {
    private DatabaseManager db;

    private PasswordField keyField;
    private Span errorMessageLabel;

    public SlotsLoginView() {
        db = MainBotProcess.db;

        setMargin(false);
        setPadding(false);
        setClassName("loginLayoutMargin roundedBox");
        setWidth(350, Unit.PIXELS);

        setAlignItems(Alignment.CENTER);


        Label title = new Label("Slots");
        H1 h1Title = new H1(title);
        add(h1Title);


        HorizontalLayout loginLine = new HorizontalLayout();
        loginLine.setJustifyContentMode(JustifyContentMode.END);

        errorMessageLabel = new Span("");
        errorMessageLabel.setClassName("errorMes");
        errorMessageLabel.setVisible(false);
        add(errorMessageLabel);

        keyField = new PasswordField("Auth Key");
        keyField.addKeyDownListener(Key.ENTER, keyDownEvent -> authButton(null));
        loginLine.add(keyField);
        add(loginLine);

        Button loginButton = new Button("Auth", this::authButton);
        add(loginButton);
    }

    private void authButton(ClickEvent<Button> buttonClickEvent) {
        String key = keyField.getValue();

        SlotsWebLogin login = db.getLoginFromKey(key);

        if(login != null) {
            if(login.getCookie() == null) {
                String cookieValue = KeyGen.generateUniqueCookie();

                Cookie cookie = new Cookie("auth-cookie", cookieValue);
                cookie.setMaxAge(60 * 60 * 24 * 365);
                cookie.setPath(VaadinService.getCurrentRequest().getContextPath());

                VaadinService.getCurrentResponse().addCookie(cookie);

                login.bumpDeletionTime();
                login.setCookie(cookieValue);

                db.addLogin(login);

                UI.getCurrent().getPage().reload();
            } else {
                errorMessageLabel.setText("Key was already used");
                errorMessageLabel.setVisible(true);
            }
        } else {
            errorMessageLabel.setText("Invalid auth key");
            errorMessageLabel.setVisible(true);
        }
    }
}
