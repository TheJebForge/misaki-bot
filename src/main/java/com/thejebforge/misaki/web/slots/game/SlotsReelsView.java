package com.thejebforge.misaki.web.slots.game;

import com.google.common.collect.ImmutableMap;
import com.thejebforge.misaki.bot.modules.TextSlotsModule;
import com.thejebforge.misaki.bot.types.VoiceProfile;
import com.thejebforge.misaki.dao.SlotsWebLogin;
import com.thejebforge.misaki.runtime.MainBotProcess;
import com.thejebforge.misaki.slots.SlotOutcome;
import com.thejebforge.misaki.slots.SlotRoller;
import com.thejebforge.misaki.slots.SlotVariant;
import com.thejebforge.misaki.web.slots.SlotsGameView;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ClientCallable;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.JavaScript;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Main;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import lombok.SneakyThrows;
import org.apache.commons.io.IOUtils;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@JavaScript("./js/slots.js")
@JavaScript("./js/slotmachine.min.js")
@JavaScript("https://code.jquery.com/jquery-3.6.0.min.js")
public class SlotsReelsView extends VerticalLayout {
    private final SlotsGameView parent;
    private final SlotsWebLogin login;

    private final Map<String, String> charToInt;
    private SlotRoller roller;
    private SlotOutcome previous = null;
    private SlotOutcome current = null;

    private Div winningModal;
    private Button spin;

    private int previousBet;

    @SneakyThrows
    public SlotsReelsView(SlotsGameView parent, SlotsWebLogin login) {
        this.parent = parent;
        this.login = login;

        roller = new SlotRoller();

        charToInt = ImmutableMap.<String, String>builder()
                .put("C", "1")
                .put("1", "3")
                .put("2", "5")
                .put("3", "4")
                .put("7", "0")
                .put("D", "2")
                .build();

        setClassName("roundedBox");
        setId("reelsView");

        InputStream html = getClass().getClassLoader().getResourceAsStream("META-INF/resources/frontend/web/slotsreel.html");
        assert html != null;
        String reelCode = IOUtils.toString(html, StandardCharsets.UTF_8);

        HorizontalLayout reels = new HorizontalLayout();
        reels.setSizeFull();
        reels.setAlignItems(Alignment.CENTER);
        reels.setSpacing(true);
        reels.setJustifyContentMode(JustifyContentMode.CENTER);

        reels.setClassName("modalContainer");

        for(int i = 0; i < 3; i++){
            Div reel = new Div();

            reel.setClassName("reel");
            reel.setId("reel" + i);
            reel.getElement().setProperty("innerHTML", reelCode);

            reels.add(reel);
        }

        add(reels);

        Div modal = new Div();
        modal.setClassName("modalItem");

        winningModal = new Div();
        winningModal.setId("winningModal");
        winningModal.setClassName("roundedBox modalContent");
        getElement().executeJs("$('#winningModal').fadeOut(0);");
        modal.add(winningModal);

        reels.add(modal);


        spin = new Button(parent.trn.formatKey("slots.text.spin"), this::spin);
        spin.setWidthFull();
        spin.addClickShortcut(Key.SPACE);
        add(spin);

        getElement().executeJs("setupReels();");
    }

    private void spin(ClickEvent<Button> buttonClickEvent) {
        if(spin.getElement().isEnabled()) {
            if (!parent.db.loginExists(login.getKey())) {
                UI.getCurrent().getPage().reload();
                return;
            }

            VoiceProfile vp = MainBotProcess.vp.getVoiceProfile(login.getUserId(), login.getGuildId());
            int bet = parent.balance.getBet();

            if (vp.getVoicepoints() >= bet) {
                spin.setEnabled(false);
                parent.balance.setBetEnabled(false);
                getElement().executeJs("$('#winningModal').fadeOut(200);");

                if(bet != previousBet) previous = null;

                SlotsWebLogin lg = MainBotProcess.db.getLoginFromKey(login.getKey());
                if(lg != null) {
                    lg.setPreviousBet(bet);
                    MainBotProcess.db.addLogin(lg);
                }


                SlotOutcome outcome = roller.roll(bet, previous, (int) (login.getUserId() % 100000));

                List<String> results = new ArrayList<>();
                for (SlotVariant variant : outcome.getReels()) {
                    results.add(charToInt.get(variant.getCharacter()));
                }

                current = outcome;
                previousBet = bet;
                getElement().executeJs("spin([" + String.join(", ", results) + "]);");
            } else {
                getElement().executeJs("$('#winningModal').fadeIn(200);");
                winningModal.setText(parent.trn.formatKey("slots.text.reels.no_money"));
            }
        }
    }

    @ClientCallable
    private void spinComplete() {
        spin.setEnabled(true);
        parent.balance.setBetEnabled(true);

        if(current != null) {
            if (current.getEarned() > 0) {
//            winningModal.setClassName("roundedBox modalContent modalshow");
                getElement().executeJs("$('#winningModal').fadeIn(200);");
                String text = current.getStreak() > 0 ?
                        current.getStreak() == 2 ?
                                parent.trn.formatKey("slots.text.reward.maxstreak.value", Integer.toString(current.getEarned()), Integer.toString(current.getStreak() + 1))
                                : parent.trn.formatKey("slots.text.reward.streak.value", Integer.toString(current.getEarned()), Integer.toString(current.getStreak() + 1))
                        : parent.trn.formatKey("slots.text.reward.value", Integer.toString(current.getEarned()));

                text = text.replaceAll("\n", "<br>");
                winningModal.getElement().setProperty("innerHTML", text);
            }

            VoiceProfile botVP = MainBotProcess.vp.getVoiceProfile(MainBotProcess.gateway.getSelfId().asLong(), TextSlotsModule.botGuildId);

            int userBalanceChange = 0;
            int botBalanceChange = 0;

            userBalanceChange -= current.getPaid();
            botBalanceChange += current.getPaid();

            userBalanceChange += current.getEarned();
            botBalanceChange -= current.getEarned();

            if (botVP.getVoicepoints() + botBalanceChange < 5000)
                botBalanceChange += Math.min(5000 + current.getEarned(), 5000);

            parent.totalChange += userBalanceChange;

            SlotsWebLogin lg = MainBotProcess.db.getLoginFromKey(login.getKey());
            if(lg != null) {
                lg.setTotal(lg.getTotal() + userBalanceChange);
                MainBotProcess.db.addLogin(lg);
            }

            if (userBalanceChange != 0)
                MainBotProcess.vp.voicePointAdd(login.getUserId(), login.getGuildId(), userBalanceChange);
            if (botBalanceChange != 0)
                MainBotProcess.vp.voicePointAdd(MainBotProcess.gateway.getSelfId().asLong(), TextSlotsModule.botGuildId, botBalanceChange);

            parent.balance.update();
            parent.total.update();

            previous = current;
            current = null;
        }
    }
}
