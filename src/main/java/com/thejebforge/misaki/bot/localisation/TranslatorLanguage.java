package com.thejebforge.misaki.bot.localisation;

import java.util.ArrayList;
import java.util.List;

public enum TranslatorLanguage {
    English,
    Russian,
    Owo;

    public static List<String> getAllLanguages(){
        List<String> langs = new ArrayList<>();

        for (TranslatorLanguage lang : TranslatorLanguage.values()){
            langs.add(lang.name());
        }

        return langs;
    }
}
