package com.thejebforge.misaki.bot.localisation;

public class OwoLocalisation extends EnglishLocalisation {

    private String translateString(String input) {
        if(input != null)
            return input.replaceAll("[lr]", "w")
                        .replaceAll("[LR]", "W")
                        .replaceAll("n([aeiou])","ny$1")
                        .replaceAll("N([aeiou])","Ny$1")
                        .replaceAll("N([AEIOU])","NY$1")
                        .replaceAll("ove", "uv")
                        .replaceAll("OVE", "UV")
                        .replaceAll("nd(?= |$)", "ndo")
                        .replaceAll("z(?:\")", "z~")
                        .replaceAll("nd(?= |$)", "ndo")
                        .replaceAll("\\?", " uwu?")
                        .replaceAll("the", "teh")
                        .replaceAll("this", "dis")
                        .replaceAll("with", "wif")
                        .replaceAll("youw", "ur")
                        .replaceAll("you", "u")
                        .replaceAll("(\",|;)", "~")
                        .replaceAll(":\\)", ":3")
                        .replaceAll(":O", "OwO")
                        .replaceAll(":o", "owo")
                        .replaceAll(":D", "UwU")
                        .replaceAll("XD", "X3")
                        .replaceAll("xD", "x3");
        return null;
    }

    @Override
    public String formatKey(String key, String... args) {
        String result = translateString(translationMap.get(key));

        if(result != null) {
            for (int i = 0; i < args.length; i++) {
                result = result.replaceAll("\\["+i+"]", args[i]);
            }

            return result;
        } else return key;
    }

    @Override
    public boolean translatesCommands() {
        return true;
    }

    @Override
    public TranslatorLanguage getLanguageEnum() {
        return TranslatorLanguage.Owo;
    }
}
