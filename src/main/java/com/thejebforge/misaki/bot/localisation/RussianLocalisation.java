package com.thejebforge.misaki.bot.localisation;

import java.util.HashMap;
import java.util.Map;

public class RussianLocalisation extends BaseLocalisation {
   protected static final Map<String, String> translationMap = new HashMap<String, String>(){{
        put("value.set.title", "Значение поставлено");
        put("value.set.desc", "Значение параметра было поменено");
        put("testmodule.name", "Тестовый модуль");
        put("roll.single_dice_roll", "Кость выкинула [0]");
        put("roll.results.value", "Итого все кости выкинули [0]");
        put("roll.results.title", "Результаты костей");
        put("roll.results.field", "Сумма");
        put("roll.multiple_dice_roll", "Кость №[0] выкинула [1]");
        put("randommodule.name", "Рандомные команды");
        put("modules.list.show.title", "Доступные модули");
        put("language.updated.title", "Язык обновлен");
        put("language.updated.desc", "Скорее всего, вы прочтете это сообщение уже на другом языке");
        put("language.show.title", "Выбранный язык");
        put("language.show.field", "Язык");
        put("images.tags.value", "Это все теги, которые я знаю");
        put("images.tags.title", "Тэги базы картинок");
        put("images.tags.field", "Все извесные теги");
        put("images.stats.title", "Статистика базы картинок");
        put("images.stats.desc", "Количество известных картинок: [0]");
        put("imagemanager.name", "Менеджер картинок");
        put("image.tag.title", "Дай тэги картинке");
        put("image.tag.desc", "Укажи какие тэги подходят этой картинке с помошью [0] <тэги>");
        put("image.search.nothing.title", "Нечего не найдено");
        put("image.search.nothing.field", "Тэги");
        put("image.search.nothing.desc", "Не получилось найти чего-либо с предоставленными тэгами в базе");
        put("image.search.found.title", "[0] картинок найдено");
        put("image.search.found.tags", "Указанные тэги");
        put("image.search.found.random", "Чтобы получить произвольную картинку");
        put("image.search.found.next", "Чтобы увидеть картинки");
        put("image.recent.title", "Последняя картинка");
        put("image.recent.desc", "Это самая последняя картинка, которую я помню");
        put("image.random.tags", "Тэги");
        put("image.next.tags", "Тэги");
        put("image.edited.title", "Картинка изменена удачно");
        put("image.edited.field", "Тэги");
        put("image.edited.desc", "Картинка была изменена удачно");
        put("image.deleted.title", "Картинка удалена удачно");
        put("image.deleted.desc", "Картинка была удалена удачно");
        put("image.delete.confirm.title", "Подтверждение удаления");
        put("image.delete.confirm.field", "Чтобы подтвердить");
        put("image.delete.confirm.desc", "Ты уверен, что ты хочешь удалить эту картинку?");
        put("image.added.title", "Картинка была добавлена удачно");
        put("image.added.field", "Тэги");
        put("image.added.desc", "Картинка была добавлена в базу удачно");
        put("helper.name", "Помошник");
        put("help.commands.show.title", "Команды модулей");
        put("help.commands.morehelp.value", "[0] <команда/страница>");
        put("help.commands.morehelp.field", "Чтобы получить больше помощи");
        put("help.commands.furtherhelp.field", "Чтобы получить больше помощи");
        put("help.commands.furtherhelp.value", "help (команда) (подкоманды...)");
        put("help.command.usage", "Использование");
//        put("help.command.title", "Помощь команды `[0]`");
//        put("help.command.test.desc", "Команда, которая фури");
//        put("help.command.tag.desc", "Команда, которая показывает произвольную картинку и просит предоставить тэги для нее");
//        put("help.command.search.usage", "[0] <тэги>");
//        put("help.command.search.desc", "Команда, которая ищет картинки с придоставленными тэгами в базе");
//        put("help.command.roll.usage", "[0] (количество костей)d<количество сторон>");
//        put("help.command.roll.examples.value", "[0] 1d6 - кидает одну кость с 6 сторонами\n[0] d6 - кидает одну кость с 6 сторонами");
//        put("help.command.roll.examples.field", "Примеры");
//        put("help.command.roll.desc", "Команда, которая кидает кости с помощью предоставленного шаблона, может быть или ###d###, или d###\nЧисла должны быть целыми");
//        put("help.command.rickroll.desc", "Команда, которая рикролит тебя");
//        put("help.command.recent.desc", "Команда, которая показывает самую последнюю картинку");
//        put("help.command.random.usage", "[0] (all)");
//        put("help.command.random.desc", "Команда, которая показывает произвольную картинку из текущего поиска, или если есть 'all' аргумент, показывает произвольную картинку из всей базы");
//        put("help.command.next.desc", "Команда, которая показывает следующую картинку из текущего поиска");
//        put("help.command.modules.usage", "[0] <метод> <аргументы>");
//        put("help.command.modules.methods", "Методы");
//        put("help.command.modules.list", "[0] - показывает все доступные модули для этого гилда");
//        put("help.command.modules.desc", "Команда, которая позволяет просматривать и изменять данные модулей");
//        put("help.command.images.usage", "[0] <информация>");
//        put("help.command.images.info.tags", "[0] - показывает все теги, которые база знает\n[1] - показывает разную информацию о базе картинок");
//        put("help.command.images.info.section", "Доступная информация");
//        put("help.command.images.desc", "Команда, которая показывает разную информацию о базе картинок");
//        put("help.command.help.usage", "[0] <команда>");
//        put("help.command.help.desc", "Команда, которая показывает помощь других команд");
//        put("help.command.guild.usage", "[0] <метод> <аргументы>");
//        put("help.command.guild.methods.title", "Методы");
//        put("help.command.guild.methods.text", "[0] - показывает все включенные модули на текущем гилде с их кодавыми названиями\n[1] - показывает всех участников текущего гилда, которые имеют права изменять настройки гилда в боте\n[2] - показывает язык, который бот использует для этого гилда\n[3] - показывает временную зону, который бот использует для этого гилда\n[4] - показывает всю информацию, которой располагает бот об этом гилде\n[5] <язык> - меняет язык, который бот будет использовать для этого гилда\n[6] <язык> - меняет временную зону, которую бот будет использовать для этого гилда\n[7] <кодовое название> - включает упомянутый модуль в этом гилде\n[8] <кодовое название> - выключает упомянутый модуль в этом гилде\n[9] <упоминания> - добавляет упомянутого участника в менеджеры этого гилда\n[10] <упоминания> - убирает упомянутого участника из менеджеров в текущем гилде");
//        put("help.command.guild.desc", "Команда, которая позволяет просматривать и изменять данные гилда");
//        put("help.command.edit.usage", "[0] <тэги>");
//        put("help.command.edit.desc", "Команда, которая позволяет изменять картинки, изменяет последнюю картинку");
//        put("help.command.delete.desc", "Команда, которая позволяет удалять картинки из базы, удаляет самую последнюю картинку (у команды есть потверждение, ты не удалишь картинку случайно)");
//        put("help.command.debug.usage", "[0] <секция> <метод>");
//        put("help.command.debug.status.section", "Методы секции `status`");
//        put("help.command.debug.status.methods", "[0] - удаляет базу статус сообщений\n[1] <действие> <сообщение> - добавляет статус сообщение в базу\n[2] <действие> <сообщение> - удаляет статус сообщение из базы\n[3] - показывает текущие статус сообщения");
//        put("help.command.debug.images.section", "Методы секции `images`");
//        put("help.command.debug.images.methods", "[0] - удаляет базу картинок\n[1] <путь к файлу> - импортирует картинки в базу из csv файла");
//        put("help.command.debug.guilds.section", "Методы секции `guilds`");
//        put("help.command.debug.guilds.methods", "[0] - удаляет базу гилдов");
//        put("help.command.debug.general.section", "Методы секции `general`");
//        put("help.command.debug.general.methods", "[0] - выходит и убивает бота\n[1] <действие> <сообщение> - ставит статус бота");
//        put("help.command.debug.dailytop.section", "Методы секции `dailytop`");
//        put("help.command.debug.dailytop.methods", "[0] - удаляет базу топов");
//        put("help.command.debug.desc", "Команда, которая позволяет использовать дебаг возможности бота\n**НЕ ИСПОЛЬЗОВАТЬ ЭТУ КОМАНДУ** *(не то, что ты можешь)*");
//        put("help.command.choose.usage", "[0] <слова разделенные пробелом>");
//        put("help.command.choose.desc", "Команда, которая выбирает произвольное слово из предоставленных");
//        put("help.command.add.usage", "[0] <ссылка/тэг> [тэг] [тэг]...");
//        put("help.command.add.desc", "Команда, которая позволяет добавлять картинки в базу картинок, прилажи картинку к сообщению или дай ссылку на картинку как первый аргумент, потом предоставь тэги");
        put("guild.permLevel.show.title", "Уровень Позволения Гилда");
        put("guild.permLevel.show.field", "Уровень");
        put("guild.module.show.title", "Включенные модули на этом гилде");
        put("guild.module.enabled.title", "Модуль включен");
        put("guild.module.enabled.desc", "Модуль '[0]' был включен для этого гилда");
        put("guild.module.disabled.title", "Модуль выключен");
        put("guild.module.disabled.desc", "Модуль с кодовым названием '[0]' был выключен для этого гилда");
        put("guild.managers.show.title", "Менеджеры в этом гилде");
        put("guild.managers.removed.title", "Менеджеры обновлены");
        put("guild.managers.removed.desc", "Упомянутые участники были удалены из менеджеров бота");
        put("guild.managers.added.title", "Менеджеры обновлены");
        put("guild.managers.added.desc", "Упомянутые участники были добавлены как менеджеры бота");
        put("error.wrong.command.too_long.title", "Че то команда длинная как-то");
        put("error.wrong.command.too_long.desc", "Я не буду пробовать понять что ты от меня хочешь");
        put("error.wrong.command.bad_characters.title", "Странные симболы какие-то");
        put("error.wrong.command.bad_characters.desc", "Попробуй что-то нормальное завести");
        put("error.wrong.command.no_command.title", "Че?");
        put("error.wrong.command.no_command.desc", "Ты думал команду написать и не получилось?");
        put("error.value.not_int_characters.title", "В значении есть что-то кроме чисел");
        put("error.value.not_int_characters.desc", "Значение должно быть числом, посмотри [0]");
        put("error.value.not_int.title", "Значение не является челым числом");
        put("error.value.not_int.desc", "Значение должно быть целым числом, посмотри [0]");
        put("error.unknown.subcommand.section.title", "Неизвестная секция");
        put("error.unknown.subcommand.section.desc", "Нету такой секции, посмотри [0]");
        put("error.unknown.subcommand.property.title", "Неизвестный параметр");
        put("error.unknown.subcommand.property.desc", "Нету такого параметра, посмотри [0]");
        put("error.unknown.subcommand.method.title", "Неизвестный метод");
        put("error.unknown.subcommand.method.desc", "Нету такого метода, посмотри [0]");
        put("error.unknown.status.title", "Нету такого статус сообщения");
        put("error.unknown.status.desc", "Нету такого статус сообщения в базе");
        put("error.unknown.module.title", "Модуль не существует");
        put("error.unknown.module.desc", "Нету такого модуля с кодовым названием '[0]'");
        put("error.unknown.language.title", "Не правильный язык");
        put("error.unknown.language.desc", "Укажи правильный язык, бот на данный момент знает текущие языки: [0]");
        put("error.unknown.command.title", "Неизвесная команда");
        put("error.unknown.command.help.title", "Нету такой команды");
        put("error.unknown.command.help.desc", "Помощь для предоставленной команды не существует");
        put("error.unknown.command.field", "Команда");
        put("error.unknown.command.desc", "Не один модуль не имплементирует эту команду");
        put("error.roll.too_many.title", "Не могу кидать столько костей одновременно");
        put("error.roll.too_many.desc", "Могу только кидать 50 костей одновременно");
        put("error.roll.bad_pattern.title", "Неправильный шаблон");
        put("error.roll.bad_pattern.desc", "Надо указывать правильный шаблон типо #d#, посмотри [0]");
        put("error.roll.bad_number.title", "Неправильные числа");
        put("error.roll.bad_number.desc", "Эти числа или слишком большые, или не правильные, посмотри [0]");
        put("error.random.bad_argument.title", "Произвольное что?");
        put("error.random.bad_argument.desc", "Не понимаю что ты имеешь ввиду, смотри [0]");
        put("error.permission.edit.not_owner.title", "Доступ запрещен");
        put("error.permission.edit.not_owner.desc", "Только создатель бота могут изменять это");
        put("error.permission.edit.not_manager.title", "Доступ запрещен");
        put("error.permission.edit.not_manager.desc", "Только менеджеры могут изменять это");
        put("error.permission.debug.not_owner.title", "Доступ запрещен");
        put("error.permission.debug.not_owner.desc", "Только создатель бота может использовать дебаг фичи бота");
        put("error.module.permanent.disable.title", "Модуль не может быть выключен");
        put("error.module.permanent.disable.desc", "Модуль с кодовым названием '[0]' не хочет чтобы его выключали");
        put("error.module.already.enabled.title", "Гилд уже использует этот модуль");
        put("error.module.already.enabled.desc", "Модуль с кодовым названием '[0]' уже включен для этого гилда");
        put("error.module.already.disabled.title", "У гилда нету такого модуля");
        put("error.module.already.disabled.desc", "Модуль с кодовым названием '[0]' уже выключен для этого гилда");
        put("error.missing.value.set.title", "Укажи значение");
        put("error.missing.value.set.desc", "Не могу поменять значение чего-либо без значения, посмотри [0]");
        put("error.missing.help.title", "Нету помощи для этой команды");
        put("error.missing.help.desc", "У этой команды нету помощи");
        put("error.missing.tags.image_search.title", "Укажи тэги");
        put("error.missing.tags.image_search.desc", "Мне нужны тэги чтобы найти картинки, смотри [0]");
        put("error.missing.tags.image_edit.title", "Укажи тэги");
        put("error.missing.tags.image_edit.desc", "Мне нужны тэги чтобы изменить картинку, смотри [0]");
        put("error.missing.tags.image_add.title", "Укажи тэги");
        put("error.missing.tags.image_add.desc", "Мне нужны тэги чтобы добавить картинку в базу, смотри [0]");
        put("error.missing.subcommand.section.title", "Секция не была указана");
        put("error.missing.subcommand.section.desc", "У команды есть больше функционала чем ты думаешь, посмотри [0] чтобы узнать побольше");
        put("error.missing.subcommand.property.title", "Параметр не был указан");
        put("error.missing.subcommand.property.desc", "У команды есть больше функционала чем ты думаешь, посмотри [0] чтобы узнать побольше");
        put("error.missing.subcommand.method.title", "Метод не был указан");
        put("error.missing.subcommand.method.desc", "У команды есть больше функционала чем ты думаешь, посмотри [0] чтобы узнать побольше");
        put("error.missing.status.debug.title", "Нету статус сообщения");
        put("error.missing.status.debug.desc", "Укажи статус сообщение для статус дебага, смотри [0]");
        put("error.missing.search.random.title", "Поищи что-нибудь сначала");
        put("error.missing.search.random.desc", "Не могу показать произвольную картинку без уже начатого поиска, смотри [0]");
        put("error.missing.search.next.title", "Поищи что-нибудь сначала");
        put("error.missing.search.next.desc", "Не могу показать следующую картинку без уже начатого поиска, смотри [0]");
        put("error.missing.roll.pattern.title", "Шаблон не предоставлен");
        put("error.missing.roll.pattern.desc", "Мне нужен шаблон чтобы кинуть кости");
        put("error.missing.recent.image.title", "Нету последней картинки");
        put("error.missing.recent.image.desc", "Картинки не появлялись в последнее время");
        put("error.missing.random.title", "Нету картинок в папке рандома");
        put("error.missing.random.desc", "Картинки не были найдены в папке рандома");
        put("error.missing.page.too_low.title", "Нету такой страницы");
        put("error.missing.page.too_low.desc", "Не могут же страницы уходить ниже 1, так ведь?...");
        put("error.missing.page.too_high.title", "Нету такой страницы");
        put("error.missing.page.too_high.desc", "У гилда нету столько модулей чтобы достигнуть этот номер страницы");
        put("error.missing.module.enable.title", "Укажи какой модуль включить");
        put("error.missing.module.enable.desc", "Не могу включить то, чего я не знаю");
        put("error.missing.module.disable.title", "Укажи какой модуль выключить");
        put("error.missing.module.disable.desc", "Не могу выключить то, чего я не знаю");
        put("error.missing.mention.remove.title", "Упомяни кого-нибудь");
        put("error.missing.mention.remove.desc", "Не могу удалить кого-либо без упоминания, посмотри [0]");
        put("error.missing.mention.add.title", "Упомяни кого-нибудь");
        put("error.missing.mention.add.desc", "Не могу добавить кого-либо без упоминания, посмотри [0]");
        put("error.missing.language.title", "Укажи язык");
        put("error.missing.language.desc", "Укажи правильный язык, бот на данный момент знает текущие языки: [0]");
        put("error.missing.image.tag.title", "Не одна картинка не была выбрана для тэгирования");
        put("error.missing.image.tag.desc", "Напиши `[0]` сначала, чтобы выбрать картинку для тэгирования");
        put("error.missing.image.add.title", "Укажи картинку");
        put("error.missing.image.add.desc", "Добавь картинку к сообщению или дай ссылку как первый аргумент, смотри [0]");
        put("error.missing.choices.title", "Нечего выбирать");
        put("error.missing.choices.desc", "Надо же из чего-то выбирать, да?");
        put("error.missing.activity.message.title", "Предоставь какое-то сообщение");
        put("error.missing.activity.message.desc", "Сообщение не может быть пустым");
        put("error.invalid.activity.title", "Неправильное действие");
        put("error.invalid.activity.desc", "Действие может быть одним из последующих: [0]");
        put("error.images.random.find_fail.title", "Не удалось найти картинку в папке рандома");
        put("error.images.random.find_fail.desc", "Словила эксепшен пока пыталась достать картинку из папки рандома");
        put("error.images.import.fail.title", "Не удалось импортировать файл");
        put("error.images.import.fail.desc", "Поймала эксепшен пока пыталась обработать файл");
        put("error.images.file.missing.title", "Путь не был указан");
        put("error.images.file.missing.desc", "Укажи путь для импорта csv файла, смотри [0]");
        put("error.images.file.doesnt_exist.title", "Указанный файл не найден");
        put("error.images.file.doesnt_exist.desc", "Нету такого файла в папке бота");
        put("error.images.download.failed.title", "Не получилось добавить картинку");
        put("error.images.download.failed.desc", "Словила эксепшен пока пыталась скачать картинку, попробуй с чем-либо другим");
        put("error.images.delete.failed.title", "Картинка удалена удачно");
        put("error.images.delete.failed.desc", "Картинка была удалина удачно");
        put("error.images.add.failed.title", "Не получилось добавить картинку");
        put("error.images.add.failed.desc", "Словила эксепшен пока пыталась добавить картинку");
        put("error.guild.not_trusted.title", "Я не доверяю этому гилду настолько сильно");
        put("error.guild.not_trusted.desc", "Уровень позволения слишком мал, чтобы включить этот модуль с кодовым названием '[0]'");
        put("error.exception.command.title", "Ай! Словила эксепшен");
        put("error.exception.command.desc", "Поймала эксепшен пока пыталась обработать команду, ты че там делаешь? А?");
        put("error.empty.search.title", "Нету нечего в текущем поиске");
        put("error.empty.search.desc", "Ты удалил все картинки из текущего поиска?");
        put("error.empty.imagedb.title", "Добавь что-нибудь сначала");
        put("error.empty.imagedb.desc", "Нету картинок в базе, смотри [0]");
        put("debugmodule.name", "Дебаг модуль");
        put("debug.status.updated.title", "Статус обновлен");
        put("debug.status.updated.desc", "Он должен был обновится в панеле пользователей");
        put("debug.status.removed.title", "Статус удален");
        put("debug.status.removed.desc", "Этот статус больше никогда не покажится");
        put("debug.status.list.title", "Все статус сообщения в базе");
        put("debug.status.dropped.title", "Статус сообщения стерты");
        put("debug.status.dropped.desc", "База статусов теперь чистенькая");
        put("debug.status.drop.confirm.title", "Подтверждение удаления");
        put("debug.status.drop.confirm.field", "Чтобы подтвердить");
        put("debug.status.drop.confirm.desc", "Ты уверен, что ты хочешь удалить все статусы из базы статусов?");
        put("debug.status.added.title", "Статус сообщение добавлено");
        put("debug.status.added.message", "Сообщение");
        put("debug.status.added.desc", "Оно может показаться в ближайшем будущем");
        put("debug.status.added.activity", "Действие");
        put("debug.images.import.title", "[0] картинок были импортированы");
        put("debug.images.import.desc", "Теперь вопросик, а это вообще правильные файлы?...");
        put("debug.images.dropped.title", "Картинки стерты");
        put("debug.images.dropped.desc", "База картинок теперь чистенькая");
        put("debug.images.drop.confirm.title", "Подтверждение удаления");
        put("debug.images.drop.confirm.field", "Чтобы подтвердить");
        put("debug.images.drop.confirm.desc", "Ты уверен, что ты хочешь удалить все картинки из базы картинок?");
        put("debug.guilds.dropped.title", "Гилды стерты");
        put("debug.guilds.dropped.desc", "База гилдов теперь чистенькая");
        put("debug.guilds.drop.confirm.title", "Подтверждение удаления");
        put("debug.guilds.drop.confirm.field", "Чтобы подтвердить");
        put("debug.guilds.drop.confirm.desc", "Ты уверен, что ты хочешь удалить все гилды из базы гилдов?");
        put("debug.general.kill.title", "Досвидания");
        put("debug.general.kill.desc", "Процесс убит");
        put("configurator.name", "Настройщик");
        put("choose.title", "Я выбераю...");
        put("choose.field", "Выбор");
        put("choose.desc", "По моему хороший выбор");

        put("error.missing.file.path.title", "Путь к файлу не указан");
        put("error.missing.file.path.desc", "Посмотри помощь и укажи путь к файлу");

        put("error.unknown.status.activity.title", "Неправильное действие");
        put("error.unknown.status.activity.desc", "Действие может быть одним из следующих: [0]");

        put("error.missing.module.title", "Укажи кодовое название модуля");
        put("error.missing.module.desc", "Следующую вещь которую тебе надо было написать это кодовое название модуля");

        put("error.missing.status.activity.title", "Действие не указано");
        put("error.missing.status.activity.desc", "Укажи действие, посмотри помощь по команде");

        put("error.missing.status.title", "Статус не указан");
        put("error.missing.status.desc", "Укажи статус, посмотри помощь по команде");

        put("dailytop.name", "Войс Топ Модуль");

        put("dailytop.voice_level.title", "Локальный топ");
        put("dailytop.voice_level.desc", "Голосовая система");

        put("dailytop.voice_points.title", "Локальный топ");
        put("dailytop.voice_points.desc", "ВойсПоинты");

        put("dailytop.voice_time.title", "Локальный топ");
        put("dailytop.voice_time.desc", "Общее время");

        put("dailytop.top.subtext", "Обновлено в [0]");

        put("dailytop.voice_points.clear.confirm.title", "Подтверждение убирания");
        put("dailytop.voice_points.clear.confirm.desc", "Ты уверен, что ты хочешь убрать все топы войспоинтов из этого канала?");
        put("dailytop.voice_points.clear.confirm.field", "Чтобы подтвердить");

        put("dailytop.voice_points.cleared.title", "ВойсПоинт топы убраны");
        put("dailytop.voice_points.cleared.desc", "Все войспоинт топы были убраны из этого канала");

        put("dailytop.voice_points.field", "#[0] [1]");
        put("dailytop.voice_points.value", "[0] VP");

        put("dailytop.voice_level.field", "#[0] [1]");
        put("dailytop.voice_level.value", "Уровень [0] ([1] XP) ([2])");

        put("dailytop.voice_level.clear.confirm.title", "Подтверждение убирания");
        put("dailytop.voice_level.clear.confirm.desc", "Ты уверен, что ты хочешь убрать все топы уровней из этого канала?");
        put("dailytop.voice_level.clear.confirm.field", "Чтобы подтвердить");

        put("dailytop.voice_level.cleared.title", "Топы уровней убраны");
        put("dailytop.voice_level.cleared.desc", "Все топы уровней были убраны из этого канала");

        put("dailytop.voice_time.field", "#[0] [1]");
        put("dailytop.voice_time.value", "Общее время проведённое на сервере [0] Дней [1]");

        put("dailytop.voice_time.clear.confirm.title", "Подтверждение убирания");
        put("dailytop.voice_time.clear.confirm.desc", "Ты уверен, что ты хочешь убрать все топы времени из этого канала?");
        put("dailytop.voice_time.clear.confirm.field", "Чтобы подтвердить");

        put("dailytop.voice_time.cleared.title", "Топы времени убраны");
        put("dailytop.voice_time.cleared.desc", "Все топы времени были убраны из этого канала");

        put("dailytop.scheduled.title", "Планируем обновление топов");
        put("dailytop.scheduled.desc", "Все топы должны скоро обновится");

        put("error.dailytop.too_soon.title", "Обновление только что было");
        put("error.dailytop.too_soon.desc", "Подожди немного и попробуй снова");

        put("dailytop.scheduling.title", "Ждем обновление");
        put("dailytop.scheduling.desc", "Обновление топа скоро произойдет");

        put("debug.dailytop.dropped.title", "Топы стерты");
        put("debug.dailytop.dropped.desc", "База топов теперь чистенькая");

        put("debug.dailytop.drop.confirm.title", "Подтверждение удаления");
        put("debug.dailytop.drop.confirm.desc", "Ты уверен, что ты хочешь удалить все топы из базы топов?");
        put("debug.dailytop.drop.confirm.field", "Чтобы подтвердить");

        put("error.permission.use.not_manager.title", "Доступ запрещен");
        put("error.permission.use.not_manager.desc", "Только менеджеры могут использовать эту команду");

        put("error.permission.use.not_owner.title", "Доступ запрещен");
        put("error.permission.use.not_owner.desc", "Только владелец этого бота может использовать эту команду");

//        put("help.command.dailytop.desc", "Команда, которая управляет топами");
//        put("help.command.dailytop.usage", "[0] <метод>");
//        put("help.command.dailytop.methods", "Методы");
//        put("help.command.dailytop.methods.text", "[0] - обновляет все топы");
//        put("help.command.dailytop.voice_points.methods", "Методы `points`");
//        put("help.command.dailytop.voice_points.methods.text", "[0] - создает новый топ войспоинтов\n[1] - чистит все топы войспоинтов из этого канала");
//        put("help.command.dailytop.voice_time.methods", "Методы `time`");
//        put("help.command.dailytop.voice_time.methods.text", "[0] - создает новый топ времени\n[1] - чистит все топы времени из этого канала");
//        put("help.command.dailytop.voice_level.methods", "Методы `levels`");
//        put("help.command.dailytop.voice_level.methods.text", "[0] - создает новый топ уровней\n[1] - чистит все топы уровней из этого канала");

        put("timezone.show.title", "Выбранная временная зона");
        put("timezone.show.field", "ID временной зоны");

        put("error.unknown.timezone.title", "Неправильный ID временной зоны");
        put("error.unknown.timezone.desc", "Укажи правильную временную зону, посмотри этот список: [0]");

        put("error.missing.timezone.title", "Укажи ID временной зоны");
        put("error.missing.timezone.desc", "Укажи правильную временную зону, посмотри этот список: [0]");

        put("timezone.updated.title", "Временная зона обновлена");
        put("timezone.updated.desc", "Временная зона была установлена на этот ID: [0]");



        put("help.command.guild.title", "Помощь по команде `guild`");
        put("help.command.guild.desc", "Команда, которая позволяет просматиривать и изменять данные гилда");
        put("help.command.guild.usage", "guild <метод> <аргументы>");
        put("help.command.guild.sub", "Методы");
        put("help.command.guild.section", "Секция `guild`");
        put("help.command.guild.shortDesc", "показывает и изменяет данные гилда");

        put("help.command.guild.enable.title", "Помощь по подкоманде `enable`");
        put("help.command.guild.enable.desc", "Часть команды `guild`, команда которая включает модули для гилда");
        put("help.command.guild.enable.usage", "enable <модуль>");
        put("help.command.guild.enable.sub", "Методы");
        put("help.command.guild.enable.section", "Секция `enable`");
        put("help.command.guild.enable.shortDesc", "включает модули для гилда");

        put("help.command.guild.disable.title", "Помощь по подкоманде `disable`");
        put("help.command.guild.disable.desc", "Часть команды `guild`, команда которая выключает модули у гилда");
        put("help.command.guild.disable.usage", "disable <модуль>");
        put("help.command.guild.disable.sub", "Методы");
        put("help.command.guild.disable.section", "Секция `disable`");
        put("help.command.guild.disable.shortDesc", "выключает модули у гилда");

        put("help.command.guild.show.title", "Помощь по подкоманде `show`");
        put("help.command.guild.show.desc", "Часть команды `guild`, команда которая показывает разную информацию о данных гилда");
        put("help.command.guild.show.usage", "show <что показать>");
        put("help.command.guild.show.sub", "Свойства");
        put("help.command.guild.show.section", "Секция `show`");
        put("help.command.guild.show.shortDesc", "показывает информацию о данных гилда");

        put("help.command.guild.show.modules.title", "Помощь по свойству `modules`");
        put("help.command.guild.show.modules.desc", "Часть подкоманды `show`, показывает все включенные модули гилда");
        put("help.command.guild.show.modules.usage", "modules");
        put("help.command.guild.show.modules.sub", "???");
        put("help.command.guild.show.modules.section", "???");
        put("help.command.guild.show.modules.shortDesc", "показывает включенные модули гилда");

        put("help.command.guild.show.managers.title", "Помощь по свойству `managers`");
        put("help.command.guild.show.managers.desc", "Часть подкоманды `show`, показывает менеджеров бота");
        put("help.command.guild.show.managers.usage", "managers");
        put("help.command.guild.show.managers.sub", "???");
        put("help.command.guild.show.managers.section", "???");
        put("help.command.guild.show.managers.shortDesc", "показывает менеджеров бота");

        put("help.command.guild.show.permLevel.title", "Помощь по свойству `permLevel`");
        put("help.command.guild.show.permLevel.desc", "Часть подкоманды `show`, показывает уровень доверья у гилда");
        put("help.command.guild.show.permLevel.usage", "permLevel");
        put("help.command.guild.show.permLevel.sub", "???");
        put("help.command.guild.show.permLevel.section", "???");
        put("help.command.guild.show.permLevel.shortDesc", "показывает уровень доверья");

        put("help.command.guild.show.language.title", "Помощь по свойству `language`");
        put("help.command.guild.show.language.desc", "Часть подкоманды `show`, показывает выбранный язык гилда");
        put("help.command.guild.show.language.usage", "language");
        put("help.command.guild.show.language.sub", "???");
        put("help.command.guild.show.language.section", "???");
        put("help.command.guild.show.language.shortDesc", "показывает выбранный язык");

        put("help.command.guild.show.timezone.title", "Помощь по свойству `timezone`");
        put("help.command.guild.show.timezone.desc", "Часть подкоманды `show`, показывает выбранную временную зону гилда");
        put("help.command.guild.show.timezone.usage", "timezone");
        put("help.command.guild.show.timezone.sub", "???");
        put("help.command.guild.show.timezone.section", "???");
        put("help.command.guild.show.timezone.shortDesc", "показывает выбранную временную зону");

        put("help.command.guild.show.all.title", "Помощь по свойству `all`");
        put("help.command.guild.show.all.desc", "Часть подкоманды `show`, показывает всю информацию о гилде");
        put("help.command.guild.show.all.usage", "all");
        put("help.command.guild.show.all.sub", "???");
        put("help.command.guild.show.all.section", "???");
        put("help.command.guild.show.all.shortDesc", "показывает всю информацию о гилде");

        put("help.command.guild.set.title", "Помощь по подкоманде `set`");
        put("help.command.guild.set.desc", "Часть команды `guild`, команда которая ставит значения свойств");
        put("help.command.guild.set.usage", "set <что поставить>");
        put("help.command.guild.set.sub", "Свойства");
        put("help.command.guild.set.section", "Секция `set`");
        put("help.command.guild.set.shortDesc", "ставит значения свойств");

        put("help.command.guild.set.permLevel.title", "Помощь по свойству `permLevel`");
        put("help.command.guild.set.permLevel.desc", "Часть подкоманды `set`, ставит уровень доверья у гилда");
        put("help.command.guild.set.permLevel.usage", "permLevel <значение>");
        put("help.command.guild.set.permLevel.sub", "???");
        put("help.command.guild.set.permLevel.section", "???");
        put("help.command.guild.set.permLevel.shortDesc", "ставит уровень доверья");

        put("help.command.guild.set.language.title", "Помощь по свойству `language`");
        put("help.command.guild.set.language.desc", "Часть подкоманды `set`, ставит язык который бот будет использовать для гилда");
        put("help.command.guild.set.language.usage", "language <язык>");
        put("help.command.guild.set.language.sub", "???");
        put("help.command.guild.set.language.section", "???");
        put("help.command.guild.set.language.shortDesc", "ставит язык");

        put("help.command.guild.set.timezone.title", "Помощь по свойству `timezone`");
        put("help.command.guild.set.timezone.desc", "Часть подкоманды `set`, ставит временную зону которую бот будет использовать для гилда");
        put("help.command.guild.set.timezone.usage", "timezone <временная зона>");
        put("help.command.guild.set.timezone.sub", "???");
        put("help.command.guild.set.timezone.section", "???");
        put("help.command.guild.set.timezone.shortDesc", "ставит временную зону");

        put("help.command.guild.add.title", "Помощь по подкоманде `add`");
        put("help.command.guild.add.desc", "Часть команды `guild`, команда которая добавляет вещи в данные гилда");
        put("help.command.guild.add.usage", "add <что добавить>");
        put("help.command.guild.add.sub", "Свойства");
        put("help.command.guild.add.section", "Секция `add`");
        put("help.command.guild.add.shortDesc", "добавляет вещи в данные гилда");

        put("help.command.guild.add.managers.title", "Помощь по свойству `managers`");
        put("help.command.guild.add.managers.desc", "Часть подкоманды `add`, добавляет менеджеров в бота");
        put("help.command.guild.add.managers.usage", "managers <упоминания>");
        put("help.command.guild.add.managers.sub", "???");
        put("help.command.guild.add.managers.section", "???");
        put("help.command.guild.add.managers.shortDesc", "добавляет менеджеров в бота");

        put("help.command.guild.remove.title", "Помощь по подкоманде `remove`");
        put("help.command.guild.remove.desc", "Часть команды `guild`, команда которая убирает вещи из данных гилда");
        put("help.command.guild.remove.usage", "remove <что убрать>");
        put("help.command.guild.remove.sub", "Свойства");
        put("help.command.guild.remove.section", "Секция `remove`");
        put("help.command.guild.remove.shortDesc", "убирает вещи из данных гилда");

        put("help.command.guild.remove.managers.title", "Помощь по свойству `managers`");
        put("help.command.guild.remove.managers.desc", "Часть подкоманды `remove`, убирает менеджеров из бота");
        put("help.command.guild.remove.managers.usage", "managers <упоминания>");
        put("help.command.guild.remove.managers.sub", "???");
        put("help.command.guild.remove.managers.section", "???");
        put("help.command.guild.remove.managers.shortDesc", "убирает менеджеров из бота");

        put("help.command.module.title", "Помощь по команде `module`");
        put("help.command.module.desc", "Команда, которая позволяет просматиривать и изменять данные модулей");
        put("help.command.module.usage", "module <метод> <агрументы>");
        put("help.command.module.sub", "Методы");
        put("help.command.module.section", "Секция `modules`");
        put("help.command.module.shortDesc", "показывает информацию о данных модулей");

        put("help.command.module.list.title", "Помощь по подкоманде `list`");
        put("help.command.module.list.desc", "Часть команды `modules`, команда которая показывает все доступные модули");
        put("help.command.module.list.usage", "list");
        put("help.command.module.list.sub", "Свойства");
        put("help.command.module.list.section", "Секция `list`");
        put("help.command.module.list.shortDesc", "показывает доступные модули");

        put("help.command.dailytop.title", "Помощь по команде `dailytop`");
        put("help.command.dailytop.desc", "Команда которая управляет топами");
        put("help.command.dailytop.usage", "dailytop <метод>");
        put("help.command.dailytop.sub", "Методы");
        put("help.command.dailytop.section", "Секция `dailytop`");
        put("help.command.dailytop.shortDesc", "управляет топами");

        put("help.command.dailytop.voicepoints.title", "Помощь по подкоманде `points`");
        put("help.command.dailytop.voicepoints.desc", "Часть команды `dailytop`, команда которая управляет войспоинт топами");
        put("help.command.dailytop.voicepoints.usage", "points <метод>");
        put("help.command.dailytop.voicepoints.sub", "Методы");
        put("help.command.dailytop.voicepoints.section", "Секция `points`");
        put("help.command.dailytop.voicepoints.shortDesc", "управляет войспоинт топами");

        put("help.command.dailytop.voicepoints.here.title", "Помощь по подкоманде `here`");
        put("help.command.dailytop.voicepoints.here.desc", "Часть подкоманды `points`, команда которая создает новый войспоинт топ");
        put("help.command.dailytop.voicepoints.here.usage", "here");
        put("help.command.dailytop.voicepoints.here.sub", "???");
        put("help.command.dailytop.voicepoints.here.section", "???");
        put("help.command.dailytop.voicepoints.here.shortDesc", "создает новый войспоинт топ");

        put("help.command.dailytop.voicepoints.clear.title", "Помощь по подкоманде `clear`");
        put("help.command.dailytop.voicepoints.clear.desc", "Часть подкоманды `points`, команда которая убирает все войспоинт топы из канала");
        put("help.command.dailytop.voicepoints.clear.usage", "clear");
        put("help.command.dailytop.voicepoints.clear.sub", "???");
        put("help.command.dailytop.voicepoints.clear.section", "???");
        put("help.command.dailytop.voicepoints.clear.shortDesc", "убирает все войспоинт топы из канала");

        put("help.command.dailytop.voicelevels.title", "Помощь по подкоманде `levels`");
        put("help.command.dailytop.voicelevels.desc", "Часть команды `dailytop`, команда которая управляет топами войс уровня");
        put("help.command.dailytop.voicelevels.usage", "levels <метод>");
        put("help.command.dailytop.voicelevels.sub", "Методы");
        put("help.command.dailytop.voicelevels.section", "Секция `levels`");
        put("help.command.dailytop.voicelevels.shortDesc", "управляет топами войс уровня");

        put("help.command.dailytop.voicelevels.here.title", "Помощь по подкоманде `here`");
        put("help.command.dailytop.voicelevels.here.desc", "Часть подкоманды `levels`, команда которая создает новый топ войс уровней");
        put("help.command.dailytop.voicelevels.here.usage", "here");
        put("help.command.dailytop.voicelevels.here.sub", "???");
        put("help.command.dailytop.voicelevels.here.section", "???");
        put("help.command.dailytop.voicelevels.here.shortDesc", "создает новый топ войс уровней");

        put("help.command.dailytop.voicelevels.clear.title", "Помощь по подкоманде `clear`");
        put("help.command.dailytop.voicelevels.clear.desc", "Часть подкоманды `levels`, команда которая убирает все топы войс уровней из канала");
        put("help.command.dailytop.voicelevels.clear.usage", "clear");
        put("help.command.dailytop.voicelevels.clear.sub", "???");
        put("help.command.dailytop.voicelevels.clear.section", "???");
        put("help.command.dailytop.voicelevels.clear.shortDesc", "убирает все топы войс уровней из канала");

        put("help.command.dailytop.voicetime.title", "Помощь по подкоманде `time`");
        put("help.command.dailytop.voicetime.desc", "Часть команды `dailytop`, команда которая управляет топами войс времени");
        put("help.command.dailytop.voicetime.usage", "time <метод>");
        put("help.command.dailytop.voicetime.sub", "Методы");
        put("help.command.dailytop.voicetime.section", "Секция `time`");
        put("help.command.dailytop.voicetime.shortDesc", "управляет топами войс времени");

        put("help.command.dailytop.voicetime.here.title", "Помощь по подкоманде `here`");
        put("help.command.dailytop.voicetime.here.desc", "Часть подкоманды `time`, команда которая создает новый топ войс времени");
        put("help.command.dailytop.voicetime.here.usage", "here");
        put("help.command.dailytop.voicetime.here.sub", "???");
        put("help.command.dailytop.voicetime.here.section", "???");
        put("help.command.dailytop.voicetime.here.shortDesc", "создает новый топ войс времени");

        put("help.command.dailytop.voicetime.clear.title", "Помощь по подкоманде `clear`");
        put("help.command.dailytop.voicetime.clear.desc", "Часть подкоманды `time`, команда которая убирает все топы войс времени из канала");
        put("help.command.dailytop.voicetime.clear.usage", "clear");
        put("help.command.dailytop.voicetime.clear.sub", "???");
        put("help.command.dailytop.voicetime.clear.section", "???");
        put("help.command.dailytop.voicetime.clear.shortDesc", "убирает все топы войс времени из канала");

        put("help.command.dailytop.update.title", "Помощь по подкоманде `update`");
        put("help.command.dailytop.update.desc", "Часть команды `dailytop`, команда которая обновляет все топы");
        put("help.command.dailytop.update.usage", "update");
        put("help.command.dailytop.update.sub", "Методы");
        put("help.command.dailytop.update.section", "Секция `update`");
        put("help.command.dailytop.update.shortDesc", "обновляет все топы");

        put("help.command.debug.title", "Помощь по команде `debug`");
        put("help.command.debug.desc", "Команда которая позволяет использовать дебаг фичи бота\n**НЕ ИСПОЛЬЗОВАТЬ ЭТУ КОМАНДУ** *(не то, что ты можешь)*");
        put("help.command.debug.usage", "debug <секция> <метод>");
        put("help.command.debug.sub", "Методы");
        put("help.command.debug.section", "Секция `debug`");
        put("help.command.debug.shortDesc", "позволяет использовать дебаг фичи бота");

        put("help.command.debug.images.title", "Помощь по секции `images`");
        put("help.command.debug.images.desc", "Часть команды `debug`, содержит команды для дебага картинок");
        put("help.command.debug.images.usage", "images <метод>");
        put("help.command.debug.images.sub", "Методы");
        put("help.command.debug.images.section", "Секция `images`");
        put("help.command.debug.images.shortDesc", "содержит команды для дебага картинок");

        put("help.command.debug.images.drop.title", "Помощь по подкоманде `drop`");
        put("help.command.debug.images.drop.desc", "Часть секции `images`, команда которая стерает базу картинок");
        put("help.command.debug.images.drop.usage", "drop");
        put("help.command.debug.images.drop.sub", "???");
        put("help.command.debug.images.drop.section", "???");
        put("help.command.debug.images.drop.shortDesc", "стерает базу картинок");

        put("help.command.debug.images.import.title", "Помощь по подкоманде `import`");
        put("help.command.debug.images.import.desc", "Часть секции `import`, команда которая импортирует csv файл в базу картинок");
        put("help.command.debug.images.import.usage", "import <путь к файлу>");
        put("help.command.debug.images.import.sub", "???");
        put("help.command.debug.images.import.section", "???");
        put("help.command.debug.images.import.shortDesc", "импортирует csv файл в базу картинок");

        put("help.command.debug.guilds.title", "Помощь по секции `guilds`");
        put("help.command.debug.guilds.desc", "Часть команды `debug`, содержит команды для дебага гилдов");
        put("help.command.debug.guilds.usage", "guilds <метод>");
        put("help.command.debug.guilds.sub", "Методы");
        put("help.command.debug.guilds.section", "Секция `guilds`");
        put("help.command.debug.guilds.shortDesc", "содержит команды для дебага гилдов");

        put("help.command.debug.guilds.drop.title", "Помощь по подкоманде `drop`");
        put("help.command.debug.guilds.drop.desc", "Часть секции `guilds`, команда которая стерает базу гилдов");
        put("help.command.debug.guilds.drop.usage", "drop");
        put("help.command.debug.guilds.drop.sub", "???");
        put("help.command.debug.guilds.drop.section", "???");
        put("help.command.debug.guilds.drop.shortDesc", "стерает базу гилдов");

        put("help.command.debug.status.title", "Помощь по секции `status`");
        put("help.command.debug.status.desc", "Часть команды `debug`, содержит команды для дебага статусов");
        put("help.command.debug.status.usage", "status <метод>");
        put("help.command.debug.status.sub", "Методы");
        put("help.command.debug.status.section", "Секция `status`");
        put("help.command.debug.status.shortDesc", "содержит команды для дебага статусов");

        put("help.command.debug.status.add.title", "Помощь по подкоманде `add`");
        put("help.command.debug.status.add.desc", "Часть секции `status`, команда которая добавляет новый статус в базу статусов");
        put("help.command.debug.status.add.usage", "add <действие> <сообщение>");
        put("help.command.debug.status.add.sub", "???");
        put("help.command.debug.status.add.section", "???");
        put("help.command.debug.status.add.shortDesc", "добавляет статус в базу статусов");

        put("help.command.debug.status.remove.title", "Помощь по подкоманде `remove`");
        put("help.command.debug.status.remove.desc", "Часть секции `status`, команда которая удаляет статус из базы статусов");
        put("help.command.debug.status.remove.usage", "remove <действие> <сообщение>");
        put("help.command.debug.status.remove.sub", "???");
        put("help.command.debug.status.remove.section", "???");
        put("help.command.debug.status.remove.shortDesc", "удаляет статус из базы статусов");

        put("help.command.debug.status.list.title", "Помощь по подкоманде `list`");
        put("help.command.debug.status.list.desc", "Часть секции `status`, команда которая показывает сохраненные статус сообщения из базы");
        put("help.command.debug.status.list.usage", "list");
        put("help.command.debug.status.list.sub", "???");
        put("help.command.debug.status.list.section", "???");
        put("help.command.debug.status.list.shortDesc", "показывает список статусов");

        put("help.command.debug.status.drop.title", "Помощь по подкоманде `drop`");
        put("help.command.debug.status.drop.desc", "Часть секции `status`, команда которая стерает базу статусов");
        put("help.command.debug.status.drop.usage", "drop");
        put("help.command.debug.status.drop.sub", "???");
        put("help.command.debug.status.drop.section", "???");
        put("help.command.debug.status.drop.shortDesc", "стерает базу статусов");

        put("help.command.debug.general.title", "Помощь по секции `general`");
        put("help.command.debug.general.desc", "Часть команды `debug`, содержит команды для основного дебага");
        put("help.command.debug.general.usage", "general <метод>");
        put("help.command.debug.general.sub", "Методы");
        put("help.command.debug.general.section", "Секция `general`");
        put("help.command.debug.general.shortDesc", "содержит команды для основного дебага");

        put("help.command.debug.general.kill.title", "Помощь по подкоманде `kill-process`");
        put("help.command.debug.general.kill.desc", "Часть секции `general`, команда которая убивает бота");
        put("help.command.debug.general.kill.usage", "kill-process");
        put("help.command.debug.general.kill.sub", "???");
        put("help.command.debug.general.kill.section", "???");
        put("help.command.debug.general.kill.shortDesc", "убивает бота");

        put("help.command.debug.general.status.title", "Помощь по подкоманде `status`");
        put("help.command.debug.general.status.desc", "Часть секции `general`, команда которая ставит статус бота");
        put("help.command.debug.general.status.usage", "status <действие> <сообщение>");
        put("help.command.debug.general.status.sub", "???");
        put("help.command.debug.general.status.section", "???");
        put("help.command.debug.general.status.shortDesc", "ставит статус бота");

        put("help.command.debug.dailytop.title", "Помощь по секции `dailytop`");
        put("help.command.debug.dailytop.desc", "Часть команды `debug`, содержит команды для дебага топов");
        put("help.command.debug.dailytop.usage", "dailytop <метод>");
        put("help.command.debug.dailytop.sub", "Методы");
        put("help.command.debug.dailytop.section", "Секция `dailytop`");
        put("help.command.debug.dailytop.shortDesc", "содержит команды для дебага топов");

        put("help.command.debug.dailytop.drop.title", "Помощь по подкоманде `drop`");
        put("help.command.debug.dailytop.drop.desc", "Часть секции `dailytop`, команда которая стерает базу топов");
        put("help.command.debug.dailytop.drop.usage", "drop");
        put("help.command.debug.dailytop.drop.sub", "???");
        put("help.command.debug.dailytop.drop.section", "???");
        put("help.command.debug.dailytop.drop.shortDesc", "стерает базу топов");

        put("help.command.help.title", "Помощь по команде `help`");
        put("help.command.help.desc", "Команда которая показывает помощь по командам и их подкомандам");
        put("help.command.help.usage", "help (команда/страница) (подкоманды...)");
        put("help.command.help.sub", "???");
        put("help.command.help.section", "???");
        put("help.command.help.shortDesc", "показывает помощь по командам");

        put("help.command.images.title", "Помощь по команде `images`");
        put("help.command.images.desc", "Команда которая показывает информацию о базе картинок");
        put("help.command.images.usage", "images <метод>");
        put("help.command.images.sub", "Методы");
        put("help.command.images.section", "Секция `images`");
        put("help.command.images.shortDesc", "показывает информацию о базе картинок");

        put("help.command.images.stats.title", "Помощь по подкоманде `stats`");
        put("help.command.images.stats.desc", "Команда которая показывает статистику базы картинок");
        put("help.command.images.stats.usage", "stats");
        put("help.command.images.stats.sub", "???");
        put("help.command.images.stats.section", "???");
        put("help.command.images.stats.shortDesc", "показывает статистику базы картинок");

        put("help.command.images.tags.title", "Помощь по подкоманде `tags`");
        put("help.command.images.tags.desc", "Команда которая показывает теги, которые использует вся база картинок");
        put("help.command.images.tags.usage", "tags");
        put("help.command.images.tags.sub", "???");
        put("help.command.images.tags.section", "???");
        put("help.command.images.tags.shortDesc", "показывает все теги");

        put("help.command.add.title", "Помощь по команде `add`");
        put("help.command.add.desc", "Команда которая добавляет картинки в базу");
        put("help.command.add.usage", "add <ссылка/теги> <теги...>");
        put("help.command.add.sub", "???");
        put("help.command.add.section", "???");
        put("help.command.add.shortDesc", "добавляет картинки в базу");

        put("help.command.edit.title", "Помощь по команде `edit`");
        put("help.command.edit.desc", "Команда которая редактирует теги последней картинки");
        put("help.command.edit.usage", "edit <теги...>");
        put("help.command.edit.sub", "???");
        put("help.command.edit.section", "???");
        put("help.command.edit.shortDesc", "редактирует теги картинок");

        put("help.command.delete.title", "Помощь по команде `delete`");
        put("help.command.delete.desc", "Команда которая удаляет последнюю картинку, имеет подтверждение удаления");
        put("help.command.delete.usage", "delete");
        put("help.command.delete.sub", "???");
        put("help.command.delete.section", "???");
        put("help.command.delete.shortDesc", "удаляет последнюю картинку");

        put("help.command.next.title", "Помощь по команде `next`");
        put("help.command.next.desc", "Команда которая показывает следующую картинку из последнего поиска");
        put("help.command.next.usage", "next");
        put("help.command.next.sub", "???");
        put("help.command.next.section", "???");
        put("help.command.next.shortDesc", "показывает следующую картинку из последнего поиска");

        put("help.command.random.title", "Помощь по команде `random`");
        put("help.command.random.desc", "Команда которая показывает произвольную картинку из последнего поиска, или из всей базы если `all` указано");
        put("help.command.random.usage", "random (all)");
        put("help.command.random.sub", "???");
        put("help.command.random.section", "???");
        put("help.command.random.shortDesc", "показывает произвольную картинку из последнего поиска");

        put("help.command.search.title", "Помощь по команде `search`");
        put("help.command.search.desc", "Команда которая ищет картинки в базе");
        put("help.command.search.usage", "search <теги>");
        put("help.command.search.sub", "???");
        put("help.command.search.section", "???");
        put("help.command.search.shortDesc", "ищет картинки в базе");

        put("help.command.recent.title", "Помощь по команде `recent`");
        put("help.command.recent.desc", "Команда которая показывает последнюю картинку которую показывал бот");
        put("help.command.recent.usage", "recent");
        put("help.command.recent.sub", "???");
        put("help.command.recent.section", "???");
        put("help.command.recent.shortDesc", "показывает последнюю картинку");

        put("help.command.tag.title", "Помощь по команде `tag`");
        put("help.command.tag.desc", "Команда которая тегирует картинки из папки рандома, способ добавлять картинки из папки рандома");
        put("help.command.tag.usage", "tag");
        put("help.command.tag.sub", "???");
        put("help.command.tag.section", "???");
        put("help.command.tag.shortDesc", "тегирует картинки из папки рандома");

        put("help.command.choose.title", "Помощь по команде `choose`");
        put("help.command.choose.desc", "Команда которая выбирает произвольное слово");
        put("help.command.choose.usage", "choose <слова>");
        put("help.command.choose.sub", "???");
        put("help.command.choose.section", "???");
        put("help.command.choose.shortDesc", "выбирает произвольное слово");

        put("help.command.roll.title", "Помощь по команде `roll`");
        put("help.command.roll.desc", "Команда которая кидает кости, максимально может кинуть 50 костей за раз, использует шаблоны в виде #d# и d#");
        put("help.command.roll.usage", "roll #d#/d#");
        put("help.command.roll.sub", "???");
        put("help.command.roll.section", "???");
        put("help.command.roll.shortDesc", "кидает кости");

        put("help.command.rickroll.title", "Помощь по команде `rickroll`");
        put("help.command.rickroll.desc", "Команда которая рикролит тебя");
        put("help.command.rickroll.usage", "rickroll");
        put("help.command.rickroll.sub", "???");
        put("help.command.rickroll.section", "???");
        put("help.command.rickroll.shortDesc", "рикролит тебя");

        put("help.command.test.title", "Помощь по команде `test`");
        put("help.command.test.desc", "Команда которая фури");
        put("help.command.test.usage", "test");
        put("help.command.test.sub", "???");
        put("help.command.test.section", "???");
        put("help.command.test.shortDesc", "фури команда");

        put("help.command.lua.title", "Помощь по команде `lua`");
        put("help.command.lua.desc", "Команда которая позволяет использовать луа для более глубокого дебага\n**ОПЯТЬ ЖЕ, НЕ ИСПОЛЬЗОВАТЬ ЭТУ КОМАНДУ**");
        put("help.command.lua.usage", "lua <код>");
        put("help.command.lua.sub", "???");
        put("help.command.lua.section", "???");
        put("help.command.lua.shortDesc", "луа команда");

        put("help.commands.missing", "Команды отсутствуют");

        put("help.command.slots.title", "Помощь по команде `slots`");
        put("help.command.slots.desc", "Команда которая содержит игру слот машин, использует войспоинты как валюту");
        put("help.command.slots.usage", "slots <метод>");
        put("help.command.slots.sub", "Методы");
        put("help.command.slots.section", "Секция `slots`");
        put("help.command.slots.shortDesc", "играет в слоты");

        put("help.command.slots.bet.title", "Помощь по подкоманде `bet`");
        put("help.command.slots.bet.desc", "Часть команды `slots`, команда которая начинает новую сессию игры в слоты\nСтавка может быть от 5 до 500");
        put("help.command.slots.bet.usage", "bet <ставка>");
        put("help.command.slots.bet.sub", "???");
        put("help.command.slots.bet.section", "???");
        put("help.command.slots.bet.shortDesc", "начинает новую сессию");

        put("help.command.slots.table.title", "Помощь по подкоманде `table`");
        put("help.command.slots.table.desc", "Часть команды `slots`, команда которая показывает таблицу выигрышей в игре Слоты");
        put("help.command.slots.table.usage", "table");
        put("help.command.slots.table.sub", "???");
        put("help.command.slots.table.section", "???");
        put("help.command.slots.table.shortDesc", "показывает таблицу выигрышей");

        put("debug.lua.execution.title", "Результаты запуска");

        put("error.debug.lua.error.title", "Луа ошибка");
        put("error.debug.lua.error.desc", "Код поймал ошибку: [0]");
        put("textslots.name", "Слоты");

        put("slots.text.title", "Слоты");
        put("slots.text.before_start.desc", "Добро пожаловать в Слоты, крутите барабаны чтобы начать");
        put("slots.text.before_start.reels", "Некогда не крутились");
        put("slots.text.after_start.desc", "Игра в Слоты");
        put("slots.text.reels.key", "Барабаны");
        put("slots.text.reward.key", "Выигрыш");
        put("slots.text.reward.value", "Вы выиграли [0] войспоинтов");
        put("slots.text.reward.streak.value", "Вы выиграли [0] войспоинтов, [1] раза такая же комбинация!");
        put("slots.text.reward.maxstreak.value", "Вы выиграли [0] войспоинтов, [1] раза такая же комбинация!\nМаксимальная череда побед!");
        put("slots.text.reward.rolling", "Барабаны крутятся...");
        put("slots.text.bet.key", "Ставка");
        put("slots.text.bet.value", "[0] войспоинтов");
        put("slots.text.balance.key", "Баланс");
        put("slots.text.balance.value", "[0] войспоинтов");
        put("slots.text.help.key", "Помощь");
        put("slots.text.help.value", ":arrows_counterclockwise: - крутить барабаны\n:x: - закончить игру");
        put("slots.text.spin", "Крутить");

        put("slots.text.post.key", "Итог");
        put("slots.text.post.total.key", "Итог с момента логина");
        put("slots.text.post.lost.value", "[0] войспоинтов проиграно");
        put("slots.text.post.won.value", "[0] войспоинтов выиграно");

        put("slots.table.title", "Таблица выигрышей в Слотах");
        put("slots.table.desc", "Базовая ставка: 5 войспоинтов\n\n" +
                "Любой 1 <:KEKW:704752163485646878> - 3 войспоинтов\n" +
                "Любые 2 <:KEKW:704752163485646878> - 5 войспоинтов\n" +
                "<:KEKW:704752163485646878><:KEKW:704752163485646878><:KEKW:704752163485646878> - 10 войспоинтов\n" +
                "<:USALOV:570996636243853362><:USALOV:570996636243853362><:USALOV:570996636243853362> - 20 войспоинтов\n" +
                "<:zeza_pokerface:794579204263116820><:zeza_pokerface:794579204263116820><:zeza_pokerface:794579204263116820> - 30 войспоинтов\n" +
                "<:ZEZa_OY:794606274820964362><:ZEZa_OY:794606274820964362><:ZEZa_OY:794606274820964362> - 40 войспоинтов\n" +
                "<:fleme:647828664146198539><:fleme:647828664146198539><:fleme:647828664146198539> - 80 войспоинтов\n" +
                "<:n_SUPREME:811624243426230302><:n_SUPREME:811624243426230302><:n_SUPREME:811624243426230302> - 2000 войспоинтов\n\n" +
                "Череда побед сильно умножает выигрыши\nЧем больше ставка, тем больше умножение выигрыша");

        put("slots.text.ended.key", "Конец игры");
        put("slots.text.ended.value", "Игра была закончена");

        put("slots.text.timeout.key", "Конец игры");
        put("slots.text.timeout.value", "Таймоут сессии");

        put("error.missing.slots.bet.title", "Укажи ставку");
        put("error.missing.slots.bet.desc", "Нельзя играть без ставки");

        put("error.unknown.slots.bet.title", "Ставка не является целым числом");
        put("error.unknown.slots.bet.desc", "Укажи правильное целое число");

        put("error.limit.slots.bet.title", "Ставка слишком большая или слишком маленькая");
        put("error.limit.slots.bet.desc", "Ставка может быть в диапазоне от 5 до 500");

        put("error.request.error.uno.title", "Запрос завалился по вине Уночки");

        put("slots.web.command.title", "Веб версия игры в Слоты");
        put("slots.web.command.desc", "Веб версия Слотов находится на данной ссылке\nЧтобы получить ключ авторизации, среагируй \uD83D\uDD11 на это сообщение");
        put("slots.web.command.field", "URL");
        put("slots.web.command.dm", "Вот ключик для логина в Слоты\n\n||[0]||\n\nКлюч просрочится через час");

        put("slots.web.command.deauth.title", "Разлогинили все ключи");
        put("slots.web.command.deauth.desc", "[0] логинов было удалено");

        put("help.command.slots.web.title", "Помощь по подкоманде `web`");
        put("help.command.slots.web.desc", "Часть команды `slots`, команда которая показывает как добраться до веб версии игры в Слоты");
        put("help.command.slots.web.usage", "web");
        put("help.command.slots.web.sub", "Подкоманды");
        put("help.command.slots.web.section", "???");
        put("help.command.slots.web.shortDesc", "веб версия игры в Слоты");

        put("help.command.slots.web.deauth.title", "Помощь по подкоманде `deauth`");
        put("help.command.slots.web.deauth.desc", "Часть подкоманды `web`, команда которая удаляет все текущие логины в веб версии игры в Слоты");
        put("help.command.slots.web.deauth.usage", "deauth");
        put("help.command.slots.web.deauth.sub", "???");
        put("help.command.slots.web.deauth.section", "???");
        put("help.command.slots.web.deauth.shortDesc", "удаляет все веб логины");

        put("error.missing.custom.name.title", "Укажи название команды");
        put("error.missing.custom.name.desc", "Надо указать название чтобы создать новую команду");

        put("error.missing.custom.scope.title", "Укажи сферу команды");
        put("error.missing.custom.scope.desc", "Надо указать сферу команды где она будет доступна. Может быть global или guild");

        put("error.missing.custom.access.title", "Укажи доступ команды");
        put("error.missing.custom.access.desc", "Надо указать доступ команды, кто может её использовать. Может быть: anyone, managers или owner");

        put("error.unknown.custom.name.title", "Нету такой команды");
        put("error.unknown.custom.name.desc", "Нету пользовательской команды с таким именем");

        put("error.unknown.custom.scope.title", "Неизвестная сфера");
        put("error.unknown.custom.scope.desc", "Укажи правильную сферу команды. Может быть global или guild");

        put("error.unknown.custom.access.title", "Неизвестный уровень доступа");
        put("error.unknown.custom.access.desc", "Укажи правильный доступ команды. Может быть: anyone, managers или owner");


        put("error.custom.name.taken.title", "Имя уже занято");
        put("error.custom.name.taken.desc", "Нельзя иметь несколько команд с одним и тем же названием");

        put("error.custom.lua.syntax.title", "Синтаксическая ошибка");
        put("error.custom.lua.syntax.desc", "Код словил ошибку во время компиляции\n[0]");

        put("error.custom.lua.error.title", "Ошибка команды");
        put("error.custom.lua.error.desc", "Код словил ошибку во время выполнения команды\n[0]");

        put("error.custom.not_intended.title", "Команда не предназначена для этого гилда");
        put("error.custom.not_intended.desc", "Эта команда была написана для использования в другом гилде");

        put("custom.added.title", "Команда добавлена");
        put("custom.added.desc", "Новая пользовательская команда с названием '[0]' была добавлена");

        put("custom.edited.title", "Команда изменена");
        put("custom.edited.desc", "Команда была изменена удачно");

        put("custom.deleted.title", "Команда удалена");
        put("custom.deleted.desc", "Команда была удалена удачно");

        put("custom.show.title", "Код команды `[0]`");
        put("custom.show.desc", "Сфера: [0]; Доступ: [1]\n```lua\n[2]\n```");

        put("custom.list.title", "Список пользовательских команд");

        put("custom.executed.title", "Результат запуска команды");


        put("help.command.custom.title", "Помощь по команде `custom`");
        put("help.command.custom.desc", "Команда которая позволяет создавать новые команды с помощью Lua");
        put("help.command.custom.usage", "custom <метод>");
        put("help.command.custom.sub", "Методы");
        put("help.command.custom.section", "Секция `custom`");
        put("help.command.custom.shortDesc", "пользовательские команды");

        put("help.command.custom.add.title", "Помощь по подкоманде `add`");
        put("help.command.custom.add.desc", "Часть команды `custom`, команда которая создает новые пользовательские команды\nСфера может быть guild или global, сфера 'guild' запретит использовать эту команду где-либо кроме этого гилда\nДоступ может быть anyone, managers или owner");
        put("help.command.custom.add.usage", "add <название> <сфера> <доступ> <код>");
        put("help.command.custom.add.sub", "???");
        put("help.command.custom.add.section", "???");
        put("help.command.custom.add.shortDesc", "создает команду");

        put("help.command.custom.edit.title", "Помощь по подкоманде `edit`");
        put("help.command.custom.edit.desc", "Часть команды `custom`, изменяет существующие пользовательские команды");
        put("help.command.custom.edit.usage", "edit <что изменить>");
        put("help.command.custom.edit.sub", "Методы");
        put("help.command.custom.edit.section", "???");
        put("help.command.custom.edit.shortDesc", "изменяет команды");

        put("help.command.custom.edit.code.title", "Помощь по подкоманде `code`");
        put("help.command.custom.edit.code.desc", "Часть подкоманды `edit`, изменяет код команды");
        put("help.command.custom.edit.code.usage", "code <название> <код>");
        put("help.command.custom.edit.code.sub", "???");
        put("help.command.custom.edit.code.section", "???");
        put("help.command.custom.edit.code.shortDesc", "изменяет код команды");

        put("help.command.custom.edit.scope.title", "Помощь по подкоманде `scope`");
        put("help.command.custom.edit.scope.desc", "Часть подкоманды `edit`, изменяет сферу команды\nСфера может быть guild или global, сфера 'guild' запретит использовать эту команду где-либо кроме этого гилда");
        put("help.command.custom.edit.scope.usage", "scope <название> <сфера>");
        put("help.command.custom.edit.scope.sub", "???");
        put("help.command.custom.edit.scope.section", "???");
        put("help.command.custom.edit.scope.shortDesc", "изменяет сферу команды");

        put("help.command.custom.edit.access.title", "Помощь по подкоманде `access`");
        put("help.command.custom.edit.access.desc", "Часть подкоманды `edit`, изменяет доступ команды\nДоступ может быть anyone, managers или owner");
        put("help.command.custom.edit.access.usage", "access <название> <доступ>");
        put("help.command.custom.edit.access.sub", "???");
        put("help.command.custom.edit.access.section", "???");
        put("help.command.custom.edit.access.shortDesc", "изменяет доступ команды");

        put("help.command.custom.delete.title", "Помощь по подкоманде `delete`");
        put("help.command.custom.delete.desc", "Часть команды `custom`, удаляет указанную команду");
        put("help.command.custom.delete.usage", "delete <название>");
        put("help.command.custom.delete.sub", "???");
        put("help.command.custom.delete.section", "???");
        put("help.command.custom.delete.shortDesc", "удаляет команду");

        put("help.command.custom.show.title", "Помощь по подкоманде `show`");
        put("help.command.custom.show.desc", "Часть команды `custom`, показывает информацию о указанной команды");
        put("help.command.custom.show.usage", "show <название>");
        put("help.command.custom.show.sub", "???");
        put("help.command.custom.show.section", "???");
        put("help.command.custom.show.shortDesc", "показывает информацию о команде");

        put("help.command.custom.list.title", "Помощь по подкоманде `list`");
        put("help.command.custom.list.desc", "Часть команды `custom`, показывает список пользовательских команд");
        put("help.command.custom.list.usage", "list");
        put("help.command.custom.list.sub", "???");
        put("help.command.custom.list.section", "???");
        put("help.command.custom.list.shortDesc", "показывает список команд");


        put("error.unknown.integer.title", "Значение не является целым числом");
        put("error.unknown.integer.desc", "Посмотри в помощь и дай целое число где надо");

        put("error.missing.integer.title", "Отсутствует целое число");
        put("error.missing.integer.desc", "Следующию вещь которую тебе надо было указать это целое число");


        put("error.missing.custom.code.title", "Напиши код");
        put("error.missing.custom.code.desc", "Нельзя иметь пользовательскую команду без кода");

        put("custom.commands.name", "Кастумные Команды");
    }};

    @Override
    public String formatKey(String key, String... args) {
        String result = translationMap.get(key);

        if(result != null) {
            for (int i = 0; i < args.length; i++) {
                result = result.replaceAll("\\["+i+"]", args[i]);
            }

            return result;
        } else return key;
    }

     @Override
     public TranslatorLanguage getLanguageEnum() {
          return TranslatorLanguage.Russian;
     }
}
