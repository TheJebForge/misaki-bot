package com.thejebforge.misaki.bot.localisation;

import java.util.HashMap;
import java.util.Map;

public class EnglishLocalisation extends BaseLocalisation {
    protected static final Map<String, String> translationMap = new HashMap<String, String>(){{
        put("configurator.name", "Configurator");
        put("debugmodule.name", "Debug Module");
        put("custom.commands.name", "Custom Commands");
        put("helper.name", "Helper");
        put("imagemanager.name", "Image Manager");
        put("randommodule.name", "Random Commands");
        put("testmodule.name", "Test Module");
        put("textslots.name", "Slots");


        put("choose.title", "I choose...");
        put("choose.desc", "Seems like a good choice");
        put("choose.field", "Choice");


        put("debug.images.dropped.title", "Images dropped");
        put("debug.images.dropped.desc", "Image database is now squeaky clean");

        put("debug.images.drop.confirm.title", "Drop confirmation");
        put("debug.images.drop.confirm.desc", "Are you sure you want to delete every single image from image database?");
        put("debug.images.drop.confirm.field", "To confirm");


        put("debug.guilds.dropped.title", "Guilds dropped");
        put("debug.guilds.dropped.desc", "Guild database is now squeaky clean");

        put("debug.guilds.drop.confirm.title", "Drop confirmation");
        put("debug.guilds.drop.confirm.desc", "Are you sure you want to delete every single guild from guild database?");
        put("debug.guilds.drop.confirm.field", "To confirm");


        put("debug.status.added.title", "Status added");
        put("debug.status.added.desc", "It might appear in future now");
        put("debug.status.added.activity", "Activity");
        put("debug.status.added.message", "Message");

        put("debug.status.updated.title", "Status updated");
        put("debug.status.updated.desc", "It should probably reflect that on the user pane");

        put("debug.status.removed.title", "Status removed");
        put("debug.status.removed.desc", "It will never appear in future");

        put("debug.status.list.title", "All status messages in DB");

        put("debug.status.dropped.title", "Status messages dropped");
        put("debug.status.dropped.desc", "Status database is now squeaky clean");

        put("debug.status.drop.confirm.title", "Drop confirmation");
        put("debug.status.drop.confirm.desc", "Are you sure you want to delete every single status message from status database?");
        put("debug.status.drop.confirm.field", "To confirm");


        put("debug.general.kill.title", "Goodbye world");
        put("debug.general.kill.desc", "Process killed");


        put("debug.images.import.title", "[0] images were imported");
        put("debug.images.import.desc", "Now question is, if they're actually valid files...");


        put("error.exception.command.title", "Ow! Exception!");
        put("error.exception.command.desc", "I caught exception while trying to process the command D: Stop breaking me");


        put("error.empty.imagedb.title", "Add something first");
        put("error.empty.imagedb.desc", "There's no images in database, see [0]");

        put("error.empty.search.title", "There's nothing in current search");
        put("error.empty.search.desc", "Did you just deleted all images of current search?");


        put("error.guild.not_trusted.title", "Guild isn't trusted enough");
        put("error.guild.not_trusted.desc", "Guild permission level is too low for module with codename '[0]'");


        put("error.images.delete.failed.title", "Image deleted successfully");
        put("error.images.delete.failed.desc", "Image was deleted successfully");

        put("error.images.download.failed.title", "Failed to add image");
        put("error.images.download.failed.desc", "An exception was caught while trying to download the image, try again with something else");

        put("error.images.add.failed.title", "Failed to add image");
        put("error.images.add.failed.desc", "Some exception was caught while trying to add the image");

        put("error.images.import.fail.title", "Failed to import file");
        put("error.images.import.fail.desc", "Some exception was caught while trying to import the file");

        put("error.images.random.find_fail.title", "Failed to find random images");
        put("error.images.random.find_fail.desc", "Some exception was caught while trying to get a random image");

        put("error.images.file.doesnt_exist.title", "Specified file doesn't exist");
        put("error.images.file.doesnt_exist.desc", "There's no such file in bot's directory");

        put("error.missing.file.path.title", "No path was specified");
        put("error.missing.file.path.desc", "See help and specify a path for the file");


        put("error.unknown.status.activity.title", "Invalid activity");
        put("error.unknown.status.activity.desc", "Activity can be one of following: [0]");


        put("error.missing.activity.message.title", "Provide some message");
        put("error.missing.activity.message.desc", "Can't have empty activity message");

        put("error.missing.choices.title", "There's nothing to choose");
        put("error.missing.choices.desc", "I need something to choose from, right?");

        put("error.missing.subcommand.property.title", "No property specified");
        put("error.missing.subcommand.property.desc", "There's more to this command than you think, do [0] to learn more");

        put("error.missing.subcommand.method.title", "No method specified");
        put("error.missing.subcommand.method.desc", "There's more to this command than you think, do [0] to learn more");

        put("error.missing.subcommand.section.title", "No section specified");
        put("error.missing.subcommand.section.desc", "There's more to this command than you think, do [0] to learn more");

        put("error.missing.image.add.title", "Specify image");
        put("error.missing.image.add.desc", "Attach an image or add link to it as first argument, see [0]");

        put("error.missing.image.tag.title", "No image were selected for tagging");
        put("error.missing.image.tag.desc", "Run `[0]` first to select an image to tag");

        put("error.missing.language.title", "Specify language");
        put("error.missing.language.desc", "Specify known language, bot currently knows following languages: [0]");

        put("error.missing.mention.add.title", "Mention someone");
        put("error.missing.mention.add.desc", "I can't add someone without them being mentioned, check [0]");

        put("error.missing.mention.remove.title", "Mention someone");
        put("error.missing.mention.remove.desc", "I can't remove someone without them being mentioned, check [0]");

        put("error.missing.module.title", "Specify module codename");
        put("error.missing.module.desc", "Next thing you had to input is codename");

        put("error.missing.module.disable.title", "Specify what module to disable");
        put("error.missing.module.disable.desc", "I can't disable something I don't know");

        put("error.missing.recent.image.title", "No recent images");
        put("error.missing.recent.image.desc", "There wasn't any images showing up lately");

        put("error.missing.search.next.title", "Search for something first");
        put("error.missing.search.next.desc", "I can't show next image without having found something first, see [0]");

        put("error.missing.search.random.title", "Search for something first");
        put("error.missing.search.random.desc", "I can't show random image without having found something first, see [0]");

        put("error.missing.status.activity.title", "No activity was specified");
        put("error.missing.status.activity.desc", "Specify an activity, see help of the command");

        put("error.missing.tags.image_add.title", "Specify tags");
        put("error.missing.tags.image_add.desc", "I need tags to add the image into the database, see [0]");

        put("error.missing.tags.image_edit.title", "Specify tags");
        put("error.missing.tags.image_edit.desc", "I need tags to edit the image, see [0]");

        put("error.missing.tags.image_search.title", "Specify tags");
        put("error.missing.tags.image_search.desc", "I need tags to search for images, see [0]");

        put("error.missing.value.set.title", "Specify value");
        put("error.missing.value.set.desc", "I can't set something without value, check [0]");

        put("error.missing.page.too_high.title", "There's no such page");
        put("error.missing.page.too_high.desc", "There's not enough of modules to achieve that page number");

        put("error.missing.page.too_low.title", "There's no such page");
        put("error.missing.page.too_low.desc", "Pages don't go lower than 1, right?...");

        put("error.missing.random.title", "There's no random images");
        put("error.missing.random.desc", "No images were found in random images folder");

        put("error.missing.roll.pattern.title", "Pattern not provided");
        put("error.missing.roll.pattern.desc", "I need a pattern to roll dice, give me some");


        put("error.module.already.enabled.title", "Guild already has the module");
        put("error.module.already.enabled.desc", "Guild already has module with codename '[0]' enabled");

        put("error.module.already.disabled.title", "Guild doesn't have the module");
        put("error.module.already.disabled.desc", "Guild already doesn't have module with codename '[0]' enabled");

        put("error.module.permanent.disable.title", "Module can't be disabled");
        put("error.module.permanent.disable.desc", "Module with codename '[0]' does not allow to disable itself");


        put("error.permission.edit.not_manager.title", "Permission denied");
        put("error.permission.edit.not_manager.desc", "Only managers of the bot are able to edit this");

        put("error.permission.edit.not_owner.title", "Permission denied");
        put("error.permission.edit.not_owner.desc", "Only owner of the bot is able to edit this");

        put("error.permission.debug.not_owner.title", "Permission denied");
        put("error.permission.debug.not_owner.desc", "Only owner of the bot may use debug features of the bot");

        put("error.random.bad_argument.title", "Random what?");
        put("error.random.bad_argument.desc", "I don't know what you mean, see [0]");


        put("error.roll.too_many.title", "Can't roll so many dice");
        put("error.roll.too_many.desc", "I can maximum roll only 50 dice at a time");

        put("error.roll.bad_number.title", "Invalid numbers");
        put("error.roll.bad_number.desc", "Those numbers seem to be wrong or too large, see [0]");

        put("error.roll.bad_pattern.title", "Invalid pattern");
        put("error.roll.bad_pattern.desc", "You gotta specify proper pattern of #d#, see [0]");

        put("error.unknown.command.title", "Unknown command");
        put("error.unknown.command.desc", "None of the modules seem to implement that command");
        put("error.unknown.command.field", "Command");

        put("error.unknown.command.help.title", "No such command");
        put("error.unknown.command.help.desc", "Help for command that you specified does not exist");

        put("error.unknown.language.title", "Invalid language");
        put("error.unknown.language.desc", "Specify known language, bot currently knows following languages: [0]");

        put("error.unknown.module.title", "Module doesn't exist");
        put("error.unknown.module.desc", "There's no such module with codename '[0]'");

        put("error.unknown.status.title", "No such status message");
        put("error.unknown.status.desc", "There's no such message in the database");

        put("error.missing.status.title", "Missing status message");
        put("error.missing.status.desc", "Specify status, see help of the command");

        put("error.unknown.subcommand.property.title", "Unknown property specified");
        put("error.unknown.subcommand.property.desc", "There's no such property, check [0]");

        put("error.unknown.subcommand.method.title", "Unknown method specified");
        put("error.unknown.subcommand.method.desc", "There's no such method, check [0]");

        put("error.unknown.subcommand.section.title", "Unknown section specified");
        put("error.unknown.subcommand.section.desc", "There's no such section, check [0]");

        put("error.unknown.integer.title", "Value is not an integer");
        put("error.unknown.integer.desc", "Check help and provide me an integer");

        put("error.missing.integer.title", "Missing integer");
        put("error.missing.integer.desc", "Next thing you had to provide was an integer");


        put("error.value.not_int.title", "Value is not an integer");
        put("error.value.not_int.desc", "Value must be an integer, check [0]");

        put("error.value.not_int_characters.title", "Value contains something else than numbers");
        put("error.value.not_int_characters.desc", "Value must be a number, check [0]");


        put("error.wrong.command.too_long.title", "Command is suspiciously too long");
        put("error.wrong.command.too_long.desc", "Why you screaming? You need help?");

        put("error.wrong.command.bad_characters.title", "Command contains weird characters");
        put("error.wrong.command.bad_characters.desc", "All you had to do is input damn letters CJ");

        put("error.wrong.command.no_command.title", "Huh?");
        put("error.wrong.command.no_command.desc", "Did you meant to mention me?");

        put("error.missing.help.title", "No help for specified command");
        put("error.missing.help.desc", "There's no help for specified command");


        put("guild.module.enabled.title", "Module is enabled");
        put("guild.module.enabled.desc", "Module '[0]' has been enabled for current guild");

        put("guild.module.disabled.title", "Module is disabled");
        put("guild.module.disabled.desc", "Module '[0]' has been disabled for current guild");

        put("guild.module.show.title", "Enabled modules on this guild");


        put("guild.managers.added.title", "Managers updated");
        put("guild.managers.added.desc", "Mentioned members were added as managers of the bot");

        put("guild.managers.removed.title", "Managers updated");
        put("guild.managers.removed.desc", "Mentioned members were removed from managers of the bot");

        put("guild.managers.show.title", "Managers on this guild");

        put("guild.permLevel.show.title", "Guild Permission Level");
        put("guild.permLevel.show.field", "Level");


        put("help.command.guild.title", "Help of command `guild`");
        put("help.command.guild.desc", "Command that lets you view and change guild data");
        put("help.command.guild.usage", "guild <method> <arguments>");
        put("help.command.guild.sub", "Methods");
        put("help.command.guild.section", "`guild` Section");
        put("help.command.guild.shortDesc", "shows and changes guild data");

        put("help.command.guild.enable.title", "Help of subcommand `enable`");
        put("help.command.guild.enable.desc", "Part of `guild` command, command that enables modules for guilds");
        put("help.command.guild.enable.usage", "enable <module>");
        put("help.command.guild.enable.sub", "Methods");
        put("help.command.guild.enable.section", "`enable` Section");
        put("help.command.guild.enable.shortDesc", "enables modules for guilds");

        put("help.command.guild.disable.title", "Help of subcommand `disable`");
        put("help.command.guild.disable.desc", "Part of `guild` command, command that disables modules for guilds");
        put("help.command.guild.disable.usage", "disable <module>");
        put("help.command.guild.disable.sub", "Methods");
        put("help.command.guild.disable.section", "`disable` Section");
        put("help.command.guild.disable.shortDesc", "disables modules for guilds");

        put("help.command.guild.show.title", "Help of subcommand `show`");
        put("help.command.guild.show.desc", "Part of `guild` command, command that shows various information about guild");
        put("help.command.guild.show.usage", "show <what to show>");
        put("help.command.guild.show.sub", "Properties");
        put("help.command.guild.show.section", "`show` Section");
        put("help.command.guild.show.shortDesc", "shows information about guild");

        put("help.command.guild.show.modules.title", "Help of property `modules`");
        put("help.command.guild.show.modules.desc", "Part of `show` subcommand, shows all enabled modules on the guild");
        put("help.command.guild.show.modules.usage", "modules");
        put("help.command.guild.show.modules.sub", "???");
        put("help.command.guild.show.modules.section", "???");
        put("help.command.guild.show.modules.shortDesc", "shows enabled modules");

        put("help.command.guild.show.managers.title", "Help of property `managers`");
        put("help.command.guild.show.managers.desc", "Part of `show` subcommand, shows all bot's managers on the guild");
        put("help.command.guild.show.managers.usage", "managers");
        put("help.command.guild.show.managers.sub", "???");
        put("help.command.guild.show.managers.section", "???");
        put("help.command.guild.show.managers.shortDesc", "shows bot's managers");

        put("help.command.guild.show.permLevel.title", "Help of property `permLevel`");
        put("help.command.guild.show.permLevel.desc", "Part of `show` subcommand, shows permission level of the guild");
        put("help.command.guild.show.permLevel.usage", "permLevel");
        put("help.command.guild.show.permLevel.sub", "???");
        put("help.command.guild.show.permLevel.section", "???");
        put("help.command.guild.show.permLevel.shortDesc", "shows permission level");

        put("help.command.guild.show.language.title", "Help of property `language`");
        put("help.command.guild.show.language.desc", "Part of `show` subcommand, shows selected language of the guild");
        put("help.command.guild.show.language.usage", "language");
        put("help.command.guild.show.language.sub", "???");
        put("help.command.guild.show.language.section", "???");
        put("help.command.guild.show.language.shortDesc", "shows selected language");

        put("help.command.guild.show.timezone.title", "Help of property `timezone`");
        put("help.command.guild.show.timezone.desc", "Part of `show` subcommand, shows selected timezone of the guild");
        put("help.command.guild.show.timezone.usage", "timezone");
        put("help.command.guild.show.timezone.sub", "???");
        put("help.command.guild.show.timezone.section", "???");
        put("help.command.guild.show.timezone.shortDesc", "shows selected timezone");

        put("help.command.guild.show.all.title", "Help of property `all`");
        put("help.command.guild.show.all.desc", "Part of `show` subcommand, shows all information about the guild");
        put("help.command.guild.show.all.usage", "all");
        put("help.command.guild.show.all.sub", "???");
        put("help.command.guild.show.all.section", "???");
        put("help.command.guild.show.all.shortDesc", "shows all information");

        put("help.command.guild.set.title", "Help of subcommand `set`");
        put("help.command.guild.set.desc", "Part of `guild` command, command that sets properties of the guild data");
        put("help.command.guild.set.usage", "set <what to set>");
        put("help.command.guild.set.sub", "Properties");
        put("help.command.guild.set.section", "`set` Section");
        put("help.command.guild.set.shortDesc", "sets properties of the guild data");

        put("help.command.guild.set.permLevel.title", "Help of property `permLevel`");
        put("help.command.guild.set.permLevel.desc", "Part of `set` command, sets permission level of the guild");
        put("help.command.guild.set.permLevel.usage", "permLevel <value>");
        put("help.command.guild.set.permLevel.sub", "???");
        put("help.command.guild.set.permLevel.section", "???");
        put("help.command.guild.set.permLevel.shortDesc", "sets permission level");

        put("help.command.guild.set.language.title", "Help of property `language`");
        put("help.command.guild.set.language.desc", "Part of `set` command, sets language bot will use for the guild");
        put("help.command.guild.set.language.usage", "language <language>");
        put("help.command.guild.set.language.sub", "???");
        put("help.command.guild.set.language.section", "???");
        put("help.command.guild.set.language.shortDesc", "sets language");

        put("help.command.guild.set.timezone.title", "Help of property `timezone`");
        put("help.command.guild.set.timezone.desc", "Part of `set` command, sets timezone bot will use for the guild");
        put("help.command.guild.set.timezone.usage", "timezone <timezone>");
        put("help.command.guild.set.timezone.sub", "???");
        put("help.command.guild.set.timezone.section", "???");
        put("help.command.guild.set.timezone.shortDesc", "sets timezone");

        put("help.command.guild.add.title", "Help of subcommand `add`");
        put("help.command.guild.add.desc", "Part of `guild` command, command that adds things to guild data");
        put("help.command.guild.add.usage", "add <what to add>");
        put("help.command.guild.add.sub", "Properties");
        put("help.command.guild.add.section", "`add` Section");
        put("help.command.guild.add.shortDesc", "adds things to guild data");

        put("help.command.guild.add.managers.title", "Help of property `managers`");
        put("help.command.guild.add.managers.desc", "Part of `add` command, adds managers to the bot");
        put("help.command.guild.add.managers.usage", "managers <mentions>");
        put("help.command.guild.add.managers.sub", "???");
        put("help.command.guild.add.managers.section", "???");
        put("help.command.guild.add.managers.shortDesc", "adds managers to the bot");

        put("help.command.guild.remove.title", "Help of subcommand `remove`");
        put("help.command.guild.remove.desc", "Part of `guild` command, command that removes things from guild data");
        put("help.command.guild.remove.usage", "remove <what to remove>");
        put("help.command.guild.remove.sub", "Properties");
        put("help.command.guild.remove.section", "`remove` Section");
        put("help.command.guild.remove.shortDesc", "removes things from guild data");

        put("help.command.guild.remove.managers.title", "Help of property `managers`");
        put("help.command.guild.remove.managers.desc", "Part of `remove` command, removes managers from the bot");
        put("help.command.guild.remove.managers.usage", "managers <mentions>");
        put("help.command.guild.remove.managers.sub", "???");
        put("help.command.guild.remove.managers.section", "???");
        put("help.command.guild.remove.managers.shortDesc", "removes managers from the bot");

        put("help.command.module.title", "Help of command `module`");
        put("help.command.module.desc", "Command that lets you view and change module data");
        put("help.command.module.usage", "module <method> <arguments>");
        put("help.command.module.sub", "Methods");
        put("help.command.module.section", "`modules` Section");
        put("help.command.module.shortDesc", "shows and changes module data");

        put("help.command.module.list.title", "Help of subcommand `list`");
        put("help.command.module.list.desc", "Part of `module` command, command that shows all available modules");
        put("help.command.module.list.usage", "list");
        put("help.command.module.list.sub", "Properties");
        put("help.command.module.list.section", "`list` Section");
        put("help.command.module.list.shortDesc", "shows all available modules");

        put("help.command.dailytop.title", "Help of command `dailytop`");
        put("help.command.dailytop.desc", "Command that managers daily tops");
        put("help.command.dailytop.usage", "dailytop <method>");
        put("help.command.dailytop.sub", "Methods");
        put("help.command.dailytop.section", "`dailytop` Section");
        put("help.command.dailytop.shortDesc", "manages daily tops");

        put("help.command.dailytop.voicepoints.title", "Help of subcommand `points`");
        put("help.command.dailytop.voicepoints.desc", "Part of `dailytop` command, command that manages voice points tops");
        put("help.command.dailytop.voicepoints.usage", "points <method>");
        put("help.command.dailytop.voicepoints.sub", "Methods");
        put("help.command.dailytop.voicepoints.section", "`points` Section");
        put("help.command.dailytop.voicepoints.shortDesc", "manages voice points tops");

        put("help.command.dailytop.voicepoints.here.title", "Help of subcommand `here`");
        put("help.command.dailytop.voicepoints.here.desc", "Part of `points` subcommand, command that creates a new voice points top");
        put("help.command.dailytop.voicepoints.here.usage", "here");
        put("help.command.dailytop.voicepoints.here.sub", "???");
        put("help.command.dailytop.voicepoints.here.section", "???");
        put("help.command.dailytop.voicepoints.here.shortDesc", "creates voice points top");

        put("help.command.dailytop.voicepoints.clear.title", "Help of subcommand `clear`");
        put("help.command.dailytop.voicepoints.clear.desc", "Part of `points` subcommand, command that clears voice points tops from the channel");
        put("help.command.dailytop.voicepoints.clear.usage", "clear");
        put("help.command.dailytop.voicepoints.clear.sub", "???");
        put("help.command.dailytop.voicepoints.clear.section", "???");
        put("help.command.dailytop.voicepoints.clear.shortDesc", "clears voice points tops from the channel");

        put("help.command.dailytop.voicelevels.title", "Help of subcommand `levels`");
        put("help.command.dailytop.voicelevels.desc", "Part of `dailytop` command, command that manages voice level tops");
        put("help.command.dailytop.voicelevels.usage", "levels <method>");
        put("help.command.dailytop.voicelevels.sub", "Methods");
        put("help.command.dailytop.voicelevels.section", "`levels` Section");
        put("help.command.dailytop.voicelevels.shortDesc", "manages voice level tops");

        put("help.command.dailytop.voicelevels.here.title", "Help of subcommand `here`");
        put("help.command.dailytop.voicelevels.here.desc", "Part of `levels` subcommand, command that creates a new voice level top");
        put("help.command.dailytop.voicelevels.here.usage", "here");
        put("help.command.dailytop.voicelevels.here.sub", "???");
        put("help.command.dailytop.voicelevels.here.section", "???");
        put("help.command.dailytop.voicelevels.here.shortDesc", "creates voice level top");

        put("help.command.dailytop.voicelevels.clear.title", "Help of subcommand `clear`");
        put("help.command.dailytop.voicelevels.clear.desc", "Part of `levels` subcommand, command that clears voice level tops from the channel");
        put("help.command.dailytop.voicelevels.clear.usage", "clear");
        put("help.command.dailytop.voicelevels.clear.sub", "???");
        put("help.command.dailytop.voicelevels.clear.section", "???");
        put("help.command.dailytop.voicelevels.clear.shortDesc", "clears voice level tops from the channel");

        put("help.command.dailytop.voicetime.title", "Help of subcommand `time`");
        put("help.command.dailytop.voicetime.desc", "Part of `dailytop` command, command that manages voice time tops");
        put("help.command.dailytop.voicetime.usage", "time <method>");
        put("help.command.dailytop.voicetime.sub", "Methods");
        put("help.command.dailytop.voicetime.section", "`time` Section");
        put("help.command.dailytop.voicetime.shortDesc", "manages voice time tops");

        put("help.command.dailytop.voicetime.here.title", "Help of subcommand `here`");
        put("help.command.dailytop.voicetime.here.desc", "Part of `time` subcommand, command that creates a new voice time top");
        put("help.command.dailytop.voicetime.here.usage", "here");
        put("help.command.dailytop.voicetime.here.sub", "???");
        put("help.command.dailytop.voicetime.here.section", "???");
        put("help.command.dailytop.voicetime.here.shortDesc", "creates voice time top");

        put("help.command.dailytop.voicetime.clear.title", "Help of subcommand `clear`");
        put("help.command.dailytop.voicetime.clear.desc", "Part of `time` subcommand, command that clears voice time tops from the channel");
        put("help.command.dailytop.voicetime.clear.usage", "clear");
        put("help.command.dailytop.voicetime.clear.sub", "???");
        put("help.command.dailytop.voicetime.clear.section", "???");
        put("help.command.dailytop.voicetime.clear.shortDesc", "clears voice time tops from the channel");

        put("help.command.dailytop.update.title", "Help of subcommand `update`");
        put("help.command.dailytop.update.desc", "Part of `dailytop` command, command that updates all tops");
        put("help.command.dailytop.update.usage", "update");
        put("help.command.dailytop.update.sub", "Methods");
        put("help.command.dailytop.update.section", "`update` Section");
        put("help.command.dailytop.update.shortDesc", "updates all tops");

        put("help.command.debug.title", "Help of command `debug`");
        put("help.command.debug.desc", "Command that lets you use debug features of the bot\n**NO ONE SHALL USE THIS COMMAND** *(not like you can)*");
        put("help.command.debug.usage", "debug <section> <method>");
        put("help.command.debug.sub", "Methods");
        put("help.command.debug.section", "`debug` Section");
        put("help.command.debug.shortDesc", "provides debug features");

        put("help.command.debug.images.title", "Help of section `images`");
        put("help.command.debug.images.desc", "Part of command `debug`, contains commands for images debug");
        put("help.command.debug.images.usage", "images <method>");
        put("help.command.debug.images.sub", "Methods");
        put("help.command.debug.images.section", "`images` Section");
        put("help.command.debug.images.shortDesc", "contains commands for images debug");

        put("help.command.debug.images.drop.title", "Help of subcommand `drop`");
        put("help.command.debug.images.drop.desc", "Part of section `images`, command that drops images database");
        put("help.command.debug.images.drop.usage", "drop");
        put("help.command.debug.images.drop.sub", "???");
        put("help.command.debug.images.drop.section", "???");
        put("help.command.debug.images.drop.shortDesc", "drops images database");

        put("help.command.debug.images.import.title", "Help of subcommand `import`");
        put("help.command.debug.images.import.desc", "Part of section `import`, command that imports csv files into images database");
        put("help.command.debug.images.import.usage", "import <path to file>");
        put("help.command.debug.images.import.sub", "???");
        put("help.command.debug.images.import.section", "???");
        put("help.command.debug.images.import.shortDesc", "imports csv files to images db");

        put("help.command.debug.guilds.title", "Help of section `guilds`");
        put("help.command.debug.guilds.desc", "Part of command `debug`, contains commands for guilds debug");
        put("help.command.debug.guilds.usage", "guilds <method>");
        put("help.command.debug.guilds.sub", "Methods");
        put("help.command.debug.guilds.section", "`guilds` Section");
        put("help.command.debug.guilds.shortDesc", "contains commands for guilds debug");

        put("help.command.debug.guilds.drop.title", "Help of subcommand `drop`");
        put("help.command.debug.guilds.drop.desc", "Part of section `guilds`, command that drops guilds database");
        put("help.command.debug.guilds.drop.usage", "drop");
        put("help.command.debug.guilds.drop.sub", "???");
        put("help.command.debug.guilds.drop.section", "???");
        put("help.command.debug.guilds.drop.shortDesc", "drops guilds database");

        put("help.command.debug.status.title", "Help of section `status`");
        put("help.command.debug.status.desc", "Part of command `debug`, contains commands for status debug");
        put("help.command.debug.status.usage", "status <method>");
        put("help.command.debug.status.sub", "Methods");
        put("help.command.debug.status.section", "`status` Section");
        put("help.command.debug.status.shortDesc", "contains commands for status debug");

        put("help.command.debug.status.add.title", "Help of subcommand `add`");
        put("help.command.debug.status.add.desc", "Part of section `status`, command that adds new status message to database");
        put("help.command.debug.status.add.usage", "add <activity> <message>");
        put("help.command.debug.status.add.sub", "???");
        put("help.command.debug.status.add.section", "???");
        put("help.command.debug.status.add.shortDesc", "adds new status to db");

        put("help.command.debug.status.remove.title", "Help of subcommand `remove`");
        put("help.command.debug.status.remove.desc", "Part of section `status`, command that removes status message from database");
        put("help.command.debug.status.remove.usage", "remove <activity> <message>");
        put("help.command.debug.status.remove.sub", "???");
        put("help.command.debug.status.remove.section", "???");
        put("help.command.debug.status.remove.shortDesc", "removes status from db");

        put("help.command.debug.status.list.title", "Help of subcommand `list`");
        put("help.command.debug.status.list.desc", "Part of section `status`, command that lists status messages in database");
        put("help.command.debug.status.list.usage", "list");
        put("help.command.debug.status.list.sub", "???");
        put("help.command.debug.status.list.section", "???");
        put("help.command.debug.status.list.shortDesc", "lists statuses from db");

        put("help.command.debug.status.drop.title", "Help of subcommand `drop`");
        put("help.command.debug.status.drop.desc", "Part of section `status`, command that drops status database");
        put("help.command.debug.status.drop.usage", "drop");
        put("help.command.debug.status.drop.sub", "???");
        put("help.command.debug.status.drop.section", "???");
        put("help.command.debug.status.drop.shortDesc", "drops status database");

        put("help.command.debug.general.title", "Help of section `general`");
        put("help.command.debug.general.desc", "Part of command `debug`, contains commands for general debug");
        put("help.command.debug.general.usage", "general <method>");
        put("help.command.debug.general.sub", "Methods");
        put("help.command.debug.general.section", "`general` Section");
        put("help.command.debug.general.shortDesc", "contains commands for general debug");

        put("help.command.debug.general.kill.title", "Help of subcommand `kill-process`");
        put("help.command.debug.general.kill.desc", "Part of section `general`, command that kills the bot");
        put("help.command.debug.general.kill.usage", "kill-process");
        put("help.command.debug.general.kill.sub", "???");
        put("help.command.debug.general.kill.section", "???");
        put("help.command.debug.general.kill.shortDesc", "kills the bot");

        put("help.command.debug.general.status.title", "Help of subcommand `status`");
        put("help.command.debug.general.status.desc", "Part of section `general`, command that sets status of the bot");
        put("help.command.debug.general.status.usage", "status <activity> <message>");
        put("help.command.debug.general.status.sub", "???");
        put("help.command.debug.general.status.section", "???");
        put("help.command.debug.general.status.shortDesc", "sets status of the bot");

        put("help.command.debug.dailytop.title", "Help of section `dailytop`");
        put("help.command.debug.dailytop.desc", "Part of command `debug`, contains commands for dailytop debug");
        put("help.command.debug.dailytop.usage", "dailytop <method>");
        put("help.command.debug.dailytop.sub", "Methods");
        put("help.command.debug.dailytop.section", "`dailytop` Section");
        put("help.command.debug.dailytop.shortDesc", "contains commands for dailytop debug");

        put("help.command.debug.dailytop.drop.title", "Help of subcommand `drop`");
        put("help.command.debug.dailytop.drop.desc", "Part of section `dailytop`, command that drops daily top database");
        put("help.command.debug.dailytop.drop.usage", "drop");
        put("help.command.debug.dailytop.drop.sub", "???");
        put("help.command.debug.dailytop.drop.section", "???");
        put("help.command.debug.dailytop.drop.shortDesc", "drops daily top database");

        put("help.command.help.title", "Help of command `help`");
        put("help.command.help.desc", "Command that displays help for commands and their subcommands");
        put("help.command.help.usage", "help (command/page) (subcommands...)");
        put("help.command.help.sub", "???");
        put("help.command.help.section", "???");
        put("help.command.help.shortDesc", "displays help for commands");

        put("help.command.images.title", "Help of command `images`");
        put("help.command.images.desc", "Command that lists information about images database");
        put("help.command.images.usage", "images <methods>");
        put("help.command.images.sub", "Methods");
        put("help.command.images.section", "`images` Section");
        put("help.command.images.shortDesc", "shows information about images db");

        put("help.command.images.stats.title", "Help of subcommand `stats`");
        put("help.command.images.stats.desc", "Command that lists images statistics");
        put("help.command.images.stats.usage", "stats");
        put("help.command.images.stats.sub", "???");
        put("help.command.images.stats.section", "???");
        put("help.command.images.stats.shortDesc", "lists images statistics");

        put("help.command.images.tags.title", "Help of subcommand `tags`");
        put("help.command.images.tags.desc", "Command that shows tags used by images in the database");
        put("help.command.images.tags.usage", "tags");
        put("help.command.images.tags.sub", "???");
        put("help.command.images.tags.section", "???");
        put("help.command.images.tags.shortDesc", "shows all image tags");

        put("help.command.add.title", "Help of command `add`");
        put("help.command.add.desc", "Command that adds images to image database");
        put("help.command.add.usage", "add <link/tags> <tags...>");
        put("help.command.add.sub", "???");
        put("help.command.add.section", "???");
        put("help.command.add.shortDesc", "adds images to image db");

        put("help.command.edit.title", "Help of command `edit`");
        put("help.command.edit.desc", "Command that edits recent image's tags");
        put("help.command.edit.usage", "edit <tags...>");
        put("help.command.edit.sub", "???");
        put("help.command.edit.section", "???");
        put("help.command.edit.shortDesc", "edits image tags");

        put("help.command.delete.title", "Help of command `delete`");
        put("help.command.delete.desc", "Command that deletes recent image from image database, does have deletion confirmation");
        put("help.command.delete.usage", "delete");
        put("help.command.delete.sub", "???");
        put("help.command.delete.section", "???");
        put("help.command.delete.shortDesc", "deletes recent image");

        put("help.command.next.title", "Help of command `next`");
        put("help.command.next.desc", "Command that shows next image from recent search, if there wasn't any searches, you need to perform one");
        put("help.command.next.usage", "next");
        put("help.command.next.sub", "???");
        put("help.command.next.section", "???");
        put("help.command.next.shortDesc", "shows next image from search");

        put("help.command.random.title", "Help of command `random`");
        put("help.command.random.desc", "Command that random image from recent search, or from entire database if `all` is provided");
        put("help.command.random.usage", "random (all)");
        put("help.command.random.sub", "???");
        put("help.command.random.section", "???");
        put("help.command.random.shortDesc", "shows random image from search");

        put("help.command.search.title", "Help of command `search`");
        put("help.command.search.desc", "Command that searches for images from image database");
        put("help.command.search.usage", "search <tags>");
        put("help.command.search.sub", "???");
        put("help.command.search.section", "???");
        put("help.command.search.shortDesc", "searches for images");

        put("help.command.recent.title", "Help of command `recent`");
        put("help.command.recent.desc", "Command that shows the recent image that edit and delete commands use");
        put("help.command.recent.usage", "recent");
        put("help.command.recent.sub", "???");
        put("help.command.recent.section", "???");
        put("help.command.recent.shortDesc", "shows recent image");

        put("help.command.tag.title", "Help of command `tag`");
        put("help.command.tag.desc", "Command that tags images from random folder, way to add images from folder of assorted images");
        put("help.command.tag.usage", "tag");
        put("help.command.tag.sub", "???");
        put("help.command.tag.section", "???");
        put("help.command.tag.shortDesc", "tags images from random folder");

        put("help.command.choose.title", "Help of command `choose`");
        put("help.command.choose.desc", "Command that chooses random word from provided ones");
        put("help.command.choose.usage", "choose <words>");
        put("help.command.choose.sub", "???");
        put("help.command.choose.section", "???");
        put("help.command.choose.shortDesc", "chooses random word");

        put("help.command.roll.title", "Help of command `roll`");
        put("help.command.roll.desc", "Command that rolls dice, can maximum roll 50 dice at a time, uses patterns like #d# or d#");
        put("help.command.roll.usage", "roll #d#/d#");
        put("help.command.roll.sub", "???");
        put("help.command.roll.section", "???");
        put("help.command.roll.shortDesc", "rolls dice");

        put("help.command.rickroll.title", "Help of command `rickroll`");
        put("help.command.rickroll.desc", "Command that rickrolls you");
        put("help.command.rickroll.usage", "rickroll");
        put("help.command.rickroll.sub", "???");
        put("help.command.rickroll.section", "???");
        put("help.command.rickroll.shortDesc", "rickrolls you");

        put("help.command.test.title", "Help of command `test`");
        put("help.command.test.desc", "Command that is a furry");
        put("help.command.test.usage", "test");
        put("help.command.test.sub", "???");
        put("help.command.test.section", "???");
        put("help.command.test.shortDesc", "furry command");

        put("help.command.lua.title", "Help of command `lua`");
        put("help.command.lua.desc", "Command that allows to use lua interpreter to access more extensive debug features\n**ONCE AGAIN, NO ONE SHALL USE THIS COMMAND**");
        put("help.command.lua.usage", "lua <code>");
        put("help.command.lua.sub", "???");
        put("help.command.lua.section", "???");
        put("help.command.lua.shortDesc", "lua interpreter command");

        put("help.command.slots.title", "Help of command `slots`");
        put("help.command.slots.desc", "Command that contains slot machine game, uses voice points as currency");
        put("help.command.slots.usage", "slots <method>");
        put("help.command.slots.sub", "Methods");
        put("help.command.slots.section", "`slots` Section");
        put("help.command.slots.shortDesc", "plays slots");

        put("help.command.slots.bet.title", "Help of subcommand `bet`");
        put("help.command.slots.bet.desc", "Part of command `slots`, command that starts new slots session with provided bet\nBet can be from 5 to 500");
        put("help.command.slots.bet.usage", "bet <bet amount>");
        put("help.command.slots.bet.sub", "???");
        put("help.command.slots.bet.section", "???");
        put("help.command.slots.bet.shortDesc", "starts slots game");

        put("help.command.slots.table.title", "Help of subcommand `table`");
        put("help.command.slots.table.desc", "Part of command `slots`, command that shows winnings table for slots game");
        put("help.command.slots.table.usage", "table");
        put("help.command.slots.table.sub", "???");
        put("help.command.slots.table.section", "???");
        put("help.command.slots.table.shortDesc", "shows winning table");

        put("help.command.usage", "Usage");
        put("help.commands.missing", "Commands missing");


        put("help.commands.show.title", "Module commands");

        put("help.commands.morehelp.field", "To Get More Help");
        put("help.commands.morehelp.value", "[0] <command/page>");

        put("help.commands.furtherhelp.field", "To Get Additional Help");
        put("help.commands.furtherhelp.value", "help (command) (subcommands...)");


        put("image.added.title", "Image added successfully");
        put("image.added.desc", "Your image has been added successfully");
        put("image.added.field", "Tags");

        put("image.edited.title", "Image edited successfully");
        put("image.edited.desc", "Your image has been edited successfully");
        put("image.edited.field", "Tags");

        put("image.delete.confirm.title", "Deletion confirmation");
        put("image.delete.confirm.desc", "Are you sure you want to delete this image?");
        put("image.delete.confirm.field", "To confirm");

        put("image.deleted.title", "Image deleted successfully");
        put("image.deleted.desc", "Your image has been deleted successfully");

        put("image.next.tags", "Tags");

        put("image.random.tags", "Tags");

        put("image.recent.title", "Recent Image");
        put("image.recent.desc", "That's last shown image that I remember");

        put("image.search.found.title", "[0] images found");
        put("image.search.found.tags", "Specified tags");
        put("image.search.found.next", "To see images");
        put("image.search.found.random", "To get random from search");

        put("image.search.nothing.title", "Nothing found");
        put("image.search.nothing.desc", "Couldn't find anything with specified tags in database");
        put("image.search.nothing.field", "Tags");

        put("image.tag.title", "Tag an image");
        put("image.tag.desc", "Specify what tags this image would fit with [0] <tags>");

        put("images.stats.title", "Image Database Stats");
        put("images.stats.desc", "Amount of known images: [0]");

        put("images.tags.title", "Image Database Tags");
        put("images.tags.field", "All known tags");
        put("images.tags.value", "That's all tags that I know");


        put("language.updated.title", "Language is updated");
        put("language.updated.desc", "You're supposed to be seeing this in another language now");

        put("language.show.title", "Guild's selected language");
        put("language.show.field", "Language");


        put("modules.list.show.title", "Available modules");

        put("roll.single_dice_roll", "Dice rolled [0]");
        put("roll.multiple_dice_roll", "Dice #[0] rolled [1]");

        put("roll.results.title", "Dice Roll Results");
        put("roll.results.field", "Sum");
        put("roll.results.value", "In total all dice rolled [0]");


        put("value.set.title", "Value is set");
        put("value.set.desc", "Value of the parameter has been updated");

        put("dailytop.name", "Daily Top Module");

        put("dailytop.voice_level.title", "Local top");
        put("dailytop.voice_level.desc", "Voice system");

        put("dailytop.voice_points.title", "Local top");
        put("dailytop.voice_points.desc", "Voice points");

        put("dailytop.voice_time.title", "Local top");
        put("dailytop.voice_time.desc", "Total voice time");

        put("dailytop.top.subtext", "Updated at [0]");

        put("dailytop.voice_points.clear.confirm.title", "Clear confirmation");
        put("dailytop.voice_points.clear.confirm.desc", "Are you sure you want to clear all voice point tops from this channel?");
        put("dailytop.voice_points.clear.confirm.field", "To confirm");

        put("dailytop.voice_points.cleared.title", "Voice point tops cleared");
        put("dailytop.voice_points.cleared.desc", "All voice point tops have been cleared from this channel");

        put("dailytop.voice_points.field", "#[0] [1]");
        put("dailytop.voice_points.value", "[0] VP");

        put("dailytop.voice_level.field", "#[0] [1]");
        put("dailytop.voice_level.value", "Level [0] ([1] XP) ([2])");

        put("dailytop.voice_level.clear.confirm.title", "Clear confirmation");
        put("dailytop.voice_level.clear.confirm.desc", "Are you sure you want to clear all voice level tops from this channel?");
        put("dailytop.voice_level.clear.confirm.field", "To confirm");

        put("dailytop.voice_level.cleared.title", "Voice level tops cleared");
        put("dailytop.voice_level.cleared.desc", "All voice level tops have been cleared from this channel");

        put("dailytop.voice_time.field", "#[0] [1]");
        put("dailytop.voice_time.value", "Total time spent on the server [0] Days [1]");

        put("dailytop.voice_time.clear.confirm.title", "Clear confirmation");
        put("dailytop.voice_time.clear.confirm.desc", "Are you sure you want to clear all voice time tops from this channel?");
        put("dailytop.voice_time.clear.confirm.field", "To confirm");

        put("dailytop.voice_time.cleared.title", "Voice time tops cleared");
        put("dailytop.voice_time.cleared.desc", "All voice time tops have been cleared from this channel");

        put("dailytop.scheduled.title", "Scheduling daily top update");
        put("dailytop.scheduled.desc", "All daily tops should update soon");

        put("error.dailytop.too_soon.title", "Daily top update just happened");
        put("error.dailytop.too_soon.desc", "Wait a bit until forcing an update");

        put("dailytop.scheduling.title", "Waiting for next update");
        put("dailytop.scheduling.desc", "Update was scheduled to happen soon");

        put("debug.dailytop.dropped.title", "Daily tops dropped");
        put("debug.dailytop.dropped.desc", "Daily top database is now squeaky clean");

        put("debug.dailytop.drop.confirm.title", "Drop confirmation");
        put("debug.dailytop.drop.confirm.desc", "Are you sure you want to delete every single daily top from daily top database?");
        put("debug.dailytop.drop.confirm.field", "To confirm");

        put("error.permission.use.not_manager.title", "Permission denied");
        put("error.permission.use.not_manager.desc", "Only managers of the bot are able to use this command");

        put("error.permission.use.not_owner.title", "Permission denied");
        put("error.permission.use.not_owner.desc", "Only owner of the bot is able to use this command");

        put("timezone.show.title", "Guild's selected timezone");
        put("timezone.show.field", "Timezone ID");

        put("error.unknown.timezone.title", "Invalid timezone ID");
        put("error.unknown.timezone.desc", "Specify valid timezone ID, refer to this list for timezone IDs: [0]");

        put("error.missing.timezone.title", "Specify timezone ID");
        put("error.missing.timezone.desc", "Specify valid timezone ID, refer to this list for timezone IDs: [0]");

        put("timezone.updated.title", "Timezone is updated");
        put("timezone.updated.desc", "Timezone was set to following ID: [0]");

        put("debug.lua.execution.title", "Run results");

        put("error.debug.lua.error.title", "Lua error");
        put("error.debug.lua.error.desc", "Code caught an error: [0]");

        put("error.request.error.uno.title", "Request error for UnoChi");


        put("slots.text.title", "Slots");
        put("slots.text.before_start.desc", "Welcome to Slots, perform a spin to start");
        put("slots.text.before_start.reels", "Never spun yet");
        put("slots.text.after_start.desc", "The Slots game");
        put("slots.text.reels.key", "Reels");
        put("slots.text.reels.no_money", "Insufficient voicepoints");
        put("slots.text.reward.key", "Reward");
        put("slots.text.reward.value", "You won [0] voicepoints");
        put("slots.text.reward.streak.value", "You won [0] voicepoints, [1] times same combination!");
        put("slots.text.reward.maxstreak.value", "You won [0] voicepoints, [1] times same combination!\nMaximum streak!");
        put("slots.text.reward.rolling", "Reels spinning...");
        put("slots.text.streak.key", "Bet");
        put("slots.text.streak.value", "[0] voicepoints");
        put("slots.text.bet.key", "Bet");
        put("slots.text.bet.value", "[0] voicepoints");
        put("slots.text.balance.key", "Balance");
        put("slots.text.balance.value", "[0] voicepoints");
        put("slots.text.help.key", "Help");
        put("slots.text.help.value", ":arrows_counterclockwise: - spin reels\n:x: - end game");
        put("slots.text.spin", "Spin");

        put("slots.text.post.key", "Overall");
        put("slots.text.post.total.key", "Overall since login");
        put("slots.text.post.lost.value", "[0] voicepoints lost");
        put("slots.text.post.won.value", "[0] voicepoints won");



        put("slots.table.title", "Slots Winning Table");
        put("slots.table.desc", "Base bet: 5 voicepoints\n\n" +
                "Any 1 <:KEKW:704752163485646878> - 3 voicepoints\n" +
                "Any 2 <:KEKW:704752163485646878> - 5 voicepoints\n" +
                "<:KEKW:704752163485646878><:KEKW:704752163485646878><:KEKW:704752163485646878> - 10 voicepoints\n" +
                "<:USALOV:570996636243853362><:USALOV:570996636243853362><:USALOV:570996636243853362> - 20 voicepoints\n" +
                "<:zeza_pokerface:794579204263116820><:zeza_pokerface:794579204263116820><:zeza_pokerface:794579204263116820> - 30 voicepoints\n" +
                "<:ZEZa_OY:794606274820964362><:ZEZa_OY:794606274820964362><:ZEZa_OY:794606274820964362> - 40 voicepoints\n" +
                "<:fleme:647828664146198539><:fleme:647828664146198539><:fleme:647828664146198539> - 80 voicepoints\n" +
                "<:n_SUPREME:811624243426230302><:n_SUPREME:811624243426230302><:n_SUPREME:811624243426230302> - 2000 voicepoints\n\n" +
                "Streaks drastically multiply winnings.\nThe bigger the bet, the bigger is the reward multiplier");

        put("slots.text.ended.key", "Game over");
        put("slots.text.ended.value", "Game was ended");

        put("slots.text.timeout.key", "Game over");
        put("slots.text.timeout.value", "Session timed out");

        put("error.missing.slots.bet.title", "Specify bet");
        put("error.missing.slots.bet.desc", "Can't play without a bet");

        put("error.unknown.slots.bet.title", "Bet is not a integer");
        put("error.unknown.slots.bet.desc", "Specify a valid integer");

        put("error.limit.slots.bet.title", "Bet is either too low or too big");
        put("error.limit.slots.bet.desc", "Bet can be in range from 5 to 500");

        put("slots.web.command.title", "Web version of Slots");
        put("slots.web.command.desc", "Web version of Slots game resides on the following url\nTo get auth key, react with \uD83D\uDD11 to this message");
        put("slots.web.command.field", "URL");
        put("slots.web.command.dm", "Here's key for the web version of Slots\n\n||[0]||\n\nKey will be valid for a hour");

        put("slots.web.command.deauth.title", "De-authorized all clients");
        put("slots.web.command.deauth.desc", "[0] authorizations have been deleted");

        put("help.command.slots.web.title", "Help of subcommand `web`");
        put("help.command.slots.web.desc", "Part of command `slots`, command that shows information on how to reach web version of Slots");
        put("help.command.slots.web.usage", "web");
        put("help.command.slots.web.sub", "Subcommands");
        put("help.command.slots.web.section", "???");
        put("help.command.slots.web.shortDesc", "web version of Slots");

        put("help.command.slots.web.deauth.title", "Help of subcommand `deauth`");
        put("help.command.slots.web.deauth.desc", "Part of subcommand `web`, command that deletes all current logins on web version of Slots");
        put("help.command.slots.web.deauth.usage", "deauth");
        put("help.command.slots.web.deauth.sub", "???");
        put("help.command.slots.web.deauth.section", "???");
        put("help.command.slots.web.deauth.shortDesc", "de-authorizes web logins");

        put("error.missing.custom.name.title", "Specify command name");
        put("error.missing.custom.name.desc", "You need to specify a name to define a custom command");

        put("error.missing.custom.code.title", "Specify code");
        put("error.missing.custom.code.desc", "There's no custom command without code");

        put("error.missing.custom.scope.title", "Specify command scope");
        put("error.missing.custom.scope.desc", "You need to specify scope of where the command will be available. Can be either global or guild");

        put("error.missing.custom.access.title", "Specify command access");
        put("error.missing.custom.access.desc", "You need to specify who can use this command. Can be: anyone, managers or owner");

        put("error.unknown.custom.name.title", "No such command");
        put("error.unknown.custom.name.desc", "There's no custom command with specified name");

        put("error.unknown.custom.scope.title", "Invalid command scope");
        put("error.unknown.custom.scope.desc", "Specify valid command scope. Can be either global or guild");

        put("error.unknown.custom.access.title", "Invalid command access");
        put("error.unknown.custom.access.desc", "Specify valid command access. Can be: anyone, managers or owner");

        put("error.custom.name.taken.title", "Name already taken");
        put("error.custom.name.taken.desc", "Can't have multiple commands with same name");

        put("error.custom.lua.syntax.title", "Syntax error");
        put("error.custom.lua.syntax.desc", "Lua caught error while trying to load the code\n[0]");

        put("error.custom.lua.error.title", "Command error");
        put("error.custom.lua.error.desc", "Code crashed while trying to execute the command\n[0]");

        put("error.custom.not_intended.title", "Command not intended for this guild");
        put("error.custom.not_intended.desc", "This command was written to be used by a different guild");

        put("custom.added.title", "Command added");
        put("custom.added.desc", "New custom command with name '[0]' has been added");

        put("custom.edited.title", "Command edited");
        put("custom.edited.desc", "Command was edited successfully");

        put("custom.deleted.title", "Command deleted");
        put("custom.deleted.desc", "Command has been deleted successfully");

        put("custom.show.title", "Code of command `[0]`");
        put("custom.show.desc", "Scope: [0]; Access: [1]\n```lua\n[2]\n```");

        put("custom.list.title", "List of custom commands");

        put("custom.executed.title", "Command execution result");


        put("help.command.custom.title", "Help of command `custom`");
        put("help.command.custom.desc", "Command that allows to define custom commands using Lua");
        put("help.command.custom.usage", "custom <method>");
        put("help.command.custom.sub", "Methods");
        put("help.command.custom.section", "`custom` Section");
        put("help.command.custom.shortDesc", "custom commands");

        put("help.command.custom.add.title", "Help of subcommand `add`");
        put("help.command.custom.add.desc", "Part of command `custom`, command that creates new custom commands\nScope can be guild or global, guild scope will prevent the command from being used anywhere else than current guild\nAccess can be anyone, managers or owner");
        put("help.command.custom.add.usage", "add <name> <scope> <access> <code>");
        put("help.command.custom.add.sub", "???");
        put("help.command.custom.add.section", "???");
        put("help.command.custom.add.shortDesc", "adds custom command");

        put("help.command.custom.edit.title", "Help of subcommand `edit`");
        put("help.command.custom.edit.desc", "Part of subcommand `custom`, command that edits existing custom commands");
        put("help.command.custom.edit.usage", "edit <what to edit>");
        put("help.command.custom.edit.sub", "Methods");
        put("help.command.custom.edit.section", "???");
        put("help.command.custom.edit.shortDesc", "edits custom commands");

        put("help.command.custom.edit.code.title", "Help of subcommand `code`");
        put("help.command.custom.edit.code.desc", "Part of subcommand `edit`, command that edits code of mentioned custom command");
        put("help.command.custom.edit.code.usage", "code <name> <code>");
        put("help.command.custom.edit.code.sub", "???");
        put("help.command.custom.edit.code.section", "???");
        put("help.command.custom.edit.code.shortDesc", "edits command's code");

        put("help.command.custom.edit.scope.title", "Help of subcommand `scope`");
        put("help.command.custom.edit.scope.desc", "Part of subcommand `edit`, command that edits scope of mentioned custom command\nScope can be guild or global, guild scope will prevent the command from being used anywhere else than current guild");
        put("help.command.custom.edit.scope.usage", "scope <name> <scope>");
        put("help.command.custom.edit.scope.sub", "???");
        put("help.command.custom.edit.scope.section", "???");
        put("help.command.custom.edit.scope.shortDesc", "edits command's scope");

        put("help.command.custom.edit.access.title", "Help of subcommand `access`");
        put("help.command.custom.edit.access.desc", "Part of command `edit`, command that edits access of mentioned custom command\nAccess can be anyone, managers or owner");
        put("help.command.custom.edit.access.usage", "access <name> <access>");
        put("help.command.custom.edit.access.sub", "???");
        put("help.command.custom.edit.access.section", "???");
        put("help.command.custom.edit.access.shortDesc", "edits command's access");

        put("help.command.custom.delete.title", "Help of subcommand `delete`");
        put("help.command.custom.delete.desc", "Part of command `custom`, command that deletes custom commands");
        put("help.command.custom.delete.usage", "delete <name>");
        put("help.command.custom.delete.sub", "???");
        put("help.command.custom.delete.section", "???");
        put("help.command.custom.delete.shortDesc", "deletes custom commands");

        put("help.command.custom.show.title", "Help of subcommand `show`");
        put("help.command.custom.show.desc", "Part of command `custom`, command that shows custom command's information");
        put("help.command.custom.show.usage", "show <name>");
        put("help.command.custom.show.sub", "???");
        put("help.command.custom.show.section", "???");
        put("help.command.custom.show.shortDesc", "show custom command information");

        put("help.command.custom.list.title", "Help of subcommand `list`");
        put("help.command.custom.list.desc", "Part of command `custom`, command that shows list of all custom commands");
        put("help.command.custom.list.usage", "list");
        put("help.command.custom.list.sub", "???");
        put("help.command.custom.list.section", "???");
        put("help.command.custom.list.shortDesc", "lists custom commands");
    }};

    @Override
    public String formatKey(String key, String... args) {
        String result = translationMap.get(key);

        if(result != null) {
            for (int i = 0; i < args.length; i++) {
                result = result.replaceAll("\\["+i+"]", args[i]);
            }

            return result;
        } else return key;
    }

    @Override
    public TranslatorLanguage getLanguageEnum() {
        return TranslatorLanguage.English;
    }
}
