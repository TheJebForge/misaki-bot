package com.thejebforge.misaki.bot.localisation;

public abstract class BaseLocalisation {
    public String formatKey(String key, String... args) {
        return key;
    }
    public String translate(String input){
        return input;
    }
    public boolean translatesCommands() { return false; }
    public abstract TranslatorLanguage getLanguageEnum();
}
