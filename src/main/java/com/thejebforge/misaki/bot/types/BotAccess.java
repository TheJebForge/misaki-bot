package com.thejebforge.misaki.bot.types;

public enum BotAccess {
    ANYONE,
    MANAGERS,
    OWNER
}
