package com.thejebforge.misaki.bot.types;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Map;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class VoiceProfile {
    @JsonProperty("user_id")
    private long userId;

    @JsonProperty("guild_id")
    private long guildId;

    @JsonProperty("experience")
    private long experience;

    @JsonProperty("level")
    private long level;

    @JsonProperty("voicepoints")
    private long voicepoints;


    private long timeSpentGlobal;


    @JsonProperty("timespent")
    private void timeSpentMapping(Map<String, Object> timeSpents) {
        if(timeSpents.get("global") != null)
            timeSpentGlobal = (Integer) timeSpents.get("global");
    }

    public VoiceProfile() {

    }

    public String toString() {
        return "[" + "userId:" + userId +
                " - guildId:" + guildId +
                " - exp:" + experience +
                " - lvl:" + level +
                " - vp:" + voicepoints +
                " - tsglobal:" + timeSpentGlobal +
                "]";
    }
}
