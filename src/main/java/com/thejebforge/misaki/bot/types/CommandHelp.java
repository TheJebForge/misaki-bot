package com.thejebforge.misaki.bot.types;

public class CommandHelp {
    private String titleKey;
    private String descKey;
    private String usageKey;

    private String subTitleKey;
    private String sectionTitleKey;
    private String shortDescKey;

    private boolean childrenAsSections;
    private boolean hidden;
    private int order;


    // Constructors

    public CommandHelp(String key) {
        this(key, false, false, 1);
    }

    public CommandHelp(String key, boolean childrenAsSections) {
        this(key, childrenAsSections, false, 1);
    }

    public CommandHelp(String key, boolean childrenAsSections, boolean hidden, int order) {
        this.titleKey = "help.command." + key + ".title";
        this.descKey = "help.command." + key + ".desc";
        this.usageKey = "help.command." + key + ".usage";
        this.subTitleKey = "help.command." + key + ".sub";
        this.sectionTitleKey = "help.command." + key + ".section";
        this.shortDescKey = "help.command." + key + ".shortDesc";
        this.childrenAsSections = childrenAsSections;
        this.hidden = hidden;
        this.order = order;
    }

    // Getters and Setters

    public String getTitleKey() {
        return titleKey;
    }

    public void setTitleKey(String titleKey) {
        this.titleKey = titleKey;
    }

    public String getDescKey() {
        return descKey;
    }

    public void setDescKey(String descKey) {
        this.descKey = descKey;
    }

    public String getUsageKey() {
        return usageKey;
    }

    public void setUsageKey(String usageKey) {
        this.usageKey = usageKey;
    }

    public String getSubTitleKey() {
        return subTitleKey;
    }

    public void setSubTitleKey(String subTitleKey) {
        this.subTitleKey = subTitleKey;
    }

    public String getSectionTitleKey() {
        return sectionTitleKey;
    }

    public void setSectionTitleKey(String sectionTitleKey) {
        this.sectionTitleKey = sectionTitleKey;
    }

    public String getShortDescKey() {
        return shortDescKey;
    }

    public void setShortDescKey(String shortDescKey) {
        this.shortDescKey = shortDescKey;
    }

    public boolean childrenAsSections() {
        return childrenAsSections;
    }

    public void setChildrenAsSections(boolean childrenAsSections) {
        this.childrenAsSections = childrenAsSections;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }
}
