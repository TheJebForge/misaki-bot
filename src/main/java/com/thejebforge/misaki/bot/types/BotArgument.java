package com.thejebforge.misaki.bot.types;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface BotArgument {
    String value(); // Key for every message
    boolean checkValidity() default true;
    boolean checkExistence() default true;
    boolean takeRest() default false;
}
