package com.thejebforge.misaki.bot.types;

public enum DailyTopType {
    VOICE_POINTS,
    VOICE_LEVEL,
    VOICE_TIME
}
