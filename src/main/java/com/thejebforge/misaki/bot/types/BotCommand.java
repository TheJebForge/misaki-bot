package com.thejebforge.misaki.bot.types;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@SuppressWarnings({"UnusedDeclaration"})
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface BotCommand {
    String parent() default "";
    String id();
    String name();
    String childType() default "";
    boolean executable() default true;
}
