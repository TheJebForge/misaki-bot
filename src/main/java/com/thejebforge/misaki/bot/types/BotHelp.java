package com.thejebforge.misaki.bot.types;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface BotHelp {
    String value();
    boolean childAsSections() default false;
    boolean hidden() default false;
    int order() default 1;
}
