package com.thejebforge.misaki.bot.types;

import com.thejebforge.misaki.bot.config.Configuration;
import com.thejebforge.misaki.bot.localisation.BaseLocalisation;
import com.thejebforge.misaki.bot.modules.GenericModule;
import com.thejebforge.misaki.bot.utils.ArgumentProcessor;
import com.thejebforge.misaki.bot.utils.EmbedResponder;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Guild;
import discord4j.core.object.entity.Member;
import discord4j.core.object.entity.channel.TextChannel;
import discord4j.core.spec.EmbedCreateSpec;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;

import static com.thejebforge.misaki.runtime.MainBotProcess.db;

public class Command {
    private Command parent = null;

    private String childTypeTrnKey;
    private List<Command> children;

    private String id;
    private String name;

    private GenericModule module;

    private Method commandMethod;
    private List<Parameter> commandArguments;
    private CommandHelp commandHelp;

    private boolean executable;

    private BotAccess permission;


    private List<Command> setParents(Command parent, List<Command> children) {
        children.forEach(child -> child.setParent(parent));
        return children;
    }

    // Constructors

    public Command() {
        this.children = new ArrayList<>();
        this.commandArguments = new ArrayList<>();
    }

    public Command(String id, String name, Method commandMethod, boolean executable) {
        this(null, id, name, "", new ArrayList<>(), null, commandMethod, executable, null);
    }

    public Command(String id, String name, Method commandMethod, boolean executable, CommandHelp commandHelp) {
        this(null, id, name, "", new ArrayList<>(), null, commandMethod, executable, commandHelp);
    }

    public Command(String id, String name, GenericModule module, Method commandMethod, boolean executable) {
        this(null, id, name, "", new ArrayList<>(), module, commandMethod, executable, null);
    }

    public Command(String id, String name, GenericModule module, Method commandMethod, boolean executable, CommandHelp commandHelp) {
        this(null, id, name, "", new ArrayList<>(), module, commandMethod, executable, commandHelp);
    }

    public Command(String id, String name, String childTypeTrnKey, List<Command> children) {
        this(null, id, name, childTypeTrnKey, children, null, null, false, null);
    }

    public Command(String id, String name, String childTypeTrnKey, List<Command> children, CommandHelp commandHelp) {
        this(null, id, name, childTypeTrnKey, children, null, null, false, commandHelp);
    }

    public Command(String id, String name, String childTypeTrnKey, List<Command> children, Method commandMethod, boolean executable) {
        this(null, id, name, childTypeTrnKey, children, null, commandMethod, executable, null);
    }

    public Command(String id, String name, String childTypeTrnKey, List<Command> children, Method commandMethod, boolean executable, CommandHelp commandHelp) {
        this(null, id, name, childTypeTrnKey, children, null, commandMethod, executable, commandHelp);
    }

    public Command(String id, String name, String childTypeTrnKey, List<Command> children, GenericModule module) {
        this(null, id, name, childTypeTrnKey, children, module, null, false, null);
    }

    public Command(String id, String name, String childTypeTrnKey, List<Command> children, GenericModule module, CommandHelp commandHelp) {
        this(null, id, name, childTypeTrnKey, children, module, null, false, commandHelp);
    }

    public Command(String id, String name, String childTypeTrnKey, List<Command> children, GenericModule module, Method commandMethod, boolean executable) {
        this(null, id, name, childTypeTrnKey, children, module, commandMethod, executable, null);
    }

    public Command(String id, String name, String childTypeTrnKey, List<Command> children, GenericModule module, Method commandMethod, boolean executable, CommandHelp commandHelp) {
        this(null, id, name, childTypeTrnKey, children, module, commandMethod, executable, commandHelp);
    }

    public Command(Command parent, String id, String name, Method commandMethod, boolean executable) {
        this(parent, id, name, "", new ArrayList<>(), parent.getModule(), commandMethod, executable, null);
    }

    public Command(Command parent, String id, String name, String childTypeTrnKey, List<Command> children, GenericModule module, Method commandMethod, boolean executable, CommandHelp commandHelp) {
        this.commandArguments = new ArrayList<>();
        this.parent = parent;
        this.childTypeTrnKey = childTypeTrnKey;
        this.module = module;
        this.children = setParents(this, children);
        this.id = id;
        this.name = name;
        this.commandMethod = commandMethod;
        this.commandHelp = commandHelp;
        this.executable = executable;
        this.permission = null;
    }

    // Helper methods

    public List<String> getPath() {
        List<String> path = new ArrayList<>();

        Command node = this;

        while(node != null) {
            path.add(0, node.getName());
            node = node.getParent();
        }

        return path;
    }

    public static List<Command> setModuleReference(List<Command> commands, GenericModule module) {
        for(Command child : commands) {
            child.setModule(module);

            if(!child.getChildren().isEmpty())
                setModuleReference(child.getChildren(), module);
        }

        return commands;
    }

    public void runCommand(List<String> args, Guild guild, TextChannel channel, Member member, MessageCreateEvent eventData) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        if(executable) {
            List<Object> arguments = new ArrayList<>();

            for(Parameter parameter : commandMethod.getParameters()){
                if(commandArguments.contains(parameter)) {
                    BotArgument botArgument = parameter.getAnnotation(BotArgument.class);
                    Object argument = ArgumentProcessor.processArgument(args, parameter, commandArguments.size() == 1, trn, channel);

                    if(botArgument.checkExistence()) {
                        if (argument != null) arguments.add(argument);
                        else return;
                    } else {
                        arguments.add(argument);
                    }
                } else {
                    Class<?> argumentClass = parameter.getType();

                    if (argumentClass.equals(Command.class)) arguments.add(this);
                    if (argumentClass.equals(List.class)) arguments.add(args);
                    if (argumentClass.equals(Guild.class)) arguments.add(guild);
                    if (argumentClass.equals(TextChannel.class)) arguments.add(channel);
                    if (argumentClass.equals(Member.class)) arguments.add(member);
                    if (argumentClass.equals(MessageCreateEvent.class)) arguments.add(eventData);
                }
            }

            try {
                commandMethod.invoke(module, arguments.toArray());
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }

        }
    }

    public LinkedList<StringBuilder> constructHelp(BaseLocalisation trn, boolean toAddDash) {
        LinkedList<StringBuilder> coms = new LinkedList<>();

        children.sort(commandComparator);

        if(children.size() > 0) {
            if(isExecutable())
                if(hasHelp())
                    coms.add(new StringBuilder().append(trn.formatKey(commandHelp.getUsageKey())).append(" - ").append(trn.formatKey(commandHelp.getShortDescKey())));

            for(Command child : children) {
                if(child.hasHelp()) {
                    if (!child.commandHelp.isHidden()) {
                        LinkedList<StringBuilder> childHelp = child.constructHelp(trn, false);
                        childHelp.forEach(str -> str.insert(0, name + " "));
                        coms.addAll(childHelp);
                    }
                }
            }
        } else {
            if(hasHelp())
                coms.add(new StringBuilder().append(trn.formatKey(commandHelp.getUsageKey())).append(" - ").append(trn.formatKey(commandHelp.getShortDescKey())));
        }

        if(toAddDash) coms.forEach(str -> str.insert(0, "-"));

        return coms;
    }

    private Comparator<? super Command> commandComparator = (a, b) -> {
        if(a.hasHelp() && b.hasHelp()) {
            CommandHelp aHelp = a.commandHelp;
            CommandHelp bHelp = b.commandHelp;

            return aHelp.getOrder() - bHelp.getOrder();
        } else if(a.hasHelp()) return -1;
        else if(b.hasHelp()) return 1;
        else return 0;
    };

    public void displayHelp(Configuration config, BaseLocalisation trn, Command command, TextChannel channel) {
        if(hasHelp()) {
            String title = trn.formatKey(commandHelp.getTitleKey());
            String desc = trn.formatKey(commandHelp.getDescKey());

            String usageTitle = trn.formatKey("help.command.usage");
            String usage = trn.formatKey(commandHelp.getUsageKey());

            // Going down the parent route to add to usage
            Command parent = this.parent;
            while(parent != null){
                usage = parent.name + " " + usage;
                parent = parent.parent;
            }

            usage = config.getPrefix() + usage;

            LinkedHashMap<String, String> fields = new LinkedHashMap<>();
            fields.put(usageTitle, usage);

            // Sorting commands
            children.sort(commandComparator);

            if(children.size() > 0) {
                if(commandHelp.childrenAsSections()) {
                    for(Command child : children) {
                        if(child.hasHelp())
                            if(!child.commandHelp.isHidden())
                                fields.put(trn.formatKey(child.commandHelp.getSectionTitleKey()), String.join("\n", child.constructHelp(trn, true)));
                    }
                } else {
                    LinkedList<StringBuilder> allhelp = new LinkedList<>();

                    for(Command child : children) {
                        if(child.hasHelp())
                            if(!child.commandHelp.isHidden())
                                allhelp.addAll(child.constructHelp(trn, true));
                    }

                    fields.put(trn.formatKey(commandHelp.getSubTitleKey()), String.join("\n", allhelp));
                }

                fields.put(trn.formatKey("help.commands.furtherhelp.field"), config.getPrefix()+trn.formatKey("help.commands.furtherhelp.value"));
            }

            EmbedResponder.sendMessage(channel, true, title, desc, fields);
        }
    }


    // Getters and Setters

    public Command getParent() {
        return parent;
    }

    public void setParent(Command parent) {
        this.parent = parent;
    }

    public String getChildTypeTrnKey() {
        return childTypeTrnKey;
    }

    public void setChildTypeTrnKey(String childTypeTrnKey) {
        this.childTypeTrnKey = childTypeTrnKey;
    }

    public List<Command> getChildren() {
        return children;
    }

    public void setChildren(List<Command> children) {
        this.children = children;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GenericModule getModule() {
        return module;
    }

    public void setModule(GenericModule module) {
        this.module = module;
    }

    public boolean isExecutable() {
        return this.executable;
    }

    public boolean hasHelp() {
        return this.commandHelp != null;
    }

    public CommandHelp getCommandHelp() {
        return commandHelp;
    }

    public void setCommandHelp(CommandHelp commandHelp) {
        this.commandHelp = commandHelp;
    }

    public BotAccess getPermission() {
        return permission;
    }

    public void setPermission(BotAccess permission) {
        this.permission = permission;
    }

    public void addArgument(Parameter field) {
        this.commandArguments.add(field);
    }

    public List<Parameter> getArguments() {
        return this.commandArguments;
    }

}
