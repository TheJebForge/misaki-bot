package com.thejebforge.misaki.bot.types;

public enum CustomCommandScope {
    GLOBAL,
    GUILD
}
