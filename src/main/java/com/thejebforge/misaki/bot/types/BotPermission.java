package com.thejebforge.misaki.bot.types;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface BotPermission {
    BotAccess value() default BotAccess.ANYONE;
    boolean recursive() default false;
    boolean override() default false;
}
