package com.thejebforge.misaki.bot.events;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class BotEventHandler<T extends BotEvent> {
    public class BotEventListener {
        private final Consumer<T> consumer;

        public BotEventListener(Consumer<T> consumer) {
            this.consumer = consumer;
        }

        public void call(BotEvent event) {
            consumer.accept((T) event);
        }
    }

    private List<BotEventListener> listeners = new ArrayList<>();

    public void subscribe(Consumer<T> consumer){
        listeners.add(new BotEventListener(consumer));
    }

    public void call(BotEvent a) {
        for(BotEventListener e : listeners)
            e.call(a);
    }
}
