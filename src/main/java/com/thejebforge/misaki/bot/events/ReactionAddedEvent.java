package com.thejebforge.misaki.bot.events;

import discord4j.core.event.domain.message.ReactionAddEvent;

public class ReactionAddedEvent extends BotEvent {
    private final ReactionAddEvent event;

    public ReactionAddedEvent(ReactionAddEvent event) {
        this.event = event;
    }

    public ReactionAddEvent getEvent(){
        return event;
    }
}
