package com.thejebforge.misaki.bot.events;

import java.util.HashMap;
import java.util.Map;

public class BotEventDispatcher {
    private final Map<Class<? extends BotEvent>, BotEventHandler<? extends BotEvent>> handlers;

    public BotEventDispatcher() {
        handlers = new HashMap<>();

        handlers.put(ReactionAddedEvent.class, new BotEventHandler<ReactionAddedEvent>());
    }

    public <T extends BotEvent> BotEventHandler<T> on(Class<T> eventClass) {
        return (BotEventHandler<T>) handlers.get(eventClass);
    }
}
