package com.thejebforge.misaki.bot.config;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

public class Configuration {
    Properties prop;
    Properties secret;

    public Configuration(){
        prop = new Properties();
        secret = new Properties();

        InputStream is;

        if(Files.exists(Paths.get("config.properties"))){
            try {
                is = new FileInputStream(Paths.get("config.properties").toFile());
            } catch (FileNotFoundException e) {
                is = getClass().getClassLoader().getResourceAsStream("config.properties");
                e.printStackTrace();
            }
        } else {
            is = getClass().getClassLoader().getResourceAsStream("config.properties");
        }

        try {
            prop.load(is);
            assert is != null;
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
            System.exit(0);
        }

        InputStream secrets;

        if(Files.exists(Paths.get(prop.getProperty("secret_filename")))){
            try {
                secrets = new FileInputStream(Paths.get(prop.getProperty("secret_filename")).toFile());
            } catch (FileNotFoundException e) {
                secrets = getClass().getClassLoader().getResourceAsStream(prop.getProperty("secret_filename"));
                e.printStackTrace();
            }
        } else {
            secrets = getClass().getClassLoader().getResourceAsStream(prop.getProperty("secret_filename"));
        }

        try {
            secret.load(secrets);
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
            System.exit(0);
        }


    }

    public String getProperty(String key) {
        return prop.getProperty(key);
    }

    public String getToken() {
        return secret.getProperty("token");
    }

    public String getApiToken() {
        return secret.getProperty("api-token");
    }

    public String getDatabasePath() {
        return getProperty("db.path");
    }

    public String getPrefix() {
        return getProperty("prefix");
    }

    public Long getOwnerID() {
        return Long.parseLong(getProperty("owner"));
    }

    public String getKnownImagesFolder(){
        return getProperty("images.known_pics_folder");
    }

    public String getRandomImagesFolder(){
        return getProperty("images.random_pics_folder");
    }

    public String getDeletedImagesFolder(){
        return getProperty("images.deleted_pics_folder");
    }

    public String getAPIEndpoint() {
        return secret.getProperty("vp_server.endpoint");
    }

    public boolean toUpdateDailyTops() {
        return getProperty("dont_update_dailytops").equals("false");
    }

    public String getAPIEndpoint(String endpoint) {
        return secret.getProperty("vp_server.endpoint") + endpoint;
    }

    public String getMisakiEndpoint(String endpoint) {
        return secret.getProperty("misaki.endpoint") + endpoint;
    }
}
