package com.thejebforge.misaki.bot.modules;

import com.google.common.collect.ImmutableMap;
import com.thejebforge.misaki.bot.config.Configuration;
import com.thejebforge.misaki.bot.localisation.BaseLocalisation;
import com.thejebforge.misaki.bot.manager.DatabaseManager;
import com.thejebforge.misaki.bot.types.*;
import com.thejebforge.misaki.bot.utils.EmbedResponder;
import com.thejebforge.misaki.dao.DailyTopSubscription;
import com.thejebforge.misaki.dao.GuildData;
import com.thejebforge.misaki.runtime.MainBotProcess;
import discord4j.common.util.Snowflake;
import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Guild;
import discord4j.core.object.entity.Member;
import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.channel.GuildChannel;
import discord4j.core.object.entity.channel.TextChannel;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.util.*;

public class DailyTopModule extends GenericModule {
    private static final Logger logger = LoggerFactory.getLogger(DailyTopModule.class);
    private static final String CODENAME = "dailytop_module";

    private final Configuration config;
    private DateTime nextRun;
    private DateTime lastRun;

    public DailyTopModule(DatabaseManager db, GatewayDiscordClient gateway) {
        super(db, gateway);

        config = new Configuration();
        Timer timer = new Timer();
        DailyTopTask task = new DailyTopTask();

        lastRun = DateTime.now(DateTimeZone.UTC).minusMinutes(1);

        updateNextRun();
        timer.scheduleAtFixedRate(task, 0, 10000);
    }

    private void updateNextRun() {
        DateTime utc = DateTime.now(DateTimeZone.UTC);

        DateTime nextHour = new DateTime(utc.getYear(), utc.getMonthOfYear(), utc.getDayOfMonth(), utc.getHourOfDay(), 0, 0, DateTimeZone.UTC).plusHours(1);
        logger.info("Next hour at "+nextHour.toString());

        nextRun = nextHour;
    }

    private void scheduleUpdateSoon() {
        lastRun = DateTime.now(DateTimeZone.UTC);
        nextRun = DateTime.now(DateTimeZone.UTC).plusSeconds(3);
    }

    private LinkedHashMap<String, String> getTop(BaseLocalisation trn, Guild guild, DailyTopType type) {
        // sort
        LinkedHashMap<String, String> fields = new LinkedHashMap<>();

        if(type == DailyTopType.VOICE_LEVEL) {
            List<VoiceProfile> voiceProfiles = MainBotProcess.vp.getLevelTop(guild);

            for(int i = 0; i < Math.min(5, voiceProfiles.size()); i++) {
                VoiceProfile voiceProfile = voiceProfiles.get(i);
                Member member = guild.getMemberById(Snowflake.of(voiceProfile.getUserId())).block();

                if(member != null) {
                    long nextLevel = (10 + voiceProfile.getLevel()) * 10 * voiceProfile.getLevel() * voiceProfile.getLevel();
                    int percentage = (int)Math.floor((double) voiceProfile.getExperience() / nextLevel * 100);

                    fields.put(trn.formatKey("dailytop.voice_level.field", Integer.toString(i + 1), member.getUserData().username() + "#" + member.getUserData().discriminator()), trn.formatKey("dailytop.voice_level.value", Long.toString(voiceProfile.getLevel()), Long.toString(voiceProfile.getExperience()), percentage + "%"));
                }
            }
        } else if(type == DailyTopType.VOICE_POINTS) {
            List<VoiceProfile> voiceProfiles = MainBotProcess.vp.getPointsTop(guild);

            for(int i = 0; i < Math.min(5, voiceProfiles.size()); i++) {
                VoiceProfile voiceProfile = voiceProfiles.get(i);
                Member member = guild.getMemberById(Snowflake.of(voiceProfile.getUserId())).block();

                if(member != null) {
                    fields.put(trn.formatKey("dailytop.voice_points.field", Integer.toString(i + 1), member.getUserData().username() + "#" + member.getUserData().discriminator()) , trn.formatKey("dailytop.voice_points.value", Long.toString(voiceProfile.getVoicepoints())));
                }
            }
        } else if(type == DailyTopType.VOICE_TIME) {
            List<VoiceProfile> voiceProfiles = MainBotProcess.vp.getTimeTop(guild);

            for(int i = 0; i < Math.min(5, voiceProfiles.size()); i++) {
                VoiceProfile voiceProfile = voiceProfiles.get(i);
                Member member = guild.getMemberById(Snowflake.of(voiceProfile.getUserId())).block();

                if(member != null) {
                    long totalSeconds = voiceProfile.getTimeSpentGlobal();
                    long days = (long)Math.floor((double)totalSeconds / (3600 * 24));
                    totalSeconds %= 60 * 60 * 24;

                    String hours = ("0" + ((long)Math.floor((double)totalSeconds / 3600)));
                    hours = hours.substring(hours.length() - 2);

                    totalSeconds %= 3600;

                    String minutes = ("0" + ((long)Math.floor((double)totalSeconds / 60)));
                    minutes = minutes.substring(minutes.length() - 2);

                    String seconds = ("0" + (totalSeconds % 60));
                    seconds = seconds.substring(seconds.length() - 2);

                    String time = hours + ":" + minutes + ":" + seconds;

                    fields.put(trn.formatKey("dailytop.voice_time.field", Integer.toString(i + 1), member.getUserData().username() + "#" + member.getUserData().discriminator()) , trn.formatKey("dailytop.voice_time.value", Long.toString(days), time));
                }
            }
        }

        return fields;
    }



    private class DailyTopTask extends TimerTask {
        @Override
        public void run() {
            try {
                if (config.toUpdateDailyTops()) {
                    if (DateTime.now(DateTimeZone.UTC).isAfter(nextRun)) {
                        lastRun = DateTime.now(DateTimeZone.UTC);
                        updateNextRun();

                        List<DailyTopSubscription> subscriptions = db.listDailyTopSubscriptions();

                        for (DailyTopSubscription sub : subscriptions) {
                            Message message = gateway.getMessageById(Snowflake.of(sub.getChannelId()), Snowflake.of(sub.getMessageId())).block();
                            String type = sub.getType().toString().toLowerCase();

                            if (message != null) {
                                Guild guild = message.getGuild().block();

                                if (guild != null) {
                                    GuildData guildData = db.getGuild(guild);
                                    BaseLocalisation trn = guildData.getTranslator();

                                    EmbedResponder.editMessage(message, true, trn.formatKey("dailytop." + type + ".title"), trn.formatKey("dailytop." + type + ".desc"), getTop(trn, guild, sub.getType()), Instant.now());
                                } else {
                                    logger.info("Guild Id wasn't found in message " + message.getId().asLong());
                                }
                            }
                        }

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private boolean isManager(Member user, Guild guild){
        GuildData data = db.getGuild(guild);
        return data.isManager(user);
    }

    @Override
    public String getCodeName() {
        return CODENAME;
    }

    @Override
    public String getDisplayKey() {
        return "dailytop.name";
    }

    @Override
    public int getPermissionLevelRequired() {
        return 2;
    }

    @BotHelp(value = "dailytop", childAsSections = true)
    @BotCommand(id = "dailytop", name = "dailytop", childType = "method", executable = false)
    public void dailyTopsMain() {}


    // points tops
    @BotPermission(value = BotAccess.MANAGERS, recursive = true)
    @BotHelp("dailytop.voicepoints")
    @BotCommand(id = "dailytop-voicepoints", name = "points", childType = "method", executable = false, parent = "dailytop")
    public void pointsSection() {}

    @BotHelp("dailytop.voicepoints.here")
    @BotCommand(id = "dailytop-voicepoints-here", name = "here", parent = "dailytop-voicepoints")
    public void pointsHere(Command command, Guild guild, TextChannel channel, Member member) {
        processHere(command, guild, channel, member);
    }

    @BotHelp("dailytop.voicepoints.clear")
    @BotCommand(id = "dailytop-voicepoints-clear", name = "clear", parent = "dailytop-voicepoints")
    public void pointsClear(Command command, Guild guild, TextChannel channel, Member member) {
        processHere(command, guild, channel, member);
    }


    // levels tops
    @BotPermission(value = BotAccess.MANAGERS, recursive = true)
    @BotHelp("dailytop.voicelevels")
    @BotCommand(id = "dailytop-voicelevels", name = "levels", childType = "method", executable = false, parent = "dailytop")
    public void levelsSection() {}

    @BotHelp("dailytop.voicelevels.here")
    @BotCommand(id = "dailytop-voicelevels-here", name = "here", parent = "dailytop-voicelevels")
    public void levelsHere(Command command, Guild guild, TextChannel channel, Member member) {
        processHere(command, guild, channel, member);
    }

    @BotHelp("dailytop.voicelevels.clear")
    @BotCommand(id = "dailytop-voicelevels-clear", name = "clear", parent = "dailytop-voicelevels")
    public void levelsClear(Command command, Guild guild, TextChannel channel, Member member) {
        processHere(command, guild, channel, member);
    }


    // levels tops
    @BotPermission(value = BotAccess.MANAGERS, recursive = true)
    @BotHelp("dailytop.voicetime")
    @BotCommand(id = "dailytop-voicetime", name = "time", childType = "method", executable = false, parent = "dailytop")
    public void timeSection() {}

    @BotHelp("dailytop.voicetime.here")
    @BotCommand(id = "dailytop-voicetime-here", name = "here", parent = "dailytop-voicetime")
    public void timeHere(Command command, Guild guild, TextChannel channel, Member member) {
        processHere(command, guild, channel, member);
    }

    @BotHelp("dailytop.voicetime.clear")
    @BotCommand(id = "dailytop-voicetime-clear", name = "clear", parent = "dailytop-voicetime")
    public void timeClear(Command command, Guild guild, TextChannel channel, Member member) {
        processHere(command, guild, channel, member);
    }


    public void processHere(Command command, Guild guild, TextChannel channel, Member member) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        Map<String, DailyTopType> types = ImmutableMap.<String, DailyTopType>builder()
                .put("dailytop-voicepoints-here", DailyTopType.VOICE_POINTS)
                .put("dailytop-voicelevels-here", DailyTopType.VOICE_LEVEL)
                .put("dailytop-voicetime-here", DailyTopType.VOICE_TIME)
                .build();

        DailyTopType type = types.get(command.getId());

        Message message = EmbedResponder.sendMessage(channel, true, trn.formatKey("dailytop.scheduling.title"), trn.formatKey("dailytop.scheduling.desc"));

        DailyTopSubscription sub = new DailyTopSubscription(message, guild, type);
        db.addDailyTopSubscription(sub);

        scheduleUpdateSoon();
    }

    public void processClear(Command command, List<String> args, Guild guild, TextChannel channel, Member member) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        Map<String, DailyTopType> types = ImmutableMap.<String, DailyTopType>builder()
                .put("dailytop-voicepoints-clear", DailyTopType.VOICE_POINTS)
                .put("dailytop-voicelevels-clear", DailyTopType.VOICE_LEVEL)
                .put("dailytop-voicetime-clear", DailyTopType.VOICE_TIME)
                .build();

        DailyTopType type = types.get(command.getId());
        String typeStr = type.name().toLowerCase();

        Map<DailyTopType, String> subcommand = ImmutableMap.<DailyTopType, String>builder()
                .put(DailyTopType.VOICE_POINTS, "points")
                .put(DailyTopType.VOICE_LEVEL, "levels")
                .put(DailyTopType.VOICE_TIME, "time")
                .build();

        if (args.size() > 0) {
            if (args.remove(0).equals("yes")) {
                List<DailyTopSubscription> subs = db.listDailyTopSubscriptions();

                subs.removeIf(sub -> sub.getGuildId() != guild.getId().asLong());
                subs.removeIf(sub -> !sub.getType().equals(type));
                subs.removeIf(sub -> sub.getChannelId() != channel.getId().asLong());

                logger.info("cleared " + subs.size() + " "+type.name().toLowerCase()+" tops");

                for (DailyTopSubscription sub : subs) {
                    GuildChannel subChannel = guild.getChannelById(Snowflake.of(sub.getChannelId())).block();
                    TextChannel subTextChannel = (TextChannel) subChannel;

                    if (subTextChannel != null) {
                        if (subTextChannel.getId().equals(channel.getId())) {
                            Message message = subTextChannel.getMessageById(Snowflake.of(sub.getMessageId())).block();

                            if (message != null) {
                                message.delete().block();

                                db.removeDailyTopSubscription(sub);
                            }
                        }
                    }
                }

                EmbedResponder.sendMessage(channel, true, trn.formatKey("dailytop."+typeStr+".cleared.title"), trn.formatKey("dailytop."+typeStr+".cleared.desc"));
            } else {
                EmbedResponder.sendMessage(channel, false, trn.formatKey("dailytop."+typeStr+".clear.confirm.title"), trn.formatKey("dailytop."+typeStr+".clear.confirm.desc"), new LinkedHashMap<>(ImmutableMap.<String, String>builder()
                        .put(trn.formatKey("dailytop."+typeStr+".clear.confirm.field"), config.getPrefix() + "dailytop "+subcommand.get(type)+" clear yes")
                        .build()));
            }
        } else {
            EmbedResponder.sendMessage(channel, false, trn.formatKey("dailytop."+typeStr+".clear.confirm.title"), trn.formatKey("dailytop."+typeStr+".clear.confirm.desc"), new LinkedHashMap<>(ImmutableMap.<String, String>builder()
                    .put(trn.formatKey("dailytop."+typeStr+".clear.confirm.field"), config.getPrefix() + "dailytop "+subcommand.get(type)+" clear yes")
                    .build()));
        }
    }

    @BotHelp(value = "dailytop.update", order = 2)
    @BotCommand(id = "dailytop-update", name = "update", parent = "dailytop")
    public void processUpdate(Command command, List<String> args, Guild guild, TextChannel channel, Member member, MessageCreateEvent eventData) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        DateTime now = DateTime.now(DateTimeZone.UTC);
        DateTime whenToAllow = lastRun.plusSeconds(30);
        if (whenToAllow.isBefore(now)) {
            scheduleUpdateSoon();
            EmbedResponder.sendMessage(channel, true, trn.formatKey("dailytop.scheduled.title"), trn.formatKey("dailytop.scheduled.desc"));
        } else {
            EmbedResponder.sendMessage(channel, false, trn.formatKey("error.dailytop.too_soon.title"), trn.formatKey("error.dailytop.too_soon.desc"));
        }
    }
}
