package com.thejebforge.misaki.bot.modules;

import com.google.common.collect.ImmutableMap;
import com.thejebforge.misaki.bot.config.Configuration;
import com.thejebforge.misaki.bot.localisation.BaseLocalisation;
import com.thejebforge.misaki.bot.manager.DatabaseManager;
import com.thejebforge.misaki.bot.types.BotArgument;
import com.thejebforge.misaki.bot.types.BotCommand;
import com.thejebforge.misaki.bot.types.BotHelp;
import com.thejebforge.misaki.bot.types.Command;
import com.thejebforge.misaki.bot.utils.CommandHelper;
import com.thejebforge.misaki.bot.utils.EmbedResponder;
import com.thejebforge.misaki.dao.ImageData;
import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Attachment;
import discord4j.core.object.entity.Guild;
import discord4j.core.object.entity.Member;
import discord4j.core.object.entity.channel.TextChannel;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;
import java.util.stream.Stream;

public class ImageModule extends GenericModule {
    private static final Logger logger = LoggerFactory.getLogger(ImageModule.class);
    private static final String CODENAME = "image_module";

    private final Configuration config;

    private ImageData recentImage = null;

    private List<ImageData> currentSearch = null;
    private ListIterator<ImageData> currentSearchIterator = null;

    private Path currentTaggingFile = null;

    public ImageModule(DatabaseManager db, GatewayDiscordClient gateway) {
        super(db, gateway);

        config = new Configuration();
    }

    @Override
    public String getCodeName() {
        return CODENAME;
    }

    @Override
    public String getDisplayKey() {
        return "imagemanager.name";
    }

    @Override
    public int getPermissionLevelRequired() {
        return 10;
    }


    // Command methods

    @BotHelp("images")
    @BotCommand(id = "images", name = "images", executable = false, childType = "method")
    public void imagesMain() {}

    @BotHelp("images.stats")
    @BotCommand(id = "images-stats", name = "stats", parent = "images")
    public void processImagesStats(Guild guild, TextChannel channel) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        EmbedResponder.sendMessage(channel, true, trn.formatKey("images.stats.title"), trn.formatKey("images.stats.desc", Integer.toString(db.getAllImages().size())));
    }

    @BotHelp("images.tags")
    @BotCommand(id = "images-tags", name = "tags", parent = "images")
    public void processImagesTags(Guild guild, TextChannel channel) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        String tagsString = String.join(",", db.getAllImageTags());
        EmbedResponder.sendMessage(channel, true, trn.formatKey("images.tags.title"), tagsString.substring(0, Math.min(2000, tagsString.length())), new LinkedHashMap<>(ImmutableMap.<String, String>builder().put(trn.formatKey("images.tags.field"), trn.formatKey("images.tags.value")).build()));
    }

    @BotHelp("add")
    @BotCommand(id = "add", name = "add")
    public void processAdd(@BotArgument("image.add") List<String> args, Guild guild, TextChannel channel, MessageCreateEvent eventData) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        Optional<Attachment> optional = eventData.getMessage().getAttachments().stream().findFirst();

        String url;

        if(optional.isPresent()){
            url = optional.get().getUrl();
        } else {
            String arg = args.remove(0);

            if(!CommandHelper.checkIfArgumentIsSane(trn, channel, arg)) return;

            url = arg;
        }

        if(args.size() > 0){
            Set<String> tags = new HashSet<>(args);

            if(!CommandHelper.checkSetOfArguments(trn, channel, tags)) return;

            logger.info(url);

            String[] splitUrl = url.split("\\.");

            String extension = splitUrl[splitUrl.length - 1];

            String filename = hashFilename(url) + "." + extension;

            try {
                HttpURLConnection httpcon = (HttpURLConnection) new URL(url).openConnection();
                httpcon.addRequestProperty("User-Agent", "Mozilla/4.0");
                InputStream in = httpcon.getInputStream();
                Files.copy(in, Paths.get(config.getKnownImagesFolder() + "/" + filename), StandardCopyOption.REPLACE_EXISTING);
                in.close();
            } catch (IOException e) {
                EmbedResponder.sendMessage(channel, false, trn.formatKey("error.images.download.failed.title"), trn.formatKey("error.images.download.failed.desc"));
                e.printStackTrace();
                return;
            }

            ImageData data = new ImageData(filename, tags);

            recentImage = data;

            db.addImage(data);

            EmbedResponder.sendMessage(channel, true, trn.formatKey("image.added.title"), trn.formatKey("image.added.desc"), new LinkedHashMap<>(ImmutableMap.<String, String>builder()
                    .put(trn.formatKey("image.added.field"), String.join(", ", tags))
                    .build()), Paths.get(config.getKnownImagesFolder() + "/" + filename));
        } else {
            EmbedResponder.sendMessage(channel, false, trn.formatKey("error.missing.tags.image_add.title"), trn.formatKey("error.missing.tags.image_add.desc", CommandHelper.helpDirection(config, "add")));
        }
    }

    @BotHelp("edit")
    @BotCommand(id = "edit", name = "edit")
    public void processEdit(@BotArgument("tags.image_edit") List<String> args, Guild guild, TextChannel channel) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        if(recentImage != null) {
            Set<String> tags = new HashSet<>(args);

            if(!CommandHelper.checkSetOfArguments(trn, channel, tags)) return;

            db.addImage(new ImageData(recentImage.getFilename(), tags));

            EmbedResponder.sendMessage(channel, true, trn.formatKey("image.edited.title"), trn.formatKey("image.edited.desc"), new LinkedHashMap<>(ImmutableMap.<String, String>builder()
                    .put(trn.formatKey("image.edited.field"), String.join(", ", tags))
                    .build()), Paths.get(config.getKnownImagesFolder() + "/" + recentImage.getFilename()));
        } else {
            EmbedResponder.sendMessage(channel, false, trn.formatKey("error.missing.recent.image.title"), trn.formatKey("error.missing.recent.image.desc"));
        }
    }

    @BotHelp("delete")
    @BotCommand(id = "delete", name = "delete")
    public void processDelete(@BotArgument(value = "args", checkExistence = false) String confirm, Guild guild, TextChannel channel) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        if(recentImage != null) {
            if(confirm != null && confirm.equals("yes")) {
                db.deleteImage(recentImage);

                if(currentSearch != null){
                    currentSearch.remove(recentImage);
                    currentSearchIterator = currentSearch.listIterator();
                }

                try {
                    Files.move(Paths.get(config.getKnownImagesFolder() + "/" + recentImage.getFilename()), Paths.get(config.getDeletedImagesFolder() + "/" + recentImage.getFilename()));
                    recentImage = null;
                    EmbedResponder.sendMessage(channel, true, trn.formatKey("image.deleted.title"), trn.formatKey("image.deleted.desc"));
                } catch (IOException e) {
                    EmbedResponder.sendMessage(channel, false, trn.formatKey("error.images.delete.failed.title"), trn.formatKey("error.images.delete.failed.desc"));
                    e.printStackTrace();
                }
            } else {
                EmbedResponder.sendMessage(channel, false, trn.formatKey("image.delete.confirm.title"), trn.formatKey("image.delete.confirm.desc"), new LinkedHashMap<>(ImmutableMap.<String, String>builder().put(trn.formatKey("image.delete.confirm.field"), config.getPrefix()+"delete yes").build()), Paths.get(config.getKnownImagesFolder() + "/" + recentImage.getFilename()));
            }
        } else {
            EmbedResponder.sendMessage(channel, false, trn.formatKey("error.missing.recent.image.title"), trn.formatKey("error.missing.recent.image.desc"));
        }
    }

    @BotHelp("recent")
    @BotCommand(id = "recent", name = "recent")
    public void processRecent(Command command, List<String> args, Guild guild, TextChannel channel) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        if(recentImage != null) {
            EmbedResponder.sendMessage(channel, true, trn.formatKey("image.recent.title"), trn.formatKey("image.recent.desc"), Paths.get(config.getKnownImagesFolder() + "/" + recentImage.getFilename()));
        } else {
            EmbedResponder.sendMessage(channel, false, trn.formatKey("error.missing.recent.image.title"), trn.formatKey("error.missing.recent.image.desc"));
        }
    }

    @BotHelp("search")
    @BotCommand(id = "search", name = "search")
    public void processSearch(@BotArgument("tags.image_search") List<String> args, Guild guild, TextChannel channel) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        if(args.size() > 0){
            Set<String> tags = new HashSet<>(args);

            if(!CommandHelper.checkSetOfArguments(trn, channel, tags)) return;

            List<ImageData> images = db.getImagesByTags(tags);

            if(images.size() > 0) {
                currentSearch = images;
                currentSearchIterator = currentSearch.listIterator();

                EmbedResponder.sendMessage(channel, true, trn.formatKey("image.search.found.title", Integer.toString(images.size())), new LinkedHashMap<>(ImmutableMap.<String, String>builder()
                        .put(trn.formatKey("image.search.found.tags"), String.join(", ", tags))
                        .put(trn.formatKey("image.search.found.next"), config.getPrefix()+"next")
                        .put(trn.formatKey("image.search.found.random"), config.getPrefix()+"random")
                        .build()));
            } else {
                EmbedResponder.sendMessage(channel, false, trn.formatKey("image.search.nothing.title"), trn.formatKey("image.search.nothing.desc"), new LinkedHashMap<>(ImmutableMap.<String, String>builder()
                        .put(trn.formatKey("image.search.nothing.field"), String.join(", ", tags))
                        .build()));
            }
        } else {
            EmbedResponder.sendMessage(channel, false, trn.formatKey("error.missing.tags.image_search.title"), trn.formatKey("error.missing.tags.image_search.desc", CommandHelper.helpDirection(config, "search")));
        }
    }

    @BotHelp("next")
    @BotCommand(id = "next", name = "next")
    public void processNext(Guild guild, TextChannel channel) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        if(currentSearchIterator != null){
            if(!currentSearchIterator.hasNext()){
                currentSearchIterator = currentSearch.listIterator();
            }

            if(currentSearchIterator.hasNext()) {
                ImageData image = currentSearchIterator.next();
                recentImage = image;

                if (image != null) {
                    EmbedResponder.sendMessage(channel, true, new LinkedHashMap<>(ImmutableMap.<String, String>builder().put(trn.formatKey("image.next.tags"), String.join(", ", image.getTags())).build()), Paths.get(config.getKnownImagesFolder() + "/" + image.getFilename()));
                } else {
                    logger.info("Something is wrong");
                }
            } else {
                EmbedResponder.sendMessage(channel, false, trn.formatKey("error.empty.search.title"), trn.formatKey("error.empty.search.desc"));
            }
        } else {
            EmbedResponder.sendMessage(channel, false, trn.formatKey("error.missing.search.next.title"), trn.formatKey("error.missing.search.next.desc", CommandHelper.helpDirection(config, "search")));
        }
    }

    @BotHelp("random")
    @BotCommand(id = "random", name = "random")
    public void processRandom(@BotArgument(value = "args", checkExistence = false) String confirm, Guild guild, TextChannel channel, Member member, MessageCreateEvent eventData) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        if(confirm != null){
            if(confirm.equals("all")) {
                List<ImageData> allImages = db.getAllImages();

                Random rnd = new Random();
                if (allImages.size() > 0) {
                    ImageData image = allImages.get(rnd.nextInt(allImages.size()));

                    recentImage = image;

                    EmbedResponder.sendMessage(channel, true, new LinkedHashMap<>(ImmutableMap.<String, String>builder().put(trn.formatKey("image.random.tags"), String.join(", ", image.getTags())).build()), Paths.get(config.getKnownImagesFolder() + "/" + image.getFilename()));
                } else {
                    EmbedResponder.sendMessage(channel, false, trn.formatKey("error.empty.imagedb.title"), trn.formatKey("error.empty.imagedb.desc", CommandHelper.helpDirection(config, "add")));
                }
            } else {
                EmbedResponder.sendMessage(channel, false, trn.formatKey("error.random.bad_argument.title"), trn.formatKey("error.random.bad_argument.desc", CommandHelper.helpDirection(config, "random")));
            }
        } else {
            if(currentSearch != null) {
                Random rnd = new Random();

                if(currentSearch.size() > 0){
                    ImageData image = currentSearch.get(rnd.nextInt(currentSearch.size()));

                    recentImage = image;

                    EmbedResponder.sendMessage(channel, true, new LinkedHashMap<>(ImmutableMap.<String, String>builder().put(trn.formatKey("image.random.tags"), String.join(", ", image.getTags())).build()), Paths.get(config.getKnownImagesFolder() + "/" + image.getFilename()));
                } else {
                    EmbedResponder.sendMessage(channel, false, trn.formatKey("error.empty.search.title"), trn.formatKey("error.empty.search.desc"));
                }
            } else {
                EmbedResponder.sendMessage(channel, false, trn.formatKey("error.missing.search.random.title"), trn.formatKey("error.missing.search.random.desc", CommandHelper.helpDirection(config, "search")));
            }
        }
    }

    @BotHelp("tag")
    @BotCommand(id = "tag", name = "tag")
    public void processTag(@BotArgument(value = "args", checkExistence = false) List<String> args, Guild guild, TextChannel channel, Member member, MessageCreateEvent eventData) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        if(args != null){
            if(currentTaggingFile != null) {
                Set<String> tags = new HashSet<>(args);

                if(!CommandHelper.checkSetOfArguments(trn, channel, tags)) return;

                String[] splitFile = currentTaggingFile.getFileName().toString().split("\\.");

                String extension = splitFile[splitFile.length - 1];

                String filename = hashFilename(currentTaggingFile.toString()) + "." + extension;

                try {
                    Path target = Paths.get(config.getKnownImagesFolder() + "/" + filename);
                    Files.move(currentTaggingFile, target);

                    ImageData image = new ImageData(filename, tags);
                    recentImage = image;
                    db.addImage(image);

                    EmbedResponder.sendMessage(channel, true, trn.formatKey("image.added.title"), trn.formatKey("image.added.desc"), new LinkedHashMap<>(ImmutableMap.<String, String>builder()
                            .put(trn.formatKey("image.added.field"), String.join(", ", tags))
                            .build()), target);

                    currentTaggingFile = null;
                } catch (IOException e) {
                    EmbedResponder.sendMessage(channel, false, trn.formatKey("error.images.add.failed.title"), trn.formatKey("error.images.add.failed.desc"));
                    e.printStackTrace();
                }
            } else {
                EmbedResponder.sendMessage(channel, false, trn.formatKey("error.missing.image.tag.title"), trn.formatKey("error.missing.image.tag.desc", CommandHelper.helpDirection(config, "tag")));
            }
        } else {
            try {
                Stream<Path> files = Files.list(Paths.get(config.getRandomImagesFolder()));

                Optional<Path> optional = files.findAny();

                if(optional.isPresent()){
                    Path file = optional.get();
                    currentTaggingFile = file;
                    EmbedResponder.sendMessage(channel, true, trn.formatKey("image.tag.title"), trn.formatKey("image.tag.desc", CommandHelper.helpDirection(config, "tag")), file);
                } else {
                    EmbedResponder.sendMessage(channel, false, trn.formatKey("error.missing.random.title"), trn.formatKey("error.missing.random.desc"));
                }
            } catch (IOException e) {
                EmbedResponder.sendMessage(channel, false, trn.formatKey("error.images.random.find_fail.title"), trn.formatKey("error.images.random.find_fail.desc"));
                e.printStackTrace();
            }
        }
    }

    private String hashFilename(String filename) {
        return DigestUtils.sha1Hex(filename);
    }
}
