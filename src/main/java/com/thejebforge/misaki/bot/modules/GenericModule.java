package com.thejebforge.misaki.bot.modules;

import com.thejebforge.misaki.bot.manager.DatabaseManager;
import discord4j.core.GatewayDiscordClient;

public abstract class GenericModule {
    protected DatabaseManager db;
    protected GatewayDiscordClient gateway;

    public GenericModule(DatabaseManager db, GatewayDiscordClient gateway){
        this.db = db;
        this.gateway = gateway;
    }

    public void init(){}

    public abstract String getCodeName();
    public abstract String getDisplayKey();
    public abstract int getPermissionLevelRequired();
    public boolean canBeDisabled() { return true; }
}