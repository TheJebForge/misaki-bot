package com.thejebforge.misaki.bot.modules;

import com.thejebforge.misaki.bot.localisation.BaseLocalisation;
import com.thejebforge.misaki.bot.manager.DatabaseManager;
import com.thejebforge.misaki.bot.types.*;
import com.thejebforge.misaki.bot.utils.CommandHelper;
import com.thejebforge.misaki.bot.utils.EmbedResponder;
import com.thejebforge.misaki.bot.utils.LuaHelper;
import com.thejebforge.misaki.dao.CustomCommand;
import com.thejebforge.misaki.runtime.MainBotProcess;
import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Guild;
import discord4j.core.object.entity.Member;
import discord4j.core.object.entity.channel.TextChannel;
import org.apache.commons.io.IOUtils;
import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaError;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.jse.CoerceJavaToLua;
import org.luaj.vm2.lib.jse.JsePlatform;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class CustomCommandsModule extends GenericModule {
    public CustomCommandsModule(DatabaseManager db, GatewayDiscordClient gateway) {
        super(db, gateway);

        injectCommands();
    }

    @Override
    public String getCodeName() {
        return "custom_commands_module";
    }

    @Override
    public String getDisplayKey() {
        return "custom.commands.name";
    }

    @Override
    public int getPermissionLevelRequired() {
        return 1;
    }

    @BotPermission(value = BotAccess.OWNER, recursive = true)
    @BotHelp("custom")
    @BotCommand(id = "custom", name = "custom", executable = false, childType = "method")
    public void customMain() {}

    @BotHelp("custom.add")
    @BotCommand(id = "custom-add", name = "add", parent = "custom")
    public void processCommandAdd(@BotArgument("custom.name") String name, @BotArgument("custom.scope") CustomCommandScope scope, @BotArgument("custom.access") BotAccess access, @BotArgument(value = "custom.code", takeRest = true, checkValidity = false) String code, Guild guild, TextChannel channel, MessageCreateEvent event) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        if (MainBotProcess.commandMap.get(name) == null) {
            Globals globals = JsePlatform.standardGlobals();
            try {
                globals.load(code);

                CustomCommand command = new CustomCommand(name, scope, access, guild.getId().asLong(), code);
                db.addCommand(command);

                injectCommands();

                EmbedResponder.sendMessage(channel, true, trn.formatKey("custom.added.title"), trn.formatKey("custom.added.desc", name));
            } catch (LuaError e) {
                EmbedResponder.sendMessage(channel, false, trn.formatKey("error.custom.lua.syntax.title"), trn.formatKey("error.custom.lua.syntax.desc", e.getMessage()));
            }
        } else {
            EmbedResponder.sendMessage(channel, false, trn.formatKey("error.custom.name.taken.title"), trn.formatKey("error.custom.name.taken.desc"));
        }
    }

    @BotHelp("custom.edit")
    @BotCommand(id = "custom-edit", name = "edit", parent = "custom", executable = false, childType = "method")
    public void customEdit() {}

    @BotHelp("custom.edit.code")
    @BotCommand(id = "custom-edit-code", name = "code", parent = "custom-edit")
    public void processCommandEditCode(@BotArgument("custom.name") String name, @BotArgument(value = "custom.code", takeRest = true, checkValidity = false) String code, Guild guild, TextChannel channel, MessageCreateEvent event) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        CustomCommand command = db.getCommand(name);

        if(command != null) {

            Globals globals = JsePlatform.standardGlobals();
            try {
                globals.load(code);

                command.setCode(code);
                db.addCommand(command);

                EmbedResponder.sendMessage(channel, true, trn.formatKey("custom.edited.title"), trn.formatKey("custom.edited.desc"));
            } catch (LuaError e) {
                EmbedResponder.sendMessage(channel, false, trn.formatKey("error.custom.lua.syntax.title"), trn.formatKey("error.custom.lua.syntax.desc", e.getMessage()));
            }
        } else {
            EmbedResponder.sendMessage(channel, false, trn.formatKey("error.unknown.custom.name.title"), trn.formatKey("error.unknown.custom.name.desc"));
        }
    }

    @BotHelp("custom.edit.scope")
    @BotCommand(id = "custom-edit-scope", name = "scope", parent = "custom-edit")
    public void processCommandEditScope(@BotArgument("custom.name") String name, @BotArgument("custom.scope") CustomCommandScope scope, Guild guild, TextChannel channel, MessageCreateEvent event) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        CustomCommand command = db.getCommand(name);

        if(command != null) {
            command.setScope(scope);
            command.setGuildId(guild.getId().asLong());
            db.addCommand(command);

            EmbedResponder.sendMessage(channel, true, trn.formatKey("custom.edited.title"), trn.formatKey("custom.edited.desc"));
        } else {
            EmbedResponder.sendMessage(channel, false, trn.formatKey("error.unknown.custom.name.title"), trn.formatKey("error.unknown.custom.name.desc"));
        }
    }

    @BotHelp("custom.edit.access")
    @BotCommand(id = "custom-edit-access", name = "access", parent = "custom-edit")
    public void processCommandEditAccess(@BotArgument("custom.name") String name, @BotArgument("custom.access") BotAccess access, Guild guild, TextChannel channel, MessageCreateEvent event) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        CustomCommand command = db.getCommand(name);

        if(command != null) {
            command.setAccess(access);
            db.addCommand(command);

            EmbedResponder.sendMessage(channel, true, trn.formatKey("custom.edited.title"), trn.formatKey("custom.edited.desc"));
        } else {
            EmbedResponder.sendMessage(channel, false, trn.formatKey("error.unknown.custom.name.title"), trn.formatKey("error.unknown.custom.name.desc"));
        }
    }

    @BotHelp("custom.delete")
    @BotCommand(id = "custom-delete", name = "delete", parent = "custom")
    public void processCommandDelete(@BotArgument("custom.name") String name, Guild guild, TextChannel channel, MessageCreateEvent event) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        if(db.commandExists(name)) {
            db.deleteCommand(name);
            MainBotProcess.commandMap.remove(name);

            EmbedResponder.sendMessage(channel, true, trn.formatKey("custom.deleted.title"), trn.formatKey("custom.deleted.desc"));
        } else {
            EmbedResponder.sendMessage(channel, false, trn.formatKey("error.unknown.custom.name.title"), trn.formatKey("error.unknown.custom.name.desc"));
        }
    }

    @BotHelp("custom.show")
    @BotCommand(id = "custom-show", name = "show", parent = "custom")
    public void processCommandShow(@BotArgument("custom.name") String name, Guild guild, TextChannel channel, MessageCreateEvent event) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        CustomCommand command = db.getCommand(name);

        if(command != null) {
            EmbedResponder.sendMessage(channel, true, trn.formatKey("custom.show.title", name), trn.formatKey("custom.show.desc", command.getScope().name(), command.getAccess().name(), command.getCode()));
        } else {
            EmbedResponder.sendMessage(channel, false, trn.formatKey("error.unknown.custom.name.title"), trn.formatKey("error.unknown.custom.name.desc"));
        }
    }

    @BotPermission(BotAccess.ANYONE)
    @BotHelp("custom.list")
    @BotCommand(id = "custom-list", name = "list", parent = "custom")
    public void processCommandList(Guild guild, TextChannel channel, MessageCreateEvent event) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        List<String> names = new ArrayList<>();

        for(CustomCommand command : db.listCommands()) {
            names.add(command.getName());
        }

        EmbedResponder.sendMessage(channel, true, trn.formatKey("custom.list.title"), String.join("\n", names));
    }

    public void processCustomCommand(Command command, @BotArgument(value = "args", checkExistence = false, checkValidity = false) List<String> args, Guild guild, TextChannel channel, Member member, MessageCreateEvent event) {
        if(args == null) args = new ArrayList<>();

        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        Globals globals = JsePlatform.standardGlobals();

        globals.set("db", CoerceJavaToLua.coerce(db));
        globals.set("gateway", CoerceJavaToLua.coerce(gateway));
        globals.set("responder", CoerceJavaToLua.coerce(EmbedResponder.class));
        globals.set("helper", CoerceJavaToLua.coerce(LuaHelper.class));
        globals.set("process", CoerceJavaToLua.coerce(MainBotProcess.class));
        globals.set("runtime", CoerceJavaToLua.coerce(Runtime.class));
        globals.set("system", CoerceJavaToLua.coerce(System.class));
        globals.set("ioutils", CoerceJavaToLua.coerce(IOUtils.class));

        globals.set("command", CoerceJavaToLua.coerce(command));
        globals.set("args", CoerceJavaToLua.coerce(args));
        globals.set("guild", CoerceJavaToLua.coerce(guild));
        globals.set("channel", CoerceJavaToLua.coerce(channel));
        globals.set("member", CoerceJavaToLua.coerce(member));
        globals.set("event", CoerceJavaToLua.coerce(event));
        globals.set("trn", CoerceJavaToLua.coerce(trn));

        CustomCommand customCommand = db.getCommand(command.getName());

        if(customCommand.getScope().equals(CustomCommandScope.GUILD)) {
            if(guild.getId().asLong() != customCommand.getGuildId()) {
                EmbedResponder.sendMessage(channel, false, trn.formatKey("error.custom.not_intended.title"), trn.formatKey("error.custom.not_intended.desc"));
                return;
            }
        }

        try {
            LuaValue code = globals.load(customCommand.getCode());

            LuaValue res = code.call();
            String resStr = res.toString();

            if(!res.isnil())
                EmbedResponder.sendMessage(channel, true, trn.formatKey("custom.executed.title"), resStr.substring(0, Math.min(resStr.length(), 2000)));
        } catch(LuaError e) {
            EmbedResponder.sendMessage(channel, false, trn.formatKey("error.custom.lua.error.title"), trn.formatKey("error.custom.lua.error.desc", e.getMessage()));
        }
    }

    private void injectCommands() {
        // Getting method reference
        Method method;

        try {
            method = getClass().getMethod("processCustomCommand", Command.class, List.class, Guild.class, TextChannel.class, Member.class, MessageCreateEvent.class);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            return;
        }

        // Injecting custom commands
        for(CustomCommand command : db.listCommands()) {
            Command commandDefinition = new Command(command.getName(), command.getName(), this, method, true);

            commandDefinition.setPermission(command.getAccess());

            MainBotProcess.commandMap.put(command.getName(), commandDefinition);
        }
    }
}
