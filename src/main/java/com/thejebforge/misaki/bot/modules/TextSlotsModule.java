package com.thejebforge.misaki.bot.modules;

import com.google.common.collect.ImmutableMap;
import com.thejebforge.misaki.bot.config.Configuration;
import com.thejebforge.misaki.bot.events.ReactionAddedEvent;
import com.thejebforge.misaki.bot.localisation.BaseLocalisation;
import com.thejebforge.misaki.bot.manager.DatabaseManager;
import com.thejebforge.misaki.bot.manager.VoiceProfileManager;
import com.thejebforge.misaki.bot.types.*;
import com.thejebforge.misaki.bot.utils.EmbedResponder;
import com.thejebforge.misaki.bot.utils.KeyGen;
import com.thejebforge.misaki.dao.GuildData;
import com.thejebforge.misaki.dao.SlotsWebLogin;
import com.thejebforge.misaki.runtime.MainBotProcess;
import com.thejebforge.misaki.slots.SlotOutcome;
import com.thejebforge.misaki.slots.SlotRoller;
import com.thejebforge.misaki.slots.SlotVariant;
import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.event.domain.message.ReactionAddEvent;
import discord4j.core.object.Embed;
import discord4j.core.object.entity.Guild;
import discord4j.core.object.entity.Member;
import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.User;
import discord4j.core.object.entity.channel.PrivateChannel;
import discord4j.core.object.entity.channel.TextChannel;
import discord4j.core.object.reaction.ReactionEmoji;
import discord4j.rest.http.client.ClientException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.time.Instant;
import java.util.*;

public class TextSlotsModule extends GenericModule {
    private static final Logger logger = LoggerFactory.getLogger(TextSlotsModule.class);
    private static final String CODENAME = "slots_module";

    private static final String spin_raw = "\uD83D\uDD04";
    private static final String close_raw = "❌";
    private static final String block_raw = "⬛";
    private static final String key_raw = "\uD83D\uDD11";

    public static final long botGuildId = 240492317679550465L;

    private static final Map<String, String> variantToEmoji = new HashMap<String, String>() {{
        put("C", "<:KEKW:704752163485646878>");
        put("1", "<:USALOV:570996636243853362>");
        put("2", "<:zeza_pokerface:794579204263116820>");
        put("3", "<:ZEZa_OY:794606274820964362>");
        put("7", "<:fleme:647828664146198539>");
        put("D", "<:n_SUPREME:811624243426230302>");
    }};

    private final Map<Message, Session> sessions;
    private final SlotRoller roller;
    private final Configuration config;

    public TextSlotsModule(DatabaseManager db, GatewayDiscordClient gateway) {
        super(db, gateway);

        sessions = new HashMap<>();
        roller = new SlotRoller();
        config = new Configuration();

        MainBotProcess.botEvents.on(ReactionAddedEvent.class).subscribe(this::reactionAdded);

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new SessionCheckerTask(), 0, 10000);
    }




    private static class Session {
        long total = 0;
        boolean spinning = false;
        Instant lastInteraction;
        long guildId;
        long userId;
        int bet;
        SlotOutcome previous = null;

        public Session(long userId, long guildId, int bet) {
            this.guildId = guildId;
            this.userId = userId;
            this.bet = bet;
            this.lastInteraction = Instant.now();
        }
    }

    private class SessionCheckerTask extends TimerTask {
        @Override
        public void run() {
            for(Message message : sessions.keySet()) {
                Session session = sessions.get(message);

                Guild guild = message.getGuild().block();

                if(guild != null) {
                    GuildData data = db.getGuild(guild);
                    BaseLocalisation trn = data.getTranslator();

                    if (Instant.now().minusSeconds(300).isAfter(session.lastInteraction)) {
                        // edit message to session expired
                        try {
                            EmbedResponder.editMessage(message, false, trn.formatKey("slots.text.title"), trn.formatKey("slots.text.after_start.desc"), new LinkedHashMap<>(ImmutableMap.<String, String>builder()
                                    .put(trn.formatKey("slots.text.timeout.key"), trn.formatKey("slots.text.timeout.value"))
                                    .put(trn.formatKey("slots.text.post.key"), session.total < 0  ? trn.formatKey("slots.text.post.lost.value", Long.toString(Math.abs(session.total))) : trn.formatKey("slots.text.post.won.value", Long.toString(session.total)))
                                    .build()));
                        } catch (ClientException e) {
                            logger.info("message was deleted for the session, ignoring");
                        }

                        sessions.remove(message);
                    }
                }
            }
        }
    }

    private class RollResultsTask extends TimerTask {
        private final BaseLocalisation trn;
        private final Message message;
        private final Session session;

        public RollResultsTask(BaseLocalisation trn, Message message, Session session) {
            this.trn = trn;
            this.message = message;
            this.session = session;
        }

        @Override
        public void run() {
            try {
                session.spinning = false;

                VoiceProfileManager vp = MainBotProcess.vp;

                VoiceProfile botVP = vp.getVoiceProfile(gateway.getSelfId().asLong(), botGuildId);

                SlotOutcome outcome = roller.roll(session.bet, session.previous, (int) (session.userId % 100000L));
                session.previous = outcome;

                int userBalanceChange = 0;
                int botBalanceChange = 0;

                userBalanceChange -= outcome.getPaid();
                botBalanceChange += outcome.getPaid();

                userBalanceChange += outcome.getEarned();
                botBalanceChange -= outcome.getEarned();

                if (botVP.getVoicepoints() + botBalanceChange < 5000)
                    botBalanceChange += Math.min(5000 + outcome.getEarned(), 5000);

                session.total += userBalanceChange;

                if (userBalanceChange != 0) vp.voicePointAdd(session.userId, session.guildId, userBalanceChange);
                if (botBalanceChange != 0) vp.voicePointAdd(gateway.getSelfId().asLong(), botGuildId, botBalanceChange);

                // Getting reel emojis
                StringBuilder builder = new StringBuilder();
                for (SlotVariant variant : outcome.getReels()) {
                    builder.append(variantToEmoji.get(variant.getCharacter()));
                }

                VoiceProfile voiceProfile = vp.getVoiceProfile(session.userId, session.guildId);

                EmbedResponder.editMessage(message, true, trn.formatKey("slots.text.title"), trn.formatKey("slots.text.after_start.desc"), new LinkedHashMap<>(ImmutableMap.<String, String>builder()
                        .put(trn.formatKey("slots.text.reels.key"), builder.toString())
                        .put(trn.formatKey("slots.text.reward.key"),
                                outcome.getStreak() > 0 ?
                                        outcome.getStreak() == 2 ?
                                                trn.formatKey("slots.text.reward.maxstreak.value", Integer.toString(outcome.getEarned()), Integer.toString(outcome.getStreak() + 1))
                                                : trn.formatKey("slots.text.reward.streak.value", Integer.toString(outcome.getEarned()), Integer.toString(outcome.getStreak() + 1))
                                        : trn.formatKey("slots.text.reward.value", Integer.toString(outcome.getEarned())))
                        .put(trn.formatKey("slots.text.bet.key"), trn.formatKey("slots.text.bet.value", Integer.toString(session.bet)))
                        .put(trn.formatKey("slots.text.balance.key"), trn.formatKey("slots.text.balance.value", Long.toString(voiceProfile.getVoicepoints())))
                        .put(trn.formatKey("slots.text.help.key"), trn.formatKey("slots.text.help.value"))
                        .build()));

                message.addReaction(ReactionEmoji.unicode(spin_raw)).delayElement(Duration.ofSeconds(2))
                        .then(message.addReaction(ReactionEmoji.unicode(close_raw))).subscribe();



            } catch (RuntimeException e) {
                EmbedResponder.editMessage(message, false, trn.formatKey("error.request.error.uno.title"), e.getMessage());
            }
        }
    }


    @Override
    public String getCodeName() {
        return CODENAME;
    }

    @Override
    public String getDisplayKey() {
        return "textslots.name";
    }

    @Override
    public int getPermissionLevelRequired() {
        return 2;
    }

    @BotPermission(value = BotAccess.ANYONE, recursive = true)
    @BotHelp("slots")
    @BotCommand(id = "slots", name = "slots", executable = false, childType = "method")
    public void slotsMain() {}

    @BotHelp("slots.bet")
    @BotCommand(id = "slots-bet", name = "bet", parent = "slots")
    public void processBet(@BotArgument("slots.bet") Integer bet, Guild guild, TextChannel channel, Member member) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        try {
            if(bet >= 5 && bet <= 500) {
                Session newSession = new Session(member.getId().asLong(), guild.getId().asLong(), bet);

                long balance = 0;
                VoiceProfile voiceProfile = MainBotProcess.vp.getVoiceProfile(newSession.userId, newSession.guildId);

                if (voiceProfile != null) balance = voiceProfile.getVoicepoints();

                Message message = EmbedResponder.sendMessage(channel, true, trn.formatKey("slots.text.title"), trn.formatKey("slots.text.before_start.desc"), new LinkedHashMap<>(ImmutableMap.<String, String>builder()
                        .put(trn.formatKey("slots.text.reels.key"), trn.formatKey("slots.text.before_start.reels"))
                        .put(trn.formatKey("slots.text.bet.key"), trn.formatKey("slots.text.bet.value", Integer.toString(bet)))
                        .put(trn.formatKey("slots.text.balance.key"), trn.formatKey("slots.text.balance.value", Long.toString(balance)))
                        .put(trn.formatKey("slots.text.help.key"), trn.formatKey("slots.text.help.value"))
                        .build()));

                message.addReaction(ReactionEmoji.unicode(spin_raw)).delayElement(Duration.ofSeconds(2))
                        .then(message.addReaction(ReactionEmoji.unicode(close_raw))).subscribe();

                sessions.put(message, newSession);
            } else {
                EmbedResponder.sendMessage(channel, false, trn.formatKey("error.limit.slots.bet.title"), trn.formatKey("error.limit.slots.bet.desc"));
            }
        } catch (RuntimeException e) {
            EmbedResponder.sendMessage(channel, false, trn.formatKey("error.request.error.uno.title"), e.getMessage());
        }
    }

    @BotHelp("slots.table")
    @BotCommand(id = "slots-table", name = "table", parent = "slots")
    public void processTable(Guild guild, TextChannel channel) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();
        EmbedResponder.sendMessage(channel, true, trn.formatKey("slots.table.title"), trn.formatKey("slots.table.desc"));
    }

    @BotHelp(value = "slots.web", order = 2)
    @BotCommand(id = "slots-web", name = "web", parent = "slots")
    public void processWeb(Guild guild, TextChannel channel) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();
        Message message = EmbedResponder.sendMessage(channel, true, trn.formatKey("slots.web.command.title"), trn.formatKey("slots.web.command.desc"), new LinkedHashMap<>(ImmutableMap.<String, String>builder()
                .put(trn.formatKey("slots.web.command.field"), config.getMisakiEndpoint("slots"))
                .build()));
        message.addReaction(ReactionEmoji.unicode(key_raw)).subscribe();
    }

    @BotHelp(value = "slots.web.deauth", order = 3)
    @BotCommand(id = "slots-web-deauth", name = "deauth", parent = "slots-web")
    public void processWebDeauth(Guild guild, TextChannel channel, Member member) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        int result = db.deleteLoginsForUser(member.getId().asLong());

        EmbedResponder.sendMessage(channel, true, trn.formatKey("slots.web.command.deauth.title"), trn.formatKey("slots.web.command.deauth.desc", Integer.toString(result)));
    }

    private void reactionAdded(ReactionAddedEvent event) {
        ReactionAddEvent reactionEvent = event.getEvent();

        Message reactedMessage = reactionEvent.getMessage().block();

        if(reactedMessage != null) {
            Session session = sessions.get(reactedMessage);

            ReactionEmoji emoji = reactionEvent.getEmoji();

            if(emoji.asUnicodeEmoji().isPresent()){
                ReactionEmoji.Unicode unicode = emoji.asUnicodeEmoji().get();

                User reactor = reactionEvent.getUser().block();

                if(reactor != null) {
                    if(reactor.getId().equals(gateway.getSelfId())) return;

                    if (unicode.getRaw().equals(key_raw)) sendKey(reactedMessage, reactor);

                    if (session != null) {
                        if (reactor.getId().asLong() == session.userId) {
                            session.lastInteraction = Instant.now();

                            if (unicode.getRaw().equals(spin_raw)) spinAtSession(reactedMessage, session);
                            if (unicode.getRaw().equals(close_raw)) closeSession(reactedMessage, session);
                        }
                    }
                }
            }
        }
    }

    private void spinAtSession(Message message, Session session) {
        Guild guild = message.getGuild().block();

        if(guild != null) {
            GuildData data = db.getGuild(guild);
            BaseLocalisation trn = data.getTranslator();

            if(session.spinning) return;

            // Removing all reactions
            message.removeAllReactions().block();

            try {

                long balance = 0;
                VoiceProfile voiceProfile = MainBotProcess.vp.getVoiceProfile(session.userId, session.guildId);

                if (voiceProfile != null) balance = voiceProfile.getVoicepoints();

                if (balance >= session.bet) {
                    session.spinning = true;

                    EmbedResponder.editMessage(message, true, trn.formatKey("slots.text.title"), trn.formatKey("slots.text.after_start.desc"), new LinkedHashMap<>(ImmutableMap.<String, String>builder()
                            .put(trn.formatKey("slots.text.reels.key"), block_raw + block_raw + block_raw)
                            .put(trn.formatKey("slots.text.reward.key"), trn.formatKey("slots.text.reward.rolling"))
                            .put(trn.formatKey("slots.text.bet.key"), trn.formatKey("slots.text.bet.value", Integer.toString(session.bet)))
                            .put(trn.formatKey("slots.text.balance.key"), trn.formatKey("slots.text.balance.value", Long.toString(balance)))
                            .put(trn.formatKey("slots.text.help.key"), trn.formatKey("slots.text.help.value"))
                            .build()));

                    new Timer().schedule(new RollResultsTask(trn, message, session), 600L);
                } else {
                    EmbedResponder.editMessage(message, false, trn.formatKey("slots.text.title"), trn.formatKey("slots.text.after_start.desc"), new LinkedHashMap<>(ImmutableMap.<String, String>builder()
                            .put(trn.formatKey("slots.text.reels.key"), trn.formatKey("slots.text.reels.no_money"))
                            .put(trn.formatKey("slots.text.bet.key"), trn.formatKey("slots.text.bet.value", Integer.toString(session.bet)))
                            .put(trn.formatKey("slots.text.balance.key"), trn.formatKey("slots.text.balance.value", Long.toString(balance)))
                            .put(trn.formatKey("slots.text.help.key"), trn.formatKey("slots.text.help.value"))
                            .build()));

                    message.addReaction(ReactionEmoji.unicode(spin_raw)).delayElement(Duration.ofSeconds(2))
                            .then(message.addReaction(ReactionEmoji.unicode(close_raw))).subscribe();
                }

            } catch (RuntimeException e) {
                EmbedResponder.editMessage(message, false, trn.formatKey("error.request.error.uno.title"), e.getMessage());
            }
        }
    }

    private void closeSession(Message message, Session session) {
        Guild guild = message.getGuild().block();

        if(guild != null) {
            GuildData data = db.getGuild(guild);
            BaseLocalisation trn = data.getTranslator();

            try {

                long balance = 0;
                VoiceProfile voiceProfile = MainBotProcess.vp.getVoiceProfile(session.userId, session.guildId);

                if (voiceProfile != null) balance = voiceProfile.getVoicepoints();

                EmbedResponder.editMessage(message, true, trn.formatKey("slots.text.title"), trn.formatKey("slots.text.after_start.desc"), new LinkedHashMap<>(ImmutableMap.<String, String>builder()
                        .put(trn.formatKey("slots.text.ended.key"), trn.formatKey("slots.text.ended.value"))
                        .put(trn.formatKey("slots.text.post.key"), session.total < 0 ? trn.formatKey("slots.text.post.lost.value", Long.toString(Math.abs(session.total))) : trn.formatKey("slots.text.post.won.value", Long.toString(session.total)))
                        .build()));

                sessions.remove(message);

            } catch (RuntimeException e) {
                EmbedResponder.editMessage(message, false, trn.formatKey("error.request.error.uno.title"), e.getMessage());
            }
        }
    }

    private void sendKey(Message message, User reactor) {
        Guild guild = message.getGuild().block();

        if(guild != null) {
            GuildData data = db.getGuild(guild);
            BaseLocalisation trn = data.getTranslator();

            if(message.getEmbeds().size() > 0) {
                Embed embed = message.getEmbeds().stream().findFirst().orElse(null);

                if(embed != null) {
                    if(embed.getTitle().isPresent()) {
                        if(embed.getTitle().get().equals(trn.formatKey("slots.web.command.title"))) {
                            // Valid message
                            String key = KeyGen.generateUniqueKey();

                            PrivateChannel dm = reactor.getPrivateChannel().block();

                            if(dm != null) {
                                try {
                                    dm.createMessage(trn.formatKey("slots.web.command.dm", key)).block();

                                    SlotsWebLogin login = new SlotsWebLogin(key);
                                    login.setUserId(reactor.getId().asLong());
                                    login.setGuildId(guild.getId().asLong());
                                    db.addLogin(login);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
