package com.thejebforge.misaki.bot.modules;

import com.google.common.collect.ImmutableMap;
import com.thejebforge.misaki.bot.config.Configuration;
import com.thejebforge.misaki.bot.localisation.BaseLocalisation;
import com.thejebforge.misaki.bot.localisation.TranslatorLanguage;
import com.thejebforge.misaki.bot.manager.DatabaseManager;
import com.thejebforge.misaki.bot.types.*;
import com.thejebforge.misaki.bot.utils.CommandHelper;
import com.thejebforge.misaki.bot.utils.EmbedResponder;
import com.thejebforge.misaki.dao.GuildData;
import com.thejebforge.misaki.runtime.MainBotProcess;
import discord4j.common.util.Snowflake;
import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Guild;
import discord4j.core.object.entity.Member;
import discord4j.core.object.entity.channel.TextChannel;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ConfiguratorModule extends GenericModule {
    private static final String CODENAME = "config_module";

    private final Configuration config;

    public ConfiguratorModule(DatabaseManager db, GatewayDiscordClient gateway) {
        super(db, gateway);

        config = new Configuration();
    }

    @Override
    public String getCodeName() {
        return CODENAME;
    }

    @Override
    public String getDisplayKey() {
        return "configurator.name";
    }

    @Override
    public int getPermissionLevelRequired() {
        return 1;
    }

    @Override
    public boolean canBeDisabled(){
        return false;
    }

    // Command methods

    @BotPermission(value = BotAccess.MANAGERS, recursive = true)
    @BotHelp(value = "guild", childAsSections = true)
    @BotCommand(id = "guild", name = "guild", executable = false, childType = "method")
    public void guildMain() {}

    @BotPermission(value = BotAccess.MANAGERS, recursive = true)
    @BotHelp("module")
    @BotCommand(id = "module", name = "module", executable = false, childType = "method")
    public void moduleMain() {}


    // Show methods

    @BotPermission(value = BotAccess.ANYONE, recursive = true, override = true)
    @BotHelp(value = "guild.show", order = -1)
    @BotCommand(id = "guild-show", name = "show", executable = false, childType = "property", parent = "guild")
    public void showMethod() {}

    @BotPermission(BotAccess.ANYONE)
    @BotHelp("module.list")
    @BotCommand(id = "module-list", name = "list", parent = "module")
    public void showAllModules(Guild guild, TextChannel channel) {
        GuildData data = db.getGuild(guild);
        BaseLocalisation trn = data.getTranslator();

        StringBuilder modules = new StringBuilder();

        for (GenericModule module : MainBotProcess.moduleMap.values()) {
            if (module != null) {
                if(data.getPermissionLevel() >= module.getPermissionLevelRequired()) {
                    modules.append(trn.formatKey(module.getDisplayKey())).append(" (").append(module.getCodeName()).append(")\n");
                }
            }
        }

        EmbedResponder.sendMessage(channel, true, trn.formatKey("modules.list.show.title"), modules.toString());
    }

    @BotHelp("guild.show.modules")
    @BotCommand(id = "guild-show-modules", name = "modules", parent = "guild-show")
    public void showGuildModules(Guild guild, TextChannel channel) {
        GuildData data = db.getGuild(guild);
        BaseLocalisation trn = data.getTranslator();

        StringBuilder modules = new StringBuilder();

        for (String module : data.getEnabledModules()) {
            GenericModule moduleObject = MainBotProcess.moduleMap.get(module);

            if (moduleObject != null) {
                modules.append(trn.formatKey(moduleObject.getDisplayKey())).append(" (").append(module).append(")\n");
            }
        }

        EmbedResponder.sendMessage(channel, true, trn.formatKey("guild.module.show.title"), modules.toString());
    }

    @BotHelp("guild.show.managers")
    @BotCommand(id = "guild-show-managers", name = "managers", parent = "guild-show")
    public void showGuildManagers(Guild guild, TextChannel channel) {
        GuildData data = db.getGuild(guild);
        BaseLocalisation trn = data.getTranslator();

        StringBuilder managers = new StringBuilder();

        for(long id : data.getManagers()){
            Member fetchedMember = guild.getMemberById(Snowflake.of(id)).block();

            if(fetchedMember != null) {
                managers.append(fetchedMember.getDisplayName()).append("\n");
            }
        }

        EmbedResponder.sendMessage(channel, true, trn.formatKey("guild.managers.show.title"), managers.toString());
    }

    @BotPermission(BotAccess.OWNER)
    @BotHelp(value = "guild.show.permLevel", hidden = true)
    @BotCommand(id = "guild-show-permLevel", name = "permLevel", parent = "guild-show")
    public void showGuildPermLevel(Guild guild, TextChannel channel) {
        GuildData data = db.getGuild(guild);
        BaseLocalisation trn = data.getTranslator();

        EmbedResponder.sendMessage(channel, true, trn.formatKey("guild.permLevel.show.title"), new LinkedHashMap<>(ImmutableMap.<String, String>builder().put(trn.formatKey("guild.permLevel.show.field"), Integer.toString(data.getPermissionLevel())).build()));
    }

    @BotHelp("guild.show.language")
    @BotCommand(id = "guild-show-language", name = "language", parent = "guild-show")
    public void showGuildLanguage(Guild guild, TextChannel channel) {
        GuildData data = db.getGuild(guild);
        BaseLocalisation trn = data.getTranslator();

        EmbedResponder.sendMessage(channel, true, trn.formatKey("language.show.title"), new LinkedHashMap<>(ImmutableMap.<String, String>builder().put(trn.formatKey("language.show.field"), data.getLanguage().name()).build()));
    }

    @BotHelp("guild.show.timezone")
    @BotCommand(id = "guild-show-timezone", name = "timezone", parent = "guild-show")
    public void showGuildTimezone(Guild guild, TextChannel channel) {
        GuildData data = db.getGuild(guild);
        BaseLocalisation trn = data.getTranslator();

        EmbedResponder.sendMessage(channel, true, trn.formatKey("timezone.show.title"), new LinkedHashMap<>(ImmutableMap.<String, String>builder().put(trn.formatKey("timezone.show.field"), data.getTimezone().getID()).build()));
    }

    @BotHelp(value = "guild.show.all", order = 2)
    @BotCommand(id = "guild-show-all", name = "all", parent = "guild-show")
    public void showAllGuildInfo(Guild guild, TextChannel channel) {
        showGuildModules(guild, channel);
        showGuildManagers(guild, channel);
        showGuildLanguage(guild, channel);
        showGuildTimezone(guild, channel);
    }


    // Set methods

    @BotHelp(value = "guild.set", order = 0)
    @BotCommand(id = "guild-set",name = "set", executable = false, childType = "property", parent = "guild")
    public void setMethod() {}

    @BotPermission(BotAccess.OWNER)
    @BotHelp(value = "guild.set.permLevel", hidden = true)
    @BotCommand(id = "guild-set-permLevel", name = "permLevel", parent = "guild-set")
    public void setPermLevel(@BotArgument("integer") Integer permLevel, Guild guild, TextChannel channel) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        GuildData data = db.getGuild(guild);
        data.setPermissionLevel(permLevel);
        db.setGuild(data);

        EmbedResponder.sendMessage(channel, true, trn.formatKey("value.set.title"), trn.formatKey("value.set.desc"));
    }

    @BotHelp("guild.set.language")
    @BotCommand(id = "guild-set-language", name = "language", parent = "guild-set")
    public void setLanguage(@BotArgument("language") TranslatorLanguage language, Guild guild, TextChannel channel, Member member) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        GuildData data = db.getGuild(guild);
        data.setLanguage(language);
        db.setGuild(data);

        trn = data.getTranslator();

        EmbedResponder.sendMessage(channel, true, trn.formatKey("language.updated.title"), trn.formatKey("language.updated.desc"));
    }

    @BotHelp("guild.set.timezone")
    @BotCommand(id = "guild-set-timezone", name = "timezone", parent = "guild-set")
    public void setTimezone(@BotArgument("timezone") TimeZone timezone, Guild guild, TextChannel channel, Member member) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        GuildData data = db.getGuild(guild);
        data.setTimezone(timezone);
        db.setGuild(data);

        EmbedResponder.sendMessage(channel, true, trn.formatKey("timezone.updated.title"), trn.formatKey("timezone.updated.desc", data.getTimezone().getID()));
    }

    // Add/Remove methods

    @BotHelp(value = "guild.add", order = 2)
    @BotCommand(id = "guild-add", name = "add", executable = false, childType = "property", parent = "guild")
    public void addMethod() {}

    @BotHelp(value = "guild.remove", order = 3)
    @BotCommand(id = "guild-remove", name = "remove", executable = false, childType = "property", parent = "guild")
    public void removeMethod() {}

    @BotHelp("guild.add.managers")
    @BotCommand(id = "guild-add-managers", name = "managers", parent = "guild-add")
    public void addManagers(Guild guild, TextChannel channel, Member member, MessageCreateEvent eventData) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        if (eventData.getMessage().getUserMentionIds().size() > 0) {
            GuildData data = db.getGuild(guild);
            for (Snowflake mentionedID : eventData.getMessage().getUserMentionIds()) {
                data.addManager(mentionedID.asLong());
            }
            db.setGuild(data);

            EmbedResponder.sendMessage(channel, true, trn.formatKey("guild.managers.added.title"), trn.formatKey("guild.managers.added.desc"));
        } else {
            EmbedResponder.sendMessage(channel, false, trn.formatKey("error.missing.mention.add.title"), trn.formatKey("error.missing.mention.add.desc", CommandHelper.helpDirection(config, "guild")));
        }
    }

    @BotHelp("guild.remove.managers")
    @BotCommand(id = "guild-remove-managers", name = "managers", parent = "guild-remove")
    public void removeManagers(Guild guild, TextChannel channel, Member member, MessageCreateEvent eventData) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        if (eventData.getMessage().getUserMentionIds().size() > 0) {
            GuildData data = db.getGuild(guild);
            for (Snowflake mentionedID : eventData.getMessage().getUserMentionIds()) {
                data.removeManager(mentionedID.asLong());
            }
            db.setGuild(data);

            EmbedResponder.sendMessage(channel, true, trn.formatKey("guild.managers.removed.title"), trn.formatKey("guild.managers.removed.desc"));
        } else {
            EmbedResponder.sendMessage(channel, false, trn.formatKey("error.missing.mention.remove.title"), trn.formatKey("error.missing.mention.remove.desc", CommandHelper.helpDirection(config, "guild")));
        }
    }

    @BotHelp(value = "guild.enable", order = 4)
    @BotCommand(id = "guild-enable", name = "enable", parent = "guild")
    public void enableModule(@BotArgument("module") String codename, Guild guild, TextChannel channel, Member member) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        GenericModule module = MainBotProcess.moduleMap.get(codename);

        if (module != null) {
            GuildData data = db.getGuild(guild);
            if (!data.hasModuleEnabled(module)) {
                if(data.getPermissionLevel() >= module.getPermissionLevelRequired()) {
                    data.addModule(module);
                    db.setGuild(data);
                    EmbedResponder.sendMessage(channel, true, trn.formatKey("guild.module.enabled.title"), trn.formatKey("guild.module.enabled.desc", codename));
                } else {
                    EmbedResponder.sendMessage(channel, false, trn.formatKey("error.guild.not_trusted.title"), trn.formatKey("error.guild.not_trusted.desc", codename));
                }
            } else {
                EmbedResponder.sendMessage(channel, false, trn.formatKey("error.module.already.enabled.title"), trn.formatKey("error.module.already.enabled.desc", codename));
            }
        } else {
            EmbedResponder.sendMessage(channel, false, trn.formatKey("error.unknown.module.title"), trn.formatKey("error.unknown.module.desc", codename));
        }
    }

    @BotHelp(value = "guild.disable", order = 5)
    @BotCommand(id = "guild-disable", name = "disable", parent = "guild")
    public void disableModule(@BotArgument("module") String codename, Guild guild, TextChannel channel, Member member) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        GenericModule module = MainBotProcess.moduleMap.get(codename);

        if (module != null) {
            GuildData data = db.getGuild(guild);
            if (data.hasModuleEnabled(module)) {
                if (module.canBeDisabled()) {
                    data.removeModule(module);
                    db.setGuild(data);
                    EmbedResponder.sendMessage(channel, true, trn.formatKey("guild.module.disabled.title"), trn.formatKey("guild.module.disabled.desc", codename));
                } else {
                    EmbedResponder.sendMessage(channel, false, trn.formatKey("error.module.permanent.disable.title"), trn.formatKey("error.module.permanent.disable.desc", codename));
                }
            } else {
                EmbedResponder.sendMessage(channel, false, trn.formatKey("error.module.already.disabled.title"), trn.formatKey("error.module.already.disabled.desc", codename));
            }
        } else {
            EmbedResponder.sendMessage(channel, false, trn.formatKey("error.unknown.module.title"), trn.formatKey("error.unknown.module.desc", codename));
        }
    }
}
