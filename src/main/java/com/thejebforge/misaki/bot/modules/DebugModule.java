package com.thejebforge.misaki.bot.modules;

import com.google.common.collect.ImmutableMap;
import com.thejebforge.misaki.bot.config.Configuration;
import com.thejebforge.misaki.bot.localisation.BaseLocalisation;
import com.thejebforge.misaki.bot.manager.DatabaseManager;
import com.thejebforge.misaki.bot.types.*;
import com.thejebforge.misaki.bot.utils.CommandHelper;
import com.thejebforge.misaki.bot.utils.EmbedResponder;
import com.thejebforge.misaki.bot.utils.LuaHelper;
import com.thejebforge.misaki.bot.utils.StatusHelper;
import com.thejebforge.misaki.dao.DailyTopSubscription;
import com.thejebforge.misaki.dao.GuildData;
import com.thejebforge.misaki.dao.ImageData;
import com.thejebforge.misaki.dao.StatusData;
import com.thejebforge.misaki.runtime.MainBotProcess;
import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Guild;
import discord4j.core.object.entity.Member;
import discord4j.core.object.entity.channel.TextChannel;
import discord4j.core.object.presence.Activity;
import org.apache.commons.io.IOUtils;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaError;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.jse.CoerceJavaToLua;
import org.luaj.vm2.lib.jse.JsePlatform;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class DebugModule extends GenericModule {
    private static final Logger logger = LoggerFactory.getLogger(DebugModule.class);
    private static final String CODENAME = "debug_module";

    private final Configuration config;
    private final Globals globals;

    public DebugModule(DatabaseManager db, GatewayDiscordClient gateway) {
        super(db, gateway);

        config = new Configuration();

        globals = JsePlatform.standardGlobals();
        globals.set("db", CoerceJavaToLua.coerce(db));
        globals.set("gateway", CoerceJavaToLua.coerce(gateway));
        globals.set("responder", CoerceJavaToLua.coerce(EmbedResponder.class));
        globals.set("helper", CoerceJavaToLua.coerce(LuaHelper.class));
        globals.set("process", CoerceJavaToLua.coerce(MainBotProcess.class));
        globals.set("runtime", CoerceJavaToLua.coerce(Runtime.class));
        globals.set("system", CoerceJavaToLua.coerce(System.class));
        globals.set("ioutils", CoerceJavaToLua.coerce(IOUtils.class));
    }

    @Override
    public void init() {
        Timer statusTimer = new Timer();

        statusTimer.scheduleAtFixedRate(new StatusChangeTask(), 0, 60 * 1000);
    }

    // Status changer

    private class StatusChangeTask extends TimerTask {
        @Override
        public void run() {
            StatusData message = db.getRandomStatusMessage();

            StatusHelper.setParsedStatus(db, gateway, message.getMessage(), message.getActivity());
        }
    }

    @Override
    public String getCodeName() {
        return CODENAME;
    }

    @Override
    public String getDisplayKey() {
        return "debugmodule.name";
    }

    @Override
    public int getPermissionLevelRequired() {
        return 1;
    }


    // Command methods

    @BotPermission(value = BotAccess.OWNER, recursive = true)
    @BotHelp(value = "debug", childAsSections = true)
    @BotCommand(id = "debug", name = "debug", childType = "section", executable = false)
    public void debugMain() {}


    @BotHelp(value = "debug.images", order = 2)
    @BotCommand(id = "debug-images", name = "images", childType = "method", executable = false, parent = "debug")
    public void debugImages() {}

    @BotHelp(value = "debug.guilds", order = 3)
    @BotCommand(id = "debug-guilds", name = "guilds", childType = "method", executable = false, parent = "debug")
    public void debugGuilds() {}

    @BotHelp(value = "debug.status", order = 4)
    @BotCommand(id = "debug-status", name = "status", childType = "method", executable = false, parent = "debug")
    public void debugStatus() {}

    @BotHelp(value = "debug.general", order = 5)
    @BotCommand(id = "debug-general", name = "general", childType = "method", executable = false, parent = "debug")
    public void debugGeneral() {}

    @BotHelp(value = "debug.dailytop", order = 6)
    @BotCommand(id = "debug-dailytop", name = "dailytop", childType = "method", executable = false, parent = "debug")
    public void debugDailytop() {}



    @BotHelp("debug.images.drop")
    @BotCommand(id = "debug-images-drop", name = "drop", parent = "debug-images")
    public void processImagesDrop(@BotArgument(value = "args", checkExistence = false) String confirm, Guild guild, TextChannel channel, Member member, MessageCreateEvent eventData) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        if (confirm != null) {
            if (confirm.equals("yes")) {
                ObjectRepository<ImageData> imageRepository = db.getImageRepository();

                imageRepository.remove(ObjectFilters.ALL);

                EmbedResponder.sendMessage(channel, true, trn.formatKey("debug.images.dropped.title"), trn.formatKey("debug.images.dropped.desc"));
            } else {
                EmbedResponder.sendMessage(channel, false, trn.formatKey("debug.images.drop.confirm.title"), trn.formatKey("debug.images.drop.confirm.desc"), new LinkedHashMap<>(ImmutableMap.<String, String>builder()
                        .put(trn.formatKey("debug.images.drop.confirm.field"), config.getPrefix() + "debug images drop yes")
                        .build()));
            }
        } else {
            EmbedResponder.sendMessage(channel, false, trn.formatKey("debug.images.drop.confirm.title"), trn.formatKey("debug.images.drop.confirm.desc"), new LinkedHashMap<>(ImmutableMap.<String, String>builder()
                    .put(trn.formatKey("debug.images.drop.confirm.field"), config.getPrefix() + "debug images drop yes")
                    .build()));
        }
    }

    @BotHelp("debug.images.import")
    @BotCommand(id = "debug-images-import", name = "import", parent = "debug-images")
    public void processImagesImport(@BotArgument("file.path") String path, Guild guild, TextChannel channel, Member member, MessageCreateEvent eventData) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        Path fileToImport = Paths.get(path);

        if (fileToImport.toFile().exists()) {
            try {
                BufferedReader reader = new BufferedReader(new FileReader(fileToImport.toFile()));

                String line = reader.readLine();

                int count = 0;

                while (line != null) {
                    logger.info(line);

                    String[] split = line.split(",");
                    List<String> pieces = new ArrayList<>(Arrays.asList(split));

                    String filename = pieces.remove(0);
                    Set<String> tags = new HashSet<>(pieces);

                    logger.info("Filename: " + filename);
                    logger.info("Tags: " + String.join(", ", tags));

                    ImageData image = new ImageData(filename, tags);

                    db.addImage(image);
                    count++;

                    line = reader.readLine();
                }

                reader.close();

                EmbedResponder.sendMessage(channel, true, trn.formatKey("debug.images.import.title", Integer.toString(count)), trn.formatKey("debug.images.import.desc"));
            } catch (IOException e) {
                EmbedResponder.sendMessage(channel, false, trn.formatKey("error.images.import.fail.title"), trn.formatKey("error.images.import.fail.desc"));
                e.printStackTrace();
            }
        } else {
            EmbedResponder.sendMessage(channel, false, trn.formatKey("error.images.file.doesnt_exist.title"), trn.formatKey("error.images.file.doesnt_exist.desc"));
        }
    }

    @BotHelp("debug.guilds.drop")
    @BotCommand(id = "debug-guilds-drop", name = "drop", parent = "debug-guilds")
    public void processGuildsDrop(@BotArgument(value = "args", checkExistence = false) String confirm, Guild guild, TextChannel channel, Member member, MessageCreateEvent eventData) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        if (confirm != null) {
            if (confirm.equals("yes")) {
                ObjectRepository<GuildData> guildRepository = db.getGuildRepository();

                guildRepository.remove(ObjectFilters.ALL);

                EmbedResponder.sendMessage(channel, true, trn.formatKey("debug.guilds.dropped.title"), trn.formatKey("debug.guilds.dropped.desc"));
            } else {
                EmbedResponder.sendMessage(channel, false, trn.formatKey("debug.guilds.drop.confirm.title"), trn.formatKey("debug.guilds.drop.confirm.desc"), new LinkedHashMap<>(ImmutableMap.<String, String>builder()
                        .put(trn.formatKey("debug.guilds.drop.confirm.field"), config.getPrefix() + "debug guilds drop yes")
                        .build()));
            }
        } else {
            EmbedResponder.sendMessage(channel, false, trn.formatKey("debug.guilds.drop.confirm.title"), trn.formatKey("debug.guilds.drop.confirm.desc"), new LinkedHashMap<>(ImmutableMap.<String, String>builder()
                    .put(trn.formatKey("debug.guilds.drop.confirm.field"), config.getPrefix() + "debug guilds drop yes")
                    .build()));
        }
    }

    @BotHelp("debug.status.add")
    @BotCommand(id = "debug-status-add", name = "add", parent = "debug-status")
    public void processStatusAdd(@BotArgument("status.activity") Activity.Type activity, @BotArgument(value = "status", takeRest = true) String status, Guild guild, TextChannel channel, Member member) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        db.addStatusMessage(new StatusData(status, activity));
        EmbedResponder.sendMessage(channel, true, trn.formatKey("debug.status.added.title"), trn.formatKey("debug.status.added.desc"), new LinkedHashMap<>(ImmutableMap.<String, String>builder()
                .put(trn.formatKey("debug.status.added.activity"), activity.name())
                .put(trn.formatKey("debug.status.added.message"), status)
                .build()));
    }

    @BotHelp("debug.status.remove")
    @BotCommand(id = "debug-status-remove", name = "remove", parent = "debug-status")
    public void processStatusRemove(@BotArgument("status.activity") Activity.Type activity, @BotArgument(value = "status", takeRest = true) String status, Guild guild, TextChannel channel, Member member, MessageCreateEvent eventData) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        if (db.statusMessageExists(status, activity)) {
            db.removeStatusMessage(status, activity);
            EmbedResponder.sendMessage(channel, true, trn.formatKey("debug.status.removed.title"), trn.formatKey("debug.status.removed.desc"));
        } else {
            EmbedResponder.sendMessage(channel, false, trn.formatKey("error.unknown.status.title"), trn.formatKey("error.unknown.status.desc"));
        }

    }

    @BotHelp("debug.status.list")
    @BotCommand(id = "debug-status-list", name = "list", parent = "debug-status")
    public void processStatusList(Guild guild, TextChannel channel) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        List<String> messages = new ArrayList<>();

        for (StatusData data : db.getAllStatusMessages())
            messages.add(data.getActivity().name() + " " + data.getMessage());

        EmbedResponder.sendMessage(channel, true, trn.formatKey("debug.status.list.title"), String.join("\n", messages));
    }

    @BotHelp("debug.status.drop")
    @BotCommand(id = "debug-status-drop", name = "drop", parent = "debug-status")
    public void processStatusDrop(@BotArgument(value = "args", checkExistence = false) String confirm, Guild guild, TextChannel channel, Member member, MessageCreateEvent eventData) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        if (confirm != null) {
            if (confirm.equals("yes")) {
                ObjectRepository<StatusData> guildRepository = db.getStatusMessages();

                guildRepository.remove(ObjectFilters.ALL);

                EmbedResponder.sendMessage(channel, true, trn.formatKey("debug.status.dropped.title"), trn.formatKey("debug.status.dropped.desc"));
            } else {
                EmbedResponder.sendMessage(channel, false, trn.formatKey("debug.status.drop.confirm.title"), trn.formatKey("debug.status.drop.confirm.desc"), new LinkedHashMap<>(ImmutableMap.<String, String>builder()
                        .put(trn.formatKey("debug.status.drop.confirm.field"), config.getPrefix() + "debug status drop yes")
                        .build()));
            }
        } else {
            EmbedResponder.sendMessage(channel, false, trn.formatKey("debug.status.drop.confirm.title"), trn.formatKey("debug.status.drop.confirm.desc"), new LinkedHashMap<>(ImmutableMap.<String, String>builder()
                    .put(trn.formatKey("debug.status.drop.confirm.field"), config.getPrefix() + "debug status drop yes")
                    .build()));
        }
    }

    @BotHelp("debug.general.kill")
    @BotCommand(id = "debug-general-kill-process", name = "kill-process", parent = "debug-general")
    public void processGeneralKill(Guild guild, TextChannel channel) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        EmbedResponder.sendMessage(channel, true, trn.formatKey("debug.general.kill.title"), trn.formatKey("debug.general.kill.desc"));
        gateway.logout().block();
        System.exit(0);
    }

    @BotHelp("debug.general.status")
    @BotCommand(id = "debug-general-status", name = "status", parent = "debug-general")
    public void processGeneralStatus(@BotArgument("status.activity") Activity.Type activity, @BotArgument(value = "status", takeRest = true) String status, Guild guild, TextChannel channel, Member member, MessageCreateEvent eventData) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        StatusHelper.setParsedStatus(db, gateway, status, activity);
        EmbedResponder.sendMessage(channel, true, trn.formatKey("debug.status.updated.title"), trn.formatKey("debug.status.updated.desc"));
    }

    @BotCommand(id = "debug-general-test", name = "test", parent = "debug-general")
    public void processGeneralTest(Command command, List<String> args, Guild guild, TextChannel channel, Member member, MessageCreateEvent eventData) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        //EmbedResponder.sendMessage(channel, false, "There's currently nothing in testing");
    }

    @BotHelp("debug.dailytop.drop")
    @BotCommand(id = "debug-dailytop-drop", name = "drop", parent = "debug-dailytop")
    public void processDailyTopDrop(@BotArgument(value = "args", checkExistence = false) String confirm, Guild guild, TextChannel channel) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        if (confirm != null) {
            if (confirm.equals("yes")) {
                ObjectRepository<DailyTopSubscription> dailyTopSubs = db.getDailyTopSubs();

                dailyTopSubs.remove(ObjectFilters.ALL);

                EmbedResponder.sendMessage(channel, true, trn.formatKey("debug.dailytop.dropped.title"), trn.formatKey("debug.dailytop.dropped.desc"));
            } else {
                EmbedResponder.sendMessage(channel, false, trn.formatKey("debug.dailytop.drop.confirm.title"), trn.formatKey("debug.dailytop.drop.confirm.desc"), new LinkedHashMap<>(ImmutableMap.<String, String>builder()
                        .put(trn.formatKey("debug.dailytop.drop.confirm.field"), config.getPrefix() + "debug dailytop drop yes")
                        .build()));
            }
        } else {
            EmbedResponder.sendMessage(channel, false, trn.formatKey("debug.dailytop.drop.confirm.title"), trn.formatKey("debug.dailytop.drop.confirm.desc"), new LinkedHashMap<>(ImmutableMap.<String, String>builder()
                    .put(trn.formatKey("debug.dailytop.drop.confirm.field"), config.getPrefix() + "debug dailytop drop yes")
                    .build()));
        }
    }

    private static class LuaThread extends Thread {
        private final Globals globals;
        private final String codeStr;
        private final TextChannel channel;
        private final BaseLocalisation trn;

        public LuaThread(Globals globals, String codeStr, TextChannel channel, BaseLocalisation trn) {
            this.globals = globals;
            this.codeStr = codeStr;
            this.channel = channel;
            this.trn = trn;
        }

        public void run() {
            try {
                LuaValue code = globals.load(codeStr);

                LuaValue res = code.call();
                String resStr = res.toString();

                if(!res.isnil())
                    EmbedResponder.sendMessage(channel, true, trn.formatKey("debug.lua.execution.title"), resStr.substring(0, Math.min(resStr.length(), 2000)));
            } catch (LuaError e) {
                EmbedResponder.sendMessage(channel, false, trn.formatKey("error.debug.lua.error.title"), e.getMessage());
            }
        }
    }

    @BotPermission(BotAccess.OWNER)
    @BotHelp("lua")
    @BotCommand(id = "lua", name = "lua")
    public void processLua(@BotArgument(value = "code", takeRest = true, checkValidity = false) String codeStr, Guild guild, TextChannel channel, Member member, MessageCreateEvent eventData) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        globals.set("guild", CoerceJavaToLua.coerce(guild));
        globals.set("channel", CoerceJavaToLua.coerce(channel));
        globals.set("event", CoerceJavaToLua.coerce(eventData));
        globals.set("trn", CoerceJavaToLua.coerce(trn));

        LuaThread thread = new LuaThread(globals, codeStr, channel, trn);
        thread.start();
    }
}
