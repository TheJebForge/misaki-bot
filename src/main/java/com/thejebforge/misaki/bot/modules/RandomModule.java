package com.thejebforge.misaki.bot.modules;

import com.google.common.collect.ImmutableMap;
import com.thejebforge.misaki.bot.config.Configuration;
import com.thejebforge.misaki.bot.localisation.BaseLocalisation;
import com.thejebforge.misaki.bot.manager.DatabaseManager;
import com.thejebforge.misaki.bot.types.BotArgument;
import com.thejebforge.misaki.bot.types.BotCommand;
import com.thejebforge.misaki.bot.types.BotHelp;
import com.thejebforge.misaki.bot.types.Command;
import com.thejebforge.misaki.bot.utils.CommandHelper;
import com.thejebforge.misaki.bot.utils.EmbedResponder;
import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Guild;
import discord4j.core.object.entity.Member;
import discord4j.core.object.entity.channel.TextChannel;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RandomModule extends GenericModule {
    private static final String CODENAME = "random_module";

    private final Configuration config;

    public RandomModule(DatabaseManager db, GatewayDiscordClient gateway) {
        super(db, gateway);

        config = new Configuration();
    }

    @Override
    public String getCodeName() {
        return CODENAME;
    }

    @Override
    public String getDisplayKey() {
        return "randommodule.name";
    }

    @Override
    public int getPermissionLevelRequired() {
        return 1;
    }


    // Command methods

    @BotHelp("choose")
    @BotCommand(id = "choose", name = "choose")
    public void processChoose(@BotArgument("choices") List<String> args, Guild guild, TextChannel channel) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        Random rand = new Random();

        String choice = args.get(rand.nextInt(args.size()));

        EmbedResponder.sendMessage(channel, true, trn.formatKey("choose.title"), trn.formatKey("choose.desc"),
                new LinkedHashMap<>(ImmutableMap.<String, String>builder()
                        .put(trn.formatKey("choose.field"), choice)
                        .build()));
    }

    @BotHelp("roll")
    @BotCommand(id = "roll", name = "roll")
    public void processRoll(@BotArgument("roll.pattern") String pattern, Guild guild, TextChannel channel) {
        BaseLocalisation trn = db.getGuild(guild).getTranslator();

        Matcher matcher = Pattern.compile("\\d+d\\d+").matcher(pattern);
        Matcher truncMatcher = Pattern.compile("d\\d+").matcher(pattern);

        if(matcher.find()){
            List<String> digits = new ArrayList<>(Arrays.asList(matcher.group().split("d")));

            String countStr = digits.remove(0);
            String sideStr = digits.remove(0);

            try {
                int count = Integer.parseInt(countStr);
                int side = Integer.parseInt(sideStr);

                if(count > 50) {
                    EmbedResponder.sendMessage(channel, false, trn.formatKey("error.roll.too_many.title"), trn.formatKey("error.roll.too_many.desc"));
                    return;
                }

                Random rand = new Random();
                StringBuilder results = new StringBuilder();

                long sum = 0;

                for(int i = 0; i < count; i++){
                    int roll = rand.nextInt(side) + 1;
                    results.append(trn.formatKey("roll.multiple_dice_roll", Integer.toString(i + 1), Integer.toString(roll))).append("\n");
                    sum += roll;
                }

                EmbedResponder.sendMessage(channel, true, trn.formatKey("roll.results.title"), results.toString(), new LinkedHashMap<>(ImmutableMap.<String, String>builder()
                        .put(trn.formatKey("roll.results.field"), trn.formatKey("roll.results.value", Long.toString(sum)))
                        .build()));
            } catch(NumberFormatException e) {
                EmbedResponder.sendMessage(channel, false, trn.formatKey("error.roll.bad_number.title"), trn.formatKey("error.roll.bad_number.desc", CommandHelper.helpDirection(config, "roll")));
            }

        } else if(truncMatcher.find()) {
            String sideStr = truncMatcher.group().substring(1);

            try {
                int side = Integer.parseInt(sideStr);

                Random rand = new Random();

                EmbedResponder.sendMessage(channel, true, trn.formatKey("roll.single_dice_roll", Integer.toString(rand.nextInt(side) + 1)));
            } catch(NumberFormatException e) {
                EmbedResponder.sendMessage(channel, false, trn.formatKey("error.roll.bad_number.title"), trn.formatKey("error.roll.bad_number.desc", CommandHelper.helpDirection(config, "roll")));
            }
        } else {
            EmbedResponder.sendMessage(channel, false, trn.formatKey("error.roll.bad_pattern.title"), trn.formatKey("error.roll.bad_pattern.desc", CommandHelper.helpDirection(config, "roll")));
        }
    }

    @BotHelp("rickroll")
    @BotCommand(id = "rickroll", name = "rickroll")
    public void processRickroll(TextChannel channel) {
        channel.createMessage("https://youtu.be/dQw4w9WgXcQ").block();
    }
}
