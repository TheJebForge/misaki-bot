package com.thejebforge.misaki.bot.modules;

import com.thejebforge.misaki.bot.manager.DatabaseManager;
import com.thejebforge.misaki.bot.types.BotCommand;
import com.thejebforge.misaki.bot.types.BotHelp;
import com.thejebforge.misaki.bot.types.Command;
import com.thejebforge.misaki.bot.utils.EmbedResponder;
import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Guild;
import discord4j.core.object.entity.Member;
import discord4j.core.object.entity.channel.TextChannel;

import java.util.List;

public class TestModule extends GenericModule {
    private static final String CODENAME = "test_module";

    public TestModule(DatabaseManager db, GatewayDiscordClient gateway){
        super(db, gateway);

    }

    @Override
    public String getCodeName() {
        return CODENAME;
    }

    @Override
    public String getDisplayKey() {
        return "testmodule.name";
    }

    @Override
    public int getPermissionLevelRequired() {
        return 1;
    }

    @BotHelp("test")
    @BotCommand(id = "test", name = "test")
    public void processTest(Command command, List<String> args, Guild guild, TextChannel channel, Member member, MessageCreateEvent eventData) {
        EmbedResponder.sendMessage(channel, true, "Owo?", "What's this?");
    }


}
