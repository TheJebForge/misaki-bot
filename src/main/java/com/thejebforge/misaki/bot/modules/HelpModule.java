package com.thejebforge.misaki.bot.modules;

import com.google.common.collect.ImmutableMap;
import com.thejebforge.misaki.bot.config.Configuration;
import com.thejebforge.misaki.bot.localisation.BaseLocalisation;
import com.thejebforge.misaki.bot.manager.DatabaseManager;
import com.thejebforge.misaki.bot.types.BotArgument;
import com.thejebforge.misaki.bot.types.BotCommand;
import com.thejebforge.misaki.bot.types.BotHelp;
import com.thejebforge.misaki.bot.types.Command;
import com.thejebforge.misaki.bot.utils.CommandHelper;
import com.thejebforge.misaki.bot.utils.EmbedResponder;
import com.thejebforge.misaki.dao.GuildData;
import com.thejebforge.misaki.runtime.MainBotProcess;
import discord4j.core.GatewayDiscordClient;
import discord4j.core.object.entity.Guild;
import discord4j.core.object.entity.channel.TextChannel;

import java.util.*;

public class HelpModule extends GenericModule {
    private static final String CODENAME = "help_module";

    private final Configuration config;

    public HelpModule(DatabaseManager db, GatewayDiscordClient gateway) {
        super(db, gateway);

        config = new Configuration();
    }

    @Override
    public String getCodeName() {
        return CODENAME;
    }

    @Override
    public String getDisplayKey() {
        return "helper.name";
    }

    @Override
    public int getPermissionLevelRequired() {
        return 1;
    }

    @Override
    public boolean canBeDisabled(){
        return false;
    }


    private void displayCommands(Guild guild, TextChannel channel, int page){
        GuildData data = db.getGuild(guild);
        BaseLocalisation trn = data.getTranslator();
        Map<String, String> sections = new HashMap<>();

        for(GenericModule module : MainBotProcess.moduleMap.values()){
            StringBuilder commands = new StringBuilder();

            if(data.getPermissionLevel() >= module.getPermissionLevelRequired()){
                if(data.hasModuleEnabled(module)){
                    for(Command commandDescriptor : MainBotProcess.moduleCommandsMap.get(module)) {
                        commands.append("`").append(commandDescriptor.getName()).append("` ");
                    }
                    sections.put(trn.formatKey(module.getDisplayKey()), commands.toString());
                }
            }
        }

        List<String> sectionKeys = new ArrayList<>(sections.keySet());

        LinkedHashMap<String, String> fields = new LinkedHashMap<>(25);

        int startIndex = 24 * (page - 1);
        int endIndex = 24 * (page);

        if(((int)Math.ceil((double) sectionKeys.size() / 24)) < page) {
            EmbedResponder.sendMessage(channel, false, trn.formatKey("error.missing.page.too_high.title"), trn.formatKey("error.missing.page.too_high.desc"));
            return;
        } else if(page < 1){
            EmbedResponder.sendMessage(channel, false, trn.formatKey("error.missing.page.too_low.title"), trn.formatKey("error.missing.page.too_low.desc"));
            return;
        }

        for(int i = startIndex; i < endIndex; i++){
            if(sectionKeys.size() > i){
                String key = sectionKeys.get(i);
                fields.put(key, sections.get(key).isEmpty() ? trn.formatKey("help.commands.missing") : sections.get(key));
            }
        }

        fields.put(trn.formatKey("help.commands.morehelp.field"), trn.formatKey("help.commands.morehelp.value", config.getPrefix()+"help"));

        EmbedResponder.sendMessage(channel, true, trn.formatKey("help.commands.show.title"), fields, ImmutableMap.<String, Boolean>builder().put(trn.formatKey("help.commands.morehelp.field"), false).build(),true);
    }

    private void findCommandHelp(List<String> args, BaseLocalisation trn, Command command, TextChannel channel) {
        // Check for subcommands
        if(command.getChildren().isEmpty()){
            // No subcommands
            if(command.hasHelp())
                command.displayHelp(config, trn, command, channel);
            else
                EmbedResponder.sendMessage(channel, false, trn.formatKey("error.missing.help.title"), trn.formatKey("error.missing.help.desc"));
        } else {
            // Subcommands
            if(args.size() > 0) {
                String subcommand = args.remove(0);

                if (!CommandHelper.checkIfArgumentIsSane(trn, channel, subcommand)) return;

                for (Command child : command.getChildren()) {
                    if (child.getName().equals(subcommand)) {
                        findCommandHelp(args, trn, child, channel);
                        return;
                    }
                }

                EmbedResponder.sendMessage(channel, false, trn.formatKey("error.unknown.command.help.title"), trn.formatKey("error.unknown.command.help.desc"));
            } else {
                if(command.hasHelp())
                    command.displayHelp(config, trn, command, channel);
                else
                    EmbedResponder.sendMessage(channel, false, trn.formatKey("error.missing.help.title"), trn.formatKey("error.missing.help.desc"));
            }
        }
    }

    @BotHelp("help")
    @BotCommand(id = "help", name = "help")
    public void processHelpCommand(@BotArgument(value = "args", checkExistence = false) List<String> args, Guild guild, TextChannel channel) {
        GuildData data = db.getGuild(guild);
        BaseLocalisation trn = data.getTranslator();

        if(args != null){
            String whatToHelpWith = args.remove(0);

            if(!CommandHelper.checkIfArgumentIsSane(trn, channel, whatToHelpWith)) return;

            Command commandDescriptor = MainBotProcess.commandMap.get(whatToHelpWith);

            if(commandDescriptor != null && data.hasModuleEnabled(commandDescriptor.getModule())){
                findCommandHelp(args, trn, commandDescriptor, channel);
            } else {
                try {
                    int page = Integer.parseInt(whatToHelpWith);

                    displayCommands(guild, channel, page);
                } catch(NumberFormatException e) {
                    EmbedResponder.sendMessage(channel, false, trn.formatKey("error.unknown.command.help.title"), trn.formatKey("error.unknown.command.help.desc"));
                }
            }
        } else {
            displayCommands(guild, channel, 1);
        }
    }
}
