package com.thejebforge.misaki.bot.manager;

import com.thejebforge.misaki.bot.config.Configuration;
import com.thejebforge.misaki.dao.*;
import discord4j.core.object.entity.Guild;
import discord4j.core.object.entity.Message;
import discord4j.core.object.presence.Activity;
import lombok.Getter;
import org.dizitart.no2.Nitrite;
import org.dizitart.no2.WriteResult;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectFilter;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.*;

public class DatabaseManager {
    private static final Logger logger = LoggerFactory.getLogger(DatabaseManager.class);

    @Getter
    private final ObjectRepository<GuildData> guildRepository;
    @Getter
    private final ObjectRepository<ImageData> imageRepository;
    @Getter
    private final ObjectRepository<StatusData> statusMessages;
    @Getter
    private final ObjectRepository<DailyTopSubscription> dailyTopSubs;
    @Getter
    private final ObjectRepository<SlotsWebLogin> slotsWebLogins;
    @Getter
    private final ObjectRepository<CustomCommand> customCommands;

    public DatabaseManager() {
        Configuration config = new Configuration();

        Nitrite db = Nitrite.builder()
                .filePath(config.getDatabasePath())
                .openOrCreate();

        guildRepository = db.getRepository(GuildData.class);
        imageRepository = db.getRepository(ImageData.class);
        statusMessages = db.getRepository(StatusData.class);
        dailyTopSubs = db.getRepository(DailyTopSubscription.class);
        slotsWebLogins = db.getRepository(SlotsWebLogin.class);
        customCommands = db.getRepository(CustomCommand.class);

        // Worker for deletion of old logins
        new Timer().scheduleAtFixedRate(new LoginCleanerTask(), 0, 60 * 60 * 4 * 1000);
    }

    // Guild data

    public List<GuildData> getAllGuilds(){
        return guildRepository.find().toList();
    }

    public boolean guildExists(long guildId){
        ObjectFilter guildSelector = ObjectFilters.eq("guildId", guildId);

        Cursor<GuildData> results = guildRepository.find(guildSelector);

        return results.totalCount() > 0;
    }

    public boolean guildExists(Guild guild){
        return guildExists(guild.getId().asLong());
    }

    public void setGuild(GuildData data){
        if(guildExists(data.getGuildId())){
            guildRepository.update(ObjectFilters.eq("guildId", data.getGuildId()), data);
        } else {
            guildRepository.insert(data);
        }
    }

    public GuildData getGuild(long guildId){
        ObjectFilter guildSelector = ObjectFilters.eq("guildId", guildId);

        Cursor<GuildData> results = guildRepository.find(guildSelector);

        return results.firstOrDefault();
    }

    public GuildData getGuild(Guild guild){
        return getGuild(guild.getId().asLong());
    }

    // Image data

    public List<ImageData> getAllImages() {
        return imageRepository.find().toList();
    }

    public boolean imageExists(String filename){
        ObjectFilter imageSelector = ObjectFilters.eq("filename", filename);

        Cursor<ImageData> results = imageRepository.find(imageSelector);

        return results.totalCount() > 0;
    }

    public ImageData getImage(String filename){
        ObjectFilter imageSelector = ObjectFilters.eq("filename", filename);

        Cursor<ImageData> results = imageRepository.find(imageSelector);

        return results.firstOrDefault();
    }

    public List<ImageData> getImagesByTags(Set<String> tags){
        List<ImageData> results = imageRepository.find().toList();

        for(String tag : tags) {
            results.removeIf(imageData -> !imageData.hasTag(tag));
        }

        return results;
    }

    public Set<String> getAllImageTags(){
        List<ImageData> results = imageRepository.find().toList();
        Set<String> tags = new HashSet<>();

        for(ImageData image : results) {
            tags.addAll(image.getTags());
        }

        return tags;
    }

    public void addImage(ImageData data){
        if(imageExists(data.getFilename())){
            imageRepository.update(ObjectFilters.eq("filename", data.getFilename()), data);
        } else {
            imageRepository.insert(data);
        }
    }

    public int deleteImage(String filename) {
        ObjectFilter imageSelector = ObjectFilters.eq("filename", filename);

        WriteResult rs = imageRepository.remove(imageSelector);

        return rs.getAffectedCount();
    }

    public int deleteImage(ImageData data) {
        return deleteImage(data.getFilename());
    }

    // Status messages

    public boolean statusMessageExists(StatusData data) {
        return statusMessageExists(data.getMessage(), data.getActivity());
    }

    public boolean statusMessageExists(String message, Activity.Type type) {
        Cursor<StatusData> cursor = statusMessages.find(ObjectFilters.and(ObjectFilters.eq("message", message), ObjectFilters.eq("activity", type)));

        return cursor.totalCount() > 0;
    }

    public void addStatusMessage(StatusData message) {
        Cursor<StatusData> cursor = statusMessages.find(ObjectFilters.eq("message", message.getMessage()));

        if(cursor.totalCount() <= 0) {
            statusMessages.insert(message);
        }
    }

    public void removeStatusMessage(StatusData data) {
        removeStatusMessage(data.getMessage(), data.getActivity());
    }

    public void removeStatusMessage(String message, Activity.Type type){
        ObjectFilter filter = ObjectFilters.and(ObjectFilters.eq("message", message), ObjectFilters.eq("activity", type));

        Cursor<StatusData> cursor = statusMessages.find(filter);

        if(cursor.totalCount() > 0) {
            statusMessages.remove(filter);
        }
    }

    public List<StatusData> getAllStatusMessages() {
        return statusMessages.find().toList();
    }

    public StatusData getRandomStatusMessage() {
        List<StatusData> messages = statusMessages.find().toList();

        Random random = new Random();

        return messages.get(random.nextInt(messages.size()));
    }

    // Daily Top Subscriptions

    public boolean dailyTopSubscriptionsExists(long message) {
        Cursor<DailyTopSubscription> cursor = dailyTopSubs.find(ObjectFilters.eq("messageId", message));

        return cursor.totalCount() > 0;
    }

    public List<DailyTopSubscription> listDailyTopSubscriptions() {
        return dailyTopSubs.find().toList();
    }

    public void addDailyTopSubscription(DailyTopSubscription sub) {
        dailyTopSubs.insert(sub);
    }

    public void removeDailyTopSubscription(DailyTopSubscription sub) {
        ObjectFilter filter = ObjectFilters.eq("messageId", sub.getMessageId());

        Cursor<DailyTopSubscription> cursor = dailyTopSubs.find(filter);

        if(cursor.totalCount() > 0) {
            dailyTopSubs.remove(filter);
        }
    }

    public DailyTopSubscription getDailyTopSubscription(long messageId) {
        ObjectFilter filter = ObjectFilters.eq("messageId", messageId);

        Cursor<DailyTopSubscription> cursor = dailyTopSubs.find(filter);

        return cursor.firstOrDefault();
    }

    public DailyTopSubscription getDailyTopSubscription(Message message) {
        return getDailyTopSubscription(message.getId().asLong());
    }

    // Slots Web Login operations

    public boolean loginExists(String key) {
        ObjectFilter loginSelector = ObjectFilters.eq("key", key);

        Cursor<SlotsWebLogin> results = slotsWebLogins.find(loginSelector);

        return results.totalCount() > 0;
    }

    public void addLogin(SlotsWebLogin login) {
        if(loginExists(login.getKey())){
            slotsWebLogins.update(ObjectFilters.eq("key", login.getKey()), login);
        } else {
            slotsWebLogins.insert(login);
        }
    }

    public SlotsWebLogin getLoginFromKey(String key) {
        ObjectFilter filter = ObjectFilters.eq("key", key);

        Cursor<SlotsWebLogin> cursor = slotsWebLogins.find(filter);

        return cursor.firstOrDefault();
    }

    public SlotsWebLogin getLoginFromCookie(String cookie) {
        ObjectFilter filter = ObjectFilters.eq("cookie", cookie);

        Cursor<SlotsWebLogin> cursor = slotsWebLogins.find(filter);

        return cursor.firstOrDefault();
    }

    public int deleteExpiredLogins() {
        ObjectFilter filter = ObjectFilters.lt("deletionTime", Timestamp.from(Instant.now()));

        return slotsWebLogins.remove(filter).getAffectedCount();
    }

    public int deleteLoginsForUser(long userId) {
        ObjectFilter filter = ObjectFilters.eq("userId", userId);

        return slotsWebLogins.remove(filter).getAffectedCount();
    }

    private class LoginCleanerTask extends TimerTask {
        @Override
        public void run() {
            logger.info("Deleted " + deleteExpiredLogins() + " expired logins");
        }
    }

    // Custom commands

    public boolean commandExists(String command) {
        ObjectFilter loginSelector = ObjectFilters.eq("name", command);

        Cursor<CustomCommand> results = customCommands.find(loginSelector);

        return results.totalCount() > 0;
    }

    public void addCommand(CustomCommand command) {
        if(commandExists(command.getName())){
            customCommands.update(ObjectFilters.eq("name", command.getName()), command);
        } else {
            customCommands.insert(command);
        }
    }

    public CustomCommand getCommand(String command) {
        ObjectFilter filter = ObjectFilters.eq("name", command);

        Cursor<CustomCommand> cursor = customCommands.find(filter);

        return cursor.firstOrDefault();
    }

    public int deleteCommand(String command) {
        ObjectFilter filter = ObjectFilters.eq("name", command);

        return customCommands.remove(filter).getAffectedCount();
    }

    public List<CustomCommand> listCommands() {
        return customCommands.find().toList();
    }
}
