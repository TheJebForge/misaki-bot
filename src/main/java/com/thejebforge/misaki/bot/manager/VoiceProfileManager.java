package com.thejebforge.misaki.bot.manager;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import com.thejebforge.misaki.bot.config.Configuration;
import com.thejebforge.misaki.bot.types.VoiceProfile;
import discord4j.common.util.Snowflake;
import discord4j.core.object.entity.Guild;
import discord4j.core.object.entity.Member;
import io.netty.handler.codec.http.HttpMethod;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.Exceptions;
import reactor.core.publisher.Mono;
import reactor.netty.ByteBufFlux;
import reactor.netty.ByteBufMono;
import reactor.netty.http.client.HttpClient;
import reactor.netty.http.client.HttpClientRequest;
import reactor.netty.http.client.HttpClientResponse;

import java.util.*;

public class VoiceProfileManager {
    private static final Logger logger = LoggerFactory.getLogger(VoiceProfileManager.class);

    private final Configuration config;
    private final HttpClient client;

    public VoiceProfileManager() {
        config = new Configuration();
        client = HttpClient.create()
                .headers((h) -> h.add("Authorization", config.getApiToken()))
                .doOnError(this::requestErrorHandler, this::responseErrorHandler);
    }

    private String buildJson() {
        return buildJson(null);
    }

    private String buildJson(Map<String, Object> params) {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> json = new HashMap<>();

        json.put("api_token", config.getApiToken());

        if(params != null)
            json.putAll(params);

        try {
            return mapper.writeValueAsString(json);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<VoiceProfile> getPointsTop(Guild guild) {
        String response = client.request(HttpMethod.GET)
                .uri(config.getAPIEndpoint("profile/" + guild.getId().asLong() + "/points-top"))
                .responseSingle(this::requestHandler).block();

        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);

        try {
            VoiceProfile[] array = mapper.readValue(response, VoiceProfile[].class);
            return new ArrayList<>(Arrays.asList(array));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return null;
    }

    public List<VoiceProfile> getLevelTop(Guild guild) {
        String response = client.request(HttpMethod.GET)
                .uri(config.getAPIEndpoint("profile/" + guild.getId().asLong() + "/level-top"))
                .responseSingle(this::requestHandler).block();

        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);

        try {
            VoiceProfile[] array = mapper.readValue(response, VoiceProfile[].class);
            return new ArrayList<>(Arrays.asList(array));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return null;
    }

    public List<VoiceProfile> getTimeTop(Guild guild) {
        String response = client.request(HttpMethod.GET)
                .uri(config.getAPIEndpoint("profile/" + guild.getId().asLong() + "/time-top"))
                .responseSingle(this::requestHandler).block();

        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);

        try {
            VoiceProfile[] array = mapper.readValue(response, VoiceProfile[].class);
            return new ArrayList<>(Arrays.asList(array));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void voicePointAdd(Member member, Guild guild, long voicepoints) {
        voicePointAdd(member.getId().asLong(), guild.getId().asLong(), voicepoints);
    }

    public void voicePointAdd(Snowflake member, Snowflake guild, long voicepoints) {
        voicePointAdd(member.asLong(), guild.asLong(), voicepoints);
    }

    public void voicePointAdd(long userId, long guildId, long voicepoints) {
        String json = buildJson(ImmutableMap.<String, Object>builder()
                .put("voicepoints", voicepoints)
        .build());

        if(json == null) return;

        logger.info("requesting vpadd to " + userId + " with amount " + voicepoints);

        String response = client.request(HttpMethod.PATCH)
                .uri(config.getAPIEndpoint("profile/" + guildId + "/" + userId + "/add"))
                .send(ByteBufFlux.fromString(Mono.just(json)))
                .responseSingle(this::requestHandler).block();

        logger.info(response);
    }



    public void transferVoicePoints(Member fromMember, Guild fromGuild, Member toMember, Guild toGuild, long voicepoints) {
        transferVoicePoints(fromMember.getId().asLong(), fromGuild.getId().asLong(), toMember.getId().asLong(), toGuild.getId().asLong(), voicepoints);
    }

    public void transferVoicePoints(Snowflake fromMember, Snowflake fromGuild, Snowflake toMember, Snowflake toGuild, long voicepoints) {
        transferVoicePoints(fromMember.asLong(), fromGuild.asLong(), toMember.asLong(), toGuild.asLong(), voicepoints);
    }

    public void transferVoicePoints(long fromUserId, long fromGuildId, long toUserId, long toGuildId, long voicepoints) {
        voicePointAdd(fromUserId, fromGuildId, -voicepoints);
        voicePointAdd(toUserId, toGuildId, voicepoints);
    }



    public VoiceProfile getVoiceProfile(Member member, Guild guild) {
        return getVoiceProfile(member.getId().asLong(), guild.getId().asLong());
    }

    public VoiceProfile getVoiceProfile(Snowflake userId, Snowflake guildId) {
        return getVoiceProfile(userId.asLong(), guildId.asLong());
    }

    public VoiceProfile getVoiceProfile(long userId, long guildId) {
        logger.info("requesting voice profile for " + userId);

        String response = client.request(HttpMethod.GET)
                .uri(config.getAPIEndpoint("profile/" + guildId + "/" + userId))
                .responseSingle(this::requestHandler).block();

        if(response == null) return null;
        if(response.contains("Voice profile not found")) return null;

        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);

        try {
            return mapper.readValue(response, VoiceProfile.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void testRequest() {
        client.request(HttpMethod.GET)
                .uri("http://google.com:81/")
                .responseSingle(this::requestHandler).block();
    }

    private void requestErrorHandler(HttpClientRequest httpClientRequest, Throwable throwable) {
        throw new RuntimeException(throwable.toString());
    }

    private void responseErrorHandler(HttpClientResponse response, Throwable throwable) {
        throw new RuntimeException(throwable.toString());
    }

    private Mono<String> requestHandler(HttpClientResponse response, ByteBufMono data) {
        if(response.status().code() == 200)
            return data.asString();
        else {
            if(response.status().code() == 404)
                return data.asString();
            else {
                data.asString().subscribe(logger::error);
                throw new RuntimeException(response.status().toString());
            }
        }
    }
}
