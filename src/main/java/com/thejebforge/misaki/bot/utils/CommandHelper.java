package com.thejebforge.misaki.bot.utils;

import com.google.common.collect.ImmutableMap;
import com.thejebforge.misaki.bot.config.Configuration;
import com.thejebforge.misaki.bot.localisation.BaseLocalisation;
import com.thejebforge.misaki.bot.types.Command;
import discord4j.core.object.entity.channel.TextChannel;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommandHelper {
    public static List<String> parseCommand(String message, String prefix){
        if(message.startsWith(prefix)){
            return new ArrayList<>(Arrays.asList(message.substring(prefix.length()).split(" ")));
        } else return new ArrayList<>();
    }

    public static boolean checkSetOfArguments(BaseLocalisation trn, TextChannel channel, Collection<String> set){
        for(String arg : set){
            if(!checkIfArgumentIsSane(trn, channel, arg)) return false;
        }
        return true;
    }

    public static boolean checkIfArgumentIsSane(BaseLocalisation trn, TextChannel channel, String arg){
        Matcher matcher = Pattern.compile("[^a-z0-9 ._/:\\-!?@\"'#]", Pattern.CASE_INSENSITIVE).matcher(arg);

        if (!matcher.find()) {
            if(arg.length() <= 200) {
                return true;
            } else {
                EmbedResponder.sendMessage(channel, false, trn.formatKey("error.wrong.command.too_long.title"), trn.formatKey("error.wrong.command.too_long.desc"));
            }
        } else {
            EmbedResponder.sendMessage(channel, false, trn.formatKey("error.wrong.command.bad_characters.title"), trn.formatKey("error.wrong.command.bad_characters.desc"));
        }

        return false;
    }

    public static void displayHelp(BaseLocalisation trn, TextChannel channel, String commandName, String description, String usage){
        displayHelp(trn, channel, commandName, description, usage, null);
    }

    public static void displayHelp(BaseLocalisation trn, TextChannel channel, String commandName, String description, String usage, ImmutableMap<String, String> fields){
        LinkedHashMap<String, String> messageFields = new LinkedHashMap<>();

        messageFields.put(trn.formatKey("help.command.usage"), usage);

        if(fields != null)
            messageFields.putAll(fields);

        EmbedResponder.sendMessage(channel, true, trn.formatKey("help.command.title", commandName), description, messageFields);
    }

    public static String helpDirection(Configuration config, Command command) {
        return helpDirection(config, command.getName());
    }

    public static String helpDirection(Configuration config, String command) {
        return config.getPrefix() + "help " + command;
    }
}
