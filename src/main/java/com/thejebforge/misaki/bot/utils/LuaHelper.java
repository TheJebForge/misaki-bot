package com.thejebforge.misaki.bot.utils;

import discord4j.common.util.Snowflake;
import discord4j.core.GatewayDiscordClient;
import discord4j.core.object.entity.Guild;
import discord4j.core.object.entity.channel.Channel;
import discord4j.core.object.entity.channel.GuildChannel;
import discord4j.core.object.entity.channel.MessageChannel;
import discord4j.core.object.entity.channel.VoiceChannel;
import discord4j.core.retriever.EntityRetrievalStrategy;
import discord4j.core.spec.GuildMemberEditSpec;
import discord4j.voice.AudioProvider;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.TimeZone;
import java.util.function.Consumer;

public class LuaHelper {
    public static TimeZone makeTimezone(String id) {
        return TimeZone.getTimeZone(id);
    }

    public static long makeLong(String id) {
        return new BigInteger(id).longValue();
    }

    public static Snowflake makeSnowflake(String id) {
        return Snowflake.of(id);
    }

    public static Consumer<? super GuildMemberEditSpec> makeGuildMemberEditSpec(String nickname) {
        return (guildMemberEditSpec -> guildMemberEditSpec.setNickname(nickname));
    }

    public static Guild findGuild(String id, GatewayDiscordClient gateway) {
        return gateway.getGuildById(makeSnowflake(id)).block();
    }

    public static GuildChannel findChannel(String id, Guild guild) {
        return guild.getChannelById(makeSnowflake(id)).block();
    }

    public static GuildChannel findChannel(String id, String guildId, GatewayDiscordClient gateway) {
        return findGuild(guildId, gateway).getChannelById(makeSnowflake(id)).block();
    }

    public static void joinChannel(VoiceChannel channel, AudioProvider provider) {
        channel.join(voiceChannelJoinSpec -> voiceChannelJoinSpec.setProvider(provider)).block();
    }

    public static String mention(String id) {
        return "<@" + id + ">";
    }

    public static void sendMessage(String message, MessageChannel channel) {
        channel.createMessage(message.substring(0, Math.min(message.length(), 1999))).block();
    }

    public static String execute(String command) throws IOException {
        return IOUtils.toString(Runtime.getRuntime().exec(command).getInputStream(), StandardCharsets.UTF_8);
    }

    public static EntityRetrievalStrategy getStrategy() {
        return EntityRetrievalStrategy.REST;
    }
}
