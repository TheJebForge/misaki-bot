package com.thejebforge.misaki.bot.utils;

import com.thejebforge.misaki.bot.localisation.BaseLocalisation;
import com.thejebforge.misaki.bot.localisation.TranslatorLanguage;
import com.thejebforge.misaki.bot.types.BotAccess;
import com.thejebforge.misaki.bot.types.BotArgument;
import com.thejebforge.misaki.bot.types.CustomCommandScope;
import com.thejebforge.misaki.bot.types.DailyTopType;
import discord4j.core.object.entity.channel.TextChannel;
import discord4j.core.object.presence.Activity;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Parameter;
import java.lang.reflect.ParameterizedType;
import java.util.*;

// I think I know what I'm doing?..
@SuppressWarnings({"rawtypes", "unchecked"})
public class ArgumentProcessor {
    private static final Map<Class, List<Object>> supportedEnums = new HashMap<Class, List<Object>>(){{
        put(BotAccess.class, Arrays.asList(BotAccess.values()));
        put(CustomCommandScope.class, Arrays.asList(CustomCommandScope.values()));
        put(DailyTopType.class, Arrays.asList(DailyTopType.values()));
        put(Activity.Type.class, Arrays.asList(Activity.Type.values()));
        put(TranslatorLanguage.class, Arrays.asList(TranslatorLanguage.values()));
    }};


    public static Object processArgument(List<String> args, Parameter parameter, boolean single, BaseLocalisation trn, TextChannel channel) {
        BotArgument botArgument = parameter.getAnnotation(BotArgument.class);
        Class<?> requestedClass = parameter.getType();

        if(args.size() > 0) {
            if(requestedClass.equals(String.class)) {
                // All strings
                    String result = botArgument.takeRest() ? String.join(" ", args) : single ? String.join(" ", args) : args.remove(0);

                    if(botArgument.checkValidity()) if (!CommandHelper.checkIfArgumentIsSane(trn, channel, result)) return null;

                    return result;
            } else {
                if(requestedClass.isEnum()) {
                    String query = args.remove(0).toUpperCase(Locale.ROOT);

                    for(Class supportedEnum : supportedEnums.keySet()) {
                        if(requestedClass.equals(supportedEnum)) {
                            try {
                                if(supportedEnum.equals(TranslatorLanguage.class))
                                    return Enum.valueOf(supportedEnum, StringUtils.capitalize(query.toLowerCase(Locale.ROOT)));

                                return Enum.valueOf(supportedEnum, query);
                            } catch (IllegalArgumentException e) {
                                StringBuilder allVariants = new StringBuilder("\n");

                                for(Object en : supportedEnums.get(supportedEnum)) {
                                    allVariants.append(((Enum) en).name()).append("\n");
                                }

                                EmbedResponder.sendMessage(channel, false, trn.formatKey("error.unknown."+botArgument.value()+".title"), trn.formatKey("error.unknown."+botArgument.value()+".desc", allVariants.toString()));
                                return null;
                            }
                        }
                    }
                } else {
                    if(requestedClass.equals(List.class)) {
                        Class<?> listType = (Class<?>) ((ParameterizedType) parameter.getParameterizedType()).getActualTypeArguments()[0];

                        if(listType.equals(String.class)) {
                            if(botArgument.checkValidity()) if (!CommandHelper.checkSetOfArguments(trn, channel, args)) return null;

                            return args;
                        }
                    }

                    if(requestedClass.equals(Integer.class)) {
                        String integer = args.remove(0);

                        if(botArgument.checkValidity()) if (!CommandHelper.checkIfArgumentIsSane(trn, channel, integer)) return null;

                        try {
                            return Integer.parseInt(integer);
                        } catch (NumberFormatException e) {
                            EmbedResponder.sendMessage(channel, false, trn.formatKey("error.unknown."+botArgument.value()+".title"), trn.formatKey("error.unknown."+botArgument.value()+".desc"));
                            return null;
                        }
                    }

                    if(requestedClass.equals(TimeZone.class)) {
                        String timezoneId = args.remove(0);

                        if(botArgument.checkValidity()) if (!CommandHelper.checkIfArgumentIsSane(trn, channel, timezoneId)) return null;

                        try {
                            return TimeZone.getTimeZone(timezoneId);
                        } catch (IllegalArgumentException e) {
                            EmbedResponder.sendMessage(channel, false, trn.formatKey("error.unknown."+botArgument.value()+".title"), trn.formatKey("error.unknown."+botArgument.value()+".desc"));
                            return null;
                        }
                    }




                }
            }
        } else {
            if(botArgument.checkExistence())
                EmbedResponder.sendMessage(channel, false, trn.formatKey("error.missing."+botArgument.value()+".title"), trn.formatKey("error.missing."+botArgument.value()+".desc"));
            return null;
        }

        return null;
    }
}
