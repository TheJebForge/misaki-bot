package com.thejebforge.misaki.bot.utils;

import com.thejebforge.misaki.bot.manager.DatabaseManager;
import discord4j.core.GatewayDiscordClient;
import discord4j.core.object.presence.Activity;
import discord4j.core.object.presence.ClientActivity;
import discord4j.core.object.presence.ClientPresence;
import discord4j.core.object.presence.Status;
import discord4j.discordjson.json.ActivityUpdateRequest;
import discord4j.discordjson.json.ImmutableActivityUpdateRequest;
import discord4j.discordjson.json.gateway.ImmutableStatusUpdate;
import discord4j.discordjson.json.gateway.StatusUpdate;
import discord4j.gateway.json.GatewayPayload;

import java.util.Collections;
import java.util.Optional;

public class StatusHelper {

    private static Status previousStatus = Status.ONLINE;

    public static void setStatus(GatewayDiscordClient gateway, String name, Activity.Type type){
        setStatus(gateway, name, type, null);
    }

    public static void setStatus(GatewayDiscordClient gateway, String name, Activity.Type type, Status status) {
        if(status == null){
            status = previousStatus;
        }

        ClientActivity activity;

        switch(type) {
            case PLAYING:
                activity = ClientActivity.playing(name);
                break;
            case WATCHING:
                activity = ClientActivity.watching(name);
                break;
            case COMPETING:
                activity = ClientActivity.competing(name);
                break;
            case STREAMING:
                activity = ClientActivity.streaming(name, "");
                break;
            case LISTENING:
                activity = ClientActivity.listening(name);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + type);
        }

        ClientPresence presence;

        switch(status) {
            case ONLINE:
                presence = ClientPresence.online(activity);
                break;
            case IDLE:
                presence = ClientPresence.idle(activity);
                break;
            case DO_NOT_DISTURB:
                presence = ClientPresence.doNotDisturb(activity);
                break;
            case INVISIBLE:
                presence = ClientPresence.invisible();
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + status);
        }

        gateway.updatePresence(presence);
        previousStatus = status;
    }

    public static void setParsedStatus(DatabaseManager db, GatewayDiscordClient gateway, String name, Activity.Type type) {
        setParsedStatus(db, gateway, name, type, null);
    }

    public static void setParsedStatus(DatabaseManager db, GatewayDiscordClient gateway, String name, Activity.Type type, Status status){
        String parsedMessage = name;
        parsedMessage = parsedMessage.replace("#", Integer.toString(db.getAllGuilds().size()));

        setStatus(gateway, parsedMessage, type, status);
    }
}
