package com.thejebforge.misaki.bot.utils;

import com.thejebforge.misaki.bot.manager.DatabaseManager;
import com.thejebforge.misaki.runtime.MainBotProcess;
import org.apache.commons.lang3.RandomStringUtils;

public class KeyGen {
    public static String generateRandomKey() {
        return RandomStringUtils.randomAlphabetic(8);
    }

    public static String generateUniqueKey() {
        String key = generateRandomKey();
        while(MainBotProcess.db.getLoginFromKey(key) != null) key = generateRandomKey();
        return key;
    }

    public static String generateRandomCookie() {
        return RandomStringUtils.randomAlphabetic(50);
    }

    public static String generateUniqueCookie() {
        String key = generateRandomCookie();
        while(MainBotProcess.db.getLoginFromCookie(key) != null) key = generateRandomCookie();
        return key;
    }
}
