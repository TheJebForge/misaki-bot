package com.thejebforge.misaki.bot.utils;

import com.google.common.collect.Lists;
import discord4j.core.GatewayDiscordClient;
import discord4j.core.object.entity.Guild;
import discord4j.discordjson.json.ApplicationCommandData;
import discord4j.discordjson.json.ApplicationCommandOptionChoiceData;
import discord4j.discordjson.json.ApplicationCommandOptionData;
import discord4j.discordjson.json.ApplicationCommandRequest;
import discord4j.rest.RestClient;
import discord4j.rest.util.ApplicationCommandOptionType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Mono;

public class InteractionsHelper {
    private static Logger logger = LoggerFactory.getLogger(InteractionsHelper.class);


    public static void addCommand(GatewayDiscordClient gateway, long guild) {
        logger.info("attempting to add");
        RestClient client = gateway.getRestClient();

        ApplicationCommandRequest request = ApplicationCommandRequest.builder()
                .name("testsub")
                .description("Command that is furry")
                .addOption(ApplicationCommandOptionData.builder()
                        .name("subcommand")
                        .description("subcommands test")
                        .type(ApplicationCommandOptionType.SUB_COMMAND_GROUP.getValue())
                        .addOption(ApplicationCommandOptionData.builder()
                                .name("command1")
                                .description("subcommand test 1")
                                .type(ApplicationCommandOptionType.SUB_COMMAND.getValue())
                                .build())
                        .addOption(ApplicationCommandOptionData.builder()
                                .name("command2")
                                .description("subcommand test 2")
                                .type(ApplicationCommandOptionType.SUB_COMMAND.getValue())
                                .build())
                        .build())
                .build();

        Long applicationId = client.getApplicationId().block();

        if(applicationId != null) {
            logger.info("requesting");
            client.getApplicationService()
                    .createGuildApplicationCommand(applicationId, guild, request)
                    .doOnError(e -> logger.warn("Unable to create guild command", e))
                    .onErrorResume(e -> Mono.empty())
                    .block();
        } else {
            logger.info("missing app");
        }
    }
}
