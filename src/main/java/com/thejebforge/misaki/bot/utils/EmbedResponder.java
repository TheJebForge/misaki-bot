package com.thejebforge.misaki.bot.utils;

import com.thejebforge.misaki.bot.manager.DatabaseManager;
import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.channel.MessageChannel;
import discord4j.core.spec.EmbedCreateSpec;
import discord4j.rest.util.Color;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.time.Instant;
import java.util.LinkedHashMap;
import java.util.Map;

public class EmbedResponder {

    public static DatabaseManager db;

    public static Message sendMessage(MessageChannel channel, boolean success, String title){
        return sendMessage(channel, success, title, "");
    }

    public static Message sendMessage(MessageChannel channel, boolean success, String title, Path attachedImage){
        return sendMessage(channel, success, title, "", null, null, false, attachedImage, "", null);
    }

    public static Message sendMessage(MessageChannel channel, boolean success, String title, String description){
        return sendMessage(channel, success, title, description, null, null, false, null, "", null);
    }

    public static Message sendMessage(MessageChannel channel, boolean success, String title, String description, Path attachedImage){
        return sendMessage(channel, success, title, description, null, null, false, attachedImage, "", null);
    }

    public static Message sendMessage(MessageChannel channel, boolean success, String title, LinkedHashMap<String, String> fields){
        return sendMessage(channel, success, title, "", fields, null, false, null, "", null);
    }

    public static Message sendMessage(MessageChannel channel, boolean success, String title, LinkedHashMap<String, String> fields, boolean inline){
        return sendMessage(channel, success, title, "", fields, null, inline , null, "", null);
    }

    public static Message sendMessage(MessageChannel channel, boolean success, String title, LinkedHashMap<String, String> fields, Map<String, Boolean> specificInline, boolean inline){
        return sendMessage(channel, success, title, "", fields, specificInline, inline, null, "", null);
    }

    public static Message sendMessage(MessageChannel channel, boolean success, String title, String description, LinkedHashMap<String, String> fields){
        return sendMessage(channel, success, title, description, fields, null, false, null, "", null);
    }

    public static Message sendMessage(MessageChannel channel, boolean success, String title, String description, LinkedHashMap<String, String> fields, Path attachedImage){
        return sendMessage(channel, success, title, description, fields, null, false, attachedImage, "", null);
    }

    public static Message sendMessage(MessageChannel channel, boolean success, LinkedHashMap<String, String> fields, Path attachedImage){
        return sendMessage(channel, success, "", "", fields, null, false, attachedImage, "", null);
    }

    public static Message sendMessage(MessageChannel channel, boolean success, String title, String description, LinkedHashMap<String, String> fields, boolean inline){
        return sendMessage(channel, success, title, description, fields, null, inline, null, "", null);
    }

    public static Message sendMessage(MessageChannel channel, boolean success, String title, String description, LinkedHashMap<String, String> fields, Map<String, Boolean> specificInline, boolean defaultInline, Path attachedImage, String subtext, Instant timestamp){

        final FileInputStream[] fs = {null};
        Message ms = channel.createMessage(messageCreateSpec -> {
            String filename = null;

            if(attachedImage != null) {
                filename = attachedImage.getFileName().toString();

                try {
                    fs[0] = new FileInputStream(attachedImage.toFile());
                    messageCreateSpec.addFile(filename, fs[0]);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    return;
                }
            }

            String finalFilename = filename;

            messageCreateSpec.setEmbed(embedCreateSpec -> createEmbed(embedCreateSpec, title, success, description, fields, specificInline, defaultInline, attachedImage, finalFilename, subtext, timestamp));
        }).block();

        if(fs[0] != null){
            try {
                fs[0].close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return ms;
    }

    public static Message editMessage(Message message, boolean success, String title) {
        return editMessage(message, success, title, "", null, null, false, "", null);
    }

    public static Message editMessage(Message message, boolean success, String title, LinkedHashMap<String, String> fields) {
        return editMessage(message, success, title, "", fields, null, false, "", null);
    }

    public static Message editMessage(Message message, boolean success, String title, LinkedHashMap<String, String> fields, boolean defaultInline) {
        return editMessage(message, success, title, "", fields, null, defaultInline, "", null);
    }

    public static Message editMessage(Message message, boolean success, String title, LinkedHashMap<String, String> fields, Map<String, Boolean> specificInline) {
        return editMessage(message, success, title, "", fields, specificInline, false, "", null);
    }

    public static Message editMessage(Message message, boolean success, String title, LinkedHashMap<String, String> fields, Map<String, Boolean> specificInline, boolean defaultInline) {
        return editMessage(message, success, title, "", fields, specificInline, defaultInline, "", null);
    }

    public static Message editMessage(Message message, boolean success, String title, String description) {
        return editMessage(message, success, title, description, null, null, false, "", null);
    }

    public static Message editMessage(Message message, boolean success, String title, String description, LinkedHashMap<String, String> fields) {
        return editMessage(message, success, title, description, fields, null, false, "", null);
    }

    public static Message editMessage(Message message, boolean success, String title, String description, LinkedHashMap<String, String> fields, String subtext) {
        return editMessage(message, success, title, description, fields, null, false, subtext, null);
    }

    public static Message editMessage(Message message, boolean success, String title, String description, LinkedHashMap<String, String> fields, Instant timestamp) {
        return editMessage(message, success, title, description, fields, null, false, "", timestamp);
    }

    public static Message editMessage(Message message, boolean success, String title, String description, LinkedHashMap<String, String> fields, boolean defaultInline) {
        return editMessage(message, success, title, description, fields, null, defaultInline, "", null);
    }

    public static Message editMessage(Message message, boolean success, String title, String description, LinkedHashMap<String, String> fields, Map<String, Boolean> specificInline, boolean defaultInline, String subtext, Instant timestamp) {

        return message.edit(messageEditSpec -> messageEditSpec.setEmbed(embedCreateSpec -> createEmbed(embedCreateSpec, title, success, description, fields, specificInline, defaultInline, null, null, subtext, timestamp))).block();
    }







    private static void createEmbed(EmbedCreateSpec embedCreateSpec, String title, boolean success, String description, LinkedHashMap<String, String> fields, Map<String, Boolean> specificInline, boolean defaultInline, Path attachedImage, String finalFilename, String subtext, Instant timestamp) {
        embedCreateSpec.setTitle(title);
        embedCreateSpec.setColor(success ? Color.DEEP_LILAC : Color.RED);
        if (!description.isEmpty()) embedCreateSpec.setDescription(description);

        if(!subtext.isEmpty()) embedCreateSpec.setFooter(subtext, "");

        if(timestamp != null) embedCreateSpec.setTimestamp(timestamp);

        if (fields != null) {
            for (Map.Entry<String, String> field : fields.entrySet()) {
                if (specificInline == null)
                    embedCreateSpec.addField(field.getKey(), field.getValue(), defaultInline);
                else {
                    if (specificInline.get(field.getKey()) != null) {
                        embedCreateSpec.addField(field.getKey(), field.getValue(), specificInline.get(field.getKey()));
                    } else {
                        embedCreateSpec.addField(field.getKey(), field.getValue(), defaultInline);
                    }
                }
            }
        }

        if(attachedImage != null){
            embedCreateSpec.setImage("attachment://"+ finalFilename);
        }

    }
}
