package com.thejebforge.misaki.dao;

import com.thejebforge.misaki.bot.types.DailyTopType;
import discord4j.core.object.entity.Guild;
import discord4j.core.object.entity.Message;
import lombok.Data;
import org.dizitart.no2.IndexType;
import org.dizitart.no2.objects.Index;
import org.dizitart.no2.objects.Indices;

import java.util.Objects;

@Data
@Indices({
        @Index(value = "messageId", type = IndexType.Unique)
})
public class DailyTopSubscription {
    private long messageId;
    private long channelId;
    private long guildId;
    private DailyTopType type;

    public DailyTopSubscription() { }

    public DailyTopSubscription(Message message, Guild guild, DailyTopType type) {
        this.messageId = message.getId().asLong();
        this.channelId = Objects.requireNonNull(message.getChannel().block()).getId().asLong();
        this.guildId = guild.getId().asLong();
        this.type = type;
    }
}
