package com.thejebforge.misaki.dao;

import com.thejebforge.misaki.bot.types.BotAccess;
import com.thejebforge.misaki.bot.types.CustomCommandScope;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dizitart.no2.objects.Index;
import org.dizitart.no2.objects.Indices;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Indices({
        @Index("name")
})
public class CustomCommand {
    private String name;
    private CustomCommandScope scope;
    private BotAccess access;
    private long guildId;
    private String code;
}
