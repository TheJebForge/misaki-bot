package com.thejebforge.misaki.dao;

import org.dizitart.no2.IndexType;
import org.dizitart.no2.objects.Index;
import org.dizitart.no2.objects.Indices;

import java.util.HashSet;
import java.util.Set;

@Indices({
        @Index(value = "filename", type = IndexType.Unique)
})
public class ImageData {
    // Filename

    private String filename;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    // Tags

    public Set<String> tags;

    public Set<String> getTags() {
        return tags;
    }

    public void addTag(String tag) {
        tags.add(tag);
    }

    public void removeTag(String tag) {
        tags.remove(tag);
    }

    public boolean hasTag(String tag) {
        return tags.contains(tag);
    }

    // Constructor

    public ImageData() {
        tags = new HashSet<>();
    }

    public ImageData(String filename, Set<String> tags){
        this();

        this.filename = filename;
        this.tags = tags;
    }
}
