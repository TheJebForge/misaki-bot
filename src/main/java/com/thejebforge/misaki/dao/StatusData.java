package com.thejebforge.misaki.dao;

import discord4j.core.object.presence.Activity;
import org.dizitart.no2.IndexType;
import org.dizitart.no2.objects.Index;
import org.dizitart.no2.objects.Indices;


@Indices({
        @Index(value = "message", type = IndexType.Unique)
})
public class StatusData {
    private String message;
    private Activity.Type activity;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Activity.Type getActivity() {
        return activity;
    }

    public void setActivity(Activity.Type activity) {
        this.activity = activity;
    }

    public StatusData(){

    }

    public StatusData(String message, Activity.Type type){
        this.message = message;
        this.activity = type;
    }



}
