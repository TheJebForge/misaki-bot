package com.thejebforge.misaki.dao;

import lombok.Data;
import org.dizitart.no2.IndexType;
import org.dizitart.no2.objects.Index;
import org.dizitart.no2.objects.Indices;

import java.sql.Timestamp;
import java.time.Instant;

@Data
@Indices({
        @Index(value = "key", type = IndexType.Unique),
        @Index(value = "cookie", type = IndexType.Unique),
        @Index(value = "deletionTime", type = IndexType.NonUnique),
        @Index(value = "userId", type = IndexType.NonUnique)
})
public class SlotsWebLogin {
    private String key;
    private String cookie;
    private Instant deletionTime;

    private long userId;
    private long guildId;

    private long total;
    private long previousBet;

    public SlotsWebLogin() {}
    public SlotsWebLogin(String key) {
        this.key = key;
        this.deletionTime = Instant.now().plusSeconds(3600);
    }

    public void bumpDeletionTime() {
        deletionTime = Instant.now().plusSeconds(60 * 60 * 24 * 7);
    }
}
