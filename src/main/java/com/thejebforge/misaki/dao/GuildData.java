package com.thejebforge.misaki.dao;

import com.thejebforge.misaki.bot.config.Configuration;
import com.thejebforge.misaki.bot.localisation.BaseLocalisation;
import com.thejebforge.misaki.bot.localisation.TranslatorLanguage;
import com.thejebforge.misaki.bot.modules.GenericModule;
import com.thejebforge.misaki.runtime.MainBotProcess;
import discord4j.common.util.Snowflake;
import discord4j.core.object.entity.Guild;
import discord4j.core.object.entity.Member;
import org.dizitart.no2.IndexType;
import org.dizitart.no2.objects.Id;
import org.dizitart.no2.objects.Index;
import org.dizitart.no2.objects.Indices;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.TimeZone;

@Indices({
        @Index(value = "guildId", type = IndexType.Unique)
})
public class GuildData implements Serializable {
    // Guild ID

    @Id
    private long guildId;

    public void setGuildId(long guildId) {
        this.guildId = guildId;
    }

    public long getGuildId(){
        return this.guildId;
    }

    public boolean compare(@NotNull Guild guild){
        return guild.getId().asLong() == this.guildId;
    }

    public boolean compare(@NotNull Snowflake guildId){
        return guildId.asLong() == this.guildId;
    }

    public boolean compare(long guildId){
        return guildId == this.guildId;
    }

    // Modules

    private final HashSet<String> enabledModules;

    public String[] getEnabledModules(){
        return enabledModules.toArray(new String[0]);
    }

    public void addModule(GenericModule module){
        enabledModules.add(module.getCodeName());
    }

    public void removeModule(GenericModule module){
        enabledModules.remove(module.getCodeName());
    }

    public boolean hasModuleEnabled(GenericModule module) {
        return enabledModules.contains(module.getCodeName());
    }

    // Permission id

    private int permissionLevel;

    public int getPermissionLevel(){
        return this.permissionLevel;
    }

    public void setPermissionLevel(int permissionLevel){
        this.permissionLevel = permissionLevel;
    }

    // Users permitted to manage the bot

    private Set<Long> managers;

    public Set<Long> getManagers() { return this.managers; }

    public void addManager(long manager) {
        this.managers.add(manager);
    }

    public void removeManager(long manager) {
        this.managers.remove(manager);
    }

    public boolean isManager(Member user) {
        return this.managers.contains(user.getId().asLong());
    }

    // Translation

    public TranslatorLanguage lang;

    public void setLanguage(TranslatorLanguage lang) {
        this.lang = lang;
    }

    public TranslatorLanguage getLanguage() {
        return this.lang == null ? TranslatorLanguage.English : this.lang;
    }

    public BaseLocalisation getTranslator() {
        return MainBotProcess.translators.get(lang == null ? TranslatorLanguage.English : lang);
    }

    // Timezone

    private TimeZone timezone;

    public TimeZone getTimezone() {
        return timezone == null ? TimeZone.getTimeZone("UTC"): timezone;
    }

    public void setTimezone(TimeZone timezone) {
        this.timezone = timezone;
    }

    // Constructor

    public GuildData(){
        enabledModules = new HashSet<>();
        managers = new HashSet<>();
    }

    public GuildData(Guild guild){
        this();

        // Default values
        permissionLevel = 1;

        enabledModules.add("config_module");
        enabledModules.add("help_module");

        managers.add(new Configuration().getOwnerID());

        lang = TranslatorLanguage.English;

        timezone = TimeZone.getTimeZone("UTC");

        this.guildId = guild.getId().asLong();
    }
}
