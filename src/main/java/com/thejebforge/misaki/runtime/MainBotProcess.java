package com.thejebforge.misaki.runtime;

import com.google.common.collect.ImmutableMap;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers;
import com.sedmelluq.discord.lavaplayer.track.playback.NonAllocatingAudioFrameBuffer;
import com.thejebforge.misaki.bot.config.Configuration;
import com.thejebforge.misaki.bot.events.BotEventDispatcher;
import com.thejebforge.misaki.bot.events.ReactionAddedEvent;
import com.thejebforge.misaki.bot.localisation.BaseLocalisation;
import com.thejebforge.misaki.bot.localisation.TranslatorLanguage;
import com.thejebforge.misaki.bot.manager.DatabaseManager;
import com.thejebforge.misaki.bot.manager.VoiceProfileManager;
import com.thejebforge.misaki.bot.modules.GenericModule;
import com.thejebforge.misaki.bot.types.*;
import com.thejebforge.misaki.bot.utils.*;
import com.thejebforge.misaki.dao.GuildData;
import com.thejebforge.misaki.dao.StatusData;
import discord4j.core.DiscordClient;
import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.ReactiveEventAdapter;
import discord4j.core.event.domain.InteractionCreateEvent;
import discord4j.core.event.domain.guild.GuildCreateEvent;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.event.domain.message.ReactionAddEvent;
import discord4j.core.object.entity.Guild;
import discord4j.core.object.entity.Member;
import discord4j.core.object.entity.channel.Channel;
import discord4j.core.object.entity.channel.MessageChannel;
import discord4j.core.object.entity.channel.TextChannel;
import discord4j.core.object.presence.Activity;
import discord4j.rest.util.Permission;
import discord4j.rest.util.PermissionSet;
import discord4j.voice.AudioProvider;
import org.jetbrains.annotations.NotNull;
import org.reactivestreams.Publisher;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.util.*;

@Component
public class MainBotProcess {
    private static final Logger logger = LoggerFactory.getLogger(MainBotProcess.class);

    public boolean alive = true;

    public Configuration config;
    public DiscordClient client;
    public static GatewayDiscordClient gateway;

    public static DatabaseManager db;

    public static VoiceProfileManager vp;

    public static Map<String, GenericModule> moduleMap;
    public static Map<String, Command> commandMap;
    public static Map<GenericModule, List<Command>> moduleCommandsMap;

    public static Map<TranslatorLanguage, BaseLocalisation> translators;
    public static BotEventDispatcher botEvents;

    public static AudioPlayerManager playerManager;
    public static AudioPlayer player;
    public static AudioProvider provider;
    public static TrackScheduler trackScheduler;

    public MainBotProcess(){
        // Initializations
        config = new Configuration();

        moduleMap = new HashMap<>();
        commandMap = new HashMap<>();
        moduleCommandsMap = new HashMap<>();

        translators = new HashMap<>();
        botEvents = new BotEventDispatcher();

        // Initializing music shit
        playerManager = new DefaultAudioPlayerManager();

        playerManager.getConfiguration()
                .setFrameBufferFactory(NonAllocatingAudioFrameBuffer::new);
        AudioSourceManagers.registerLocalSource(playerManager);
        AudioSourceManagers.registerRemoteSources(playerManager);

        player = playerManager.createPlayer();
        provider = new LavaPlayerAudioProvider(player);
        trackScheduler = new TrackScheduler(player);

        Reflections reflections = new Reflections("com.thejebforge.misaki.bot");


        // Login
        client = DiscordClient.create(config.getToken());
        gateway = client.login().block();

        if(gateway == null){
            System.err.println("Can't login");
            alive = false;
            return;
        }

        // Permission manager
        db = new DatabaseManager();
        EmbedResponder.db = db;

        // Voice Profile Manager
        vp = new VoiceProfileManager();

        // Default status
        db.addStatusMessage(new StatusData("over # guilds", Activity.Type.WATCHING));

        // Initializing translators
        for(Class<? extends BaseLocalisation> languageClass : reflections.getSubTypesOf(BaseLocalisation.class)) {
            try {
                BaseLocalisation language = languageClass.getConstructor().newInstance();
                translators.put(language.getLanguageEnum(), language);
            } catch (InstantiationException | NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
                logger.info("Failed to initialize localisation class: " + languageClass.getName());
                e.printStackTrace();
            }
        }

        // Preparing module array
        List<GenericModule> modules = new ArrayList<>();

        // Looking up modules
        Set<Class<? extends GenericModule>> foundModules = reflections.getSubTypesOf(GenericModule.class);

        // Initializing all found modules
        for(Class<? extends GenericModule> moduleClass : foundModules) {
            try {
                GenericModule module = moduleClass.getConstructor(DatabaseManager.class, GatewayDiscordClient.class).newInstance(db, gateway);

                modules.add(module);
            } catch (InstantiationException | NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
                logger.info("Failed to initialize module: " + moduleClass.getName());
                e.printStackTrace();
            }
        }

        // Registering commands + init
        for(GenericModule module : modules){
            module.init();

            // Finding all commands of module
            List<Method> foundMethodWithAnnotations = new ArrayList<>(Arrays.asList(module.getClass().getMethods()));

            foundMethodWithAnnotations.removeIf(method -> method.getAnnotation(BotCommand.class) == null);
            foundMethodWithAnnotations.removeIf(method -> !Modifier.isPublic(method.getModifiers()));

            Map<String, Method> foundCommands = new HashMap<>();
            Map<String, Command> createdCommands = new HashMap<>();

            for(Method method : foundMethodWithAnnotations) {
                BotCommand annotation = method.getAnnotation(BotCommand.class);
                BotHelp helpAnnotation = method.getAnnotation(BotHelp.class);

                Command commandObject = new Command(annotation.id(), annotation.name(), module, method, annotation.executable());

                if(helpAnnotation != null) commandObject.setCommandHelp(new CommandHelp(helpAnnotation.value(), helpAnnotation.childAsSections(), helpAnnotation.hidden(), helpAnnotation.order()));
                if(!annotation.childType().isEmpty()) commandObject.setChildTypeTrnKey(annotation.childType());

                foundCommands.put(annotation.id(), method);
                createdCommands.put(annotation.id(), commandObject);
            }

            // Parenting
            for(Method method : foundCommands.values()) {
                BotCommand annotation = method.getAnnotation(BotCommand.class);

                if(createdCommands.get(annotation.parent()) != null) {
                    Command child = createdCommands.get(annotation.id());
                    Command parent = createdCommands.get(annotation.parent());

                    child.setParent(parent);
                    parent.getChildren().add(child);
                }
            }

            // Setting permissions
            for(Method method : foundCommands.values()) {
                BotCommand botCommand = method.getAnnotation(BotCommand.class);
                BotPermission permission = method.getAnnotation(BotPermission.class);

                if(permission != null) {
                    if(permission.recursive())
                        setPermissionsRecursively(permission.value(), createdCommands.get(botCommand.id()), permission.override());
                    else
                        createdCommands.get(botCommand.id()).setPermission(permission.value());
                }
            }

            // Retrieving arguments
            for(Method method : foundCommands.values()) {
                BotCommand botCommand = method.getAnnotation(BotCommand.class);
                for(Parameter parameter : method.getParameters()) {
                    BotArgument argument = parameter.getAnnotation(BotArgument.class);

                    if(argument != null) {
                        createdCommands.get(botCommand.id()).addArgument(parameter);
                    }
                }
            }

            // Setting unannotated command permissions
            for(Command command : createdCommands.values()) if(command.getPermission() == null) command.setPermission(BotAccess.ANYONE);

            // Getting master commands
            List<Command> masterCommands = new ArrayList<>(createdCommands.values());
            masterCommands.removeIf(command -> command.getParent() != null);

            // Adding to command map
            for(Command command : masterCommands) commandMap.put(command.getName(), command);

            // Setting list of commands
            moduleCommandsMap.put(module, masterCommands);

            moduleMap.put(module.getCodeName(), module);
        }

        // Registering callbacks
        gateway.on(GuildCreateEvent.class).subscribe(this::processGuild);
        gateway.on(MessageCreateEvent.class).subscribe(this::processMessage);
        gateway.on(ReactionAddEvent.class).subscribe(this::processReactionAdded);
        gateway.on(new InteractionResponder()).subscribe();

        InteractionsHelper.addCommand(gateway, 769256933894914061L);
    }

    private class InteractionResponder extends ReactiveEventAdapter {
        @NotNull
        @Override
        public Publisher<?> onInteractionCreate(InteractionCreateEvent event) {
            logger.info("Received command "+event.getCommandName());
            logger.info(event.getInteraction().getCommandInteraction().getOptions().toString());

            event.getInteraction().getCommandInteraction().getOptions().stream().findAny().flatMap(option -> option.getOptions().stream().findAny()).ifPresent(option2 -> logger.info(option2.getName()));

            return event.acknowledge().then(
                    event.getInteractionResponse().createFollowupMessage("Hui test")
            );
        }
    }




    private void setPermissionsRecursively(BotAccess permission, Command command, boolean override) {
        if(command.getChildren().size() > 0) {
            for(Command child : command.getChildren()) {
                setPermissionsRecursively(permission, child, override);
            }
        } else {
            if(override)
                command.setPermission(permission);
            else {
                if (command.getPermission() == null)
                    command.setPermission(permission);
            }
        }
    }

    private void makeSureGuildHasManagers(Guild guild){
        if(db.guildExists(guild)) {
            GuildData data = db.getGuild(guild);

            if(data.getManagers().size() <= 0) {
                for(Member member : guild.getMembers().toIterable()){
                    PermissionSet perms = member.getBasePermissions().block();

                    if(perms != null){
                        if(perms.contains(Permission.ADMINISTRATOR)) {
                            long userId = member.getId().asLong();

                            if(userId != gateway.getSelfId().asLong()) {
                                logger.info("User: " + member.getDisplayName() + " is admin! Giving permissions");
                                data.addManager(userId);
                            }
                        }
                    }
                }
            }

            if(!data.getManagers().contains(config.getOwnerID())) {
                data.addManager(config.getOwnerID());
            }

            db.setGuild(data);
        }
    }

    public void processGuild(GuildCreateEvent event){
        logger.info("Existing in "+event.getGuild().getName());

        if(!db.guildExists(event.getGuild()))
            db.setGuild(new GuildData(event.getGuild()));

        makeSureGuildHasManagers(event.getGuild());
    }

    private void processCommandTree(BaseLocalisation trn, Command baseCommand, Command command, List<String> args, Guild guild, TextChannel textChannel, Member user, MessageCreateEvent event) {
        // Check for subcommands
        if(command.getChildren().isEmpty()){
            // No subcommands
            if(!command.getPermission().equals(BotAccess.ANYONE)) {
                switch (command.getPermission()) {
                    case MANAGERS: {
                            GuildData data = db.getGuild(guild);
                            if (!data.isManager(user)) {
                                EmbedResponder.sendMessage(textChannel, false, trn.formatKey("error.permission.use.not_manager.title"), trn.formatKey("error.permission.use.not_manager.desc"));
                                return;
                            }
                        }
                        break;

                    case OWNER: {
                            if (user.getId().asLong() != config.getOwnerID()) {
                                EmbedResponder.sendMessage(textChannel, false, trn.formatKey("error.permission.use.not_owner.title"), trn.formatKey("error.permission.use.not_owner.desc"));
                                return;
                            }
                        }
                        break;
                }
            }

            if(command.isExecutable())
                command.runCommand(args, guild, textChannel, user, event);
            else
                EmbedResponder.sendMessage(textChannel, false, trn.formatKey("error.unknown.command.title"), trn.formatKey("error.unknown.command.desc"),
                        new LinkedHashMap<>(
                                ImmutableMap.<String, String>builder()
                                        .put(trn.formatKey("error.unknown.command.field"), command.getName())
                                        .build()));
        } else {
            // Subcommands
            if(args.size() > 0) {
                String subcommand = args.remove(0);

                if (!CommandHelper.checkIfArgumentIsSane(trn, textChannel, subcommand)) return;

                for (Command child : command.getChildren()) {
                    if (child.getName().equals(subcommand)) {
                        processCommandTree(trn, baseCommand, child, args, guild, textChannel, user, event);
                        return;
                    }
                }

                EmbedResponder.sendMessage(textChannel, false, trn.formatKey("error.unknown.subcommand."+command.getChildTypeTrnKey()+".title"), trn.formatKey("error.unknown.subcommand."+command.getChildTypeTrnKey()+".desc", CommandHelper.helpDirection(config, baseCommand)));
            } else {
                if(!command.getPermission().equals(BotAccess.ANYONE)) {
                    switch (command.getPermission()) {
                        case MANAGERS: {
                            GuildData data = db.getGuild(guild);
                            if (!data.isManager(user)) {
                                EmbedResponder.sendMessage(textChannel, false, trn.formatKey("error.permission.use.not_manager.title"), trn.formatKey("error.permission.use.not_manager.desc"));
                                return;
                            }
                        }
                        break;

                        case OWNER: {
                            if (user.getId().asLong() != config.getOwnerID()) {
                                EmbedResponder.sendMessage(textChannel, false, trn.formatKey("error.permission.use.not_owner.title"), trn.formatKey("error.permission.use.not_owner.desc"));
                                return;
                            }
                        }
                        break;
                    }
                }

                if(command.isExecutable())
                    command.runCommand(args, guild, textChannel, user, event);
                else
                    EmbedResponder.sendMessage(textChannel, false, trn.formatKey("error.missing.subcommand."+command.getChildTypeTrnKey()+".title"), trn.formatKey("error.missing.subcommand."+command.getChildTypeTrnKey()+".desc", CommandHelper.helpDirection(config, baseCommand)));
            }
        }
    }

    public void processMessage(MessageCreateEvent event){
        Guild guild = event.getGuild().block();

        if (guild != null) {
            if (!db.guildExists(guild))
                db.setGuild(new GuildData(guild));

            makeSureGuildHasManagers(guild);
            BaseLocalisation trn = db.getGuild(guild).getTranslator();

            MessageChannel channel = event.getMessage().getChannel().block();

            if (channel != null) {
                if (channel.getType() == Channel.Type.GUILD_TEXT) {
                    TextChannel textChannel = (TextChannel) channel;

                    Member user = event.getMessage().getAuthorAsMember().block();

                    if (user != null) {
                        String messageContent = event.getMessage().getContent();

                        try {
                            if (gateway.getSelfId().equals(user.getId())) return;

                            // Message has all necessary stuff
                            List<String> commandArgs = CommandHelper.parseCommand(messageContent, config.getPrefix());

                            if (commandArgs.size() > 0) {
                                String command = commandArgs.remove(0);

                                if (!CommandHelper.checkIfArgumentIsSane(trn, textChannel, command)) return;

                                Command commandObject;

                                commandObject = commandMap.get(command);

                                GuildData data = db.getGuild(guild);

                                if (commandObject != null) {
                                    if (data.hasModuleEnabled(commandObject.getModule())) {
                                        processCommandTree(trn, commandObject, commandObject, commandArgs, guild, textChannel, user, event);
                                    } else {
                                        EmbedResponder.sendMessage(channel, false, trn.formatKey("error.unknown.command.title"), trn.formatKey("error.unknown.command.desc"),
                                                new LinkedHashMap<>(
                                                        ImmutableMap.<String, String>builder()
                                                                .put(trn.formatKey("error.unknown.command.field"), command)
                                                                .build()));
                                    }
                                } else {
                                    if(command.isEmpty()){
                                        EmbedResponder.sendMessage(channel, false, trn.formatKey("error.wrong.command.no_command.title"), trn.formatKey("error.wrong.command.no_command.desc"));
                                    } else {
                                        EmbedResponder.sendMessage(channel, false, trn.formatKey("error.unknown.command.title"), trn.formatKey("error.unknown.command.desc"),
                                                new LinkedHashMap<>(
                                                        ImmutableMap.<String, String>builder()
                                                                .put(trn.formatKey("error.unknown.command.field"), command)
                                                                .build()));
                                    }
                                }
                            }
                        } catch (Exception e) {
                            logger.info(e.getMessage());
                            e.printStackTrace();
                            EmbedResponder.sendMessage(channel, false, trn.formatKey("error.exception.command.title"), trn.formatKey("error.exception.command.desc"));
                        }

                        PermissionSet perms = user.getBasePermissions().block();

                        if(perms != null){
                            if(perms.contains(Permission.ADMINISTRATOR))
                                logger.info("admin!");
                        }

                        logger.info("[" + guild.getName() + "] #" + textChannel.getName() + " " + user.getDisplayName() + "(" + user.getId().asLong() + "): " + messageContent);
                    }
                }
            }
        }
    }

    public void processReactionAdded(ReactionAddEvent event) {
        botEvents.on(ReactionAddedEvent.class).call(new ReactionAddedEvent(event));
    }
}
