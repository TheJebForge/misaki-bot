package com.thejebforge.misaki.slots;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class SlotOutcome {
    private SlotCombination combination;
    private List<SlotVariant> reels;
    private int paid;
    private int earned;
    private int streak;


    public String toString() {
        StringBuilder builder = new StringBuilder("[");

        for(SlotVariant variant : reels) builder.append(variant.getCharacter());
        builder.append(" - paid:").append(paid).append(" - earned:").append(earned).append(" - streak:").append(streak);

        return builder.append("]").toString();
    }
}
