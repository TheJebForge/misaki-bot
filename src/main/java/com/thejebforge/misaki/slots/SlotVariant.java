package com.thejebforge.misaki.slots;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.NonNull;

@Data
@RequiredArgsConstructor
public class SlotVariant {
    @NonNull
    private String character;
    @NonNull
    private String type;
    @NonNull
    private String fullName;
    @NonNull
    private Double chance;

    private Double minRange;
    private Double maxRange;
}
