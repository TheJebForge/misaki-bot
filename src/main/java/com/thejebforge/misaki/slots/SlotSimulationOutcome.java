package com.thejebforge.misaki.slots;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SlotSimulationOutcome {
    int betAmount;
    int runCount;
    int amountOfMoney;

    public String toString() {
        StringBuilder builder = new StringBuilder("[");

        builder.append("bet:").append(betAmount).append(" - runs:").append(runCount).append(" - money:").append(amountOfMoney);

        return builder.append("]").toString();
    }
}
