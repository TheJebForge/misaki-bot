package com.thejebforge.misaki.slots;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SlotRoller {
    private static final Logger logger = LoggerFactory.getLogger(SlotRoller.class);

    public static final List<SlotVariant> slotVariants = new ArrayList<SlotVariant>(){{
        add(new SlotVariant("C", "C", "Cherry", 1.7));
        add(new SlotVariant("1", "B", "Bar", 2.1));
        add(new SlotVariant("2", "B", "2 Bars", 1.9));
        add(new SlotVariant("3", "B", "3 Bars", 1.8));
        add(new SlotVariant("7", "7", "Seven", 1.5));
        add(new SlotVariant("D", "D", "Diamond", 0.5));
    }};

    public static final List<SlotCombination> slotCombinations = new ArrayList<SlotCombination>(){{
        add(new SlotCombination("C", 1, 3));
        add(new SlotCombination("C", 2, 5));
        add(new SlotCombination("C", 3, 10));
        add(new SlotCombination("1", 3, 20));
        add(new SlotCombination("2", 3, 30));
        add(new SlotCombination("3", 3, 40));
        add(new SlotCombination("7", 3, 80));
        add(new SlotCombination("D", 3, 2000));
    }};

    private static final int price = 5;
    private static final int reelCount = 3;

    private void evenOutVariants() {
        double sum = 0;

        // counting sum of all variants
        for(SlotVariant variant : slotVariants) sum += variant.getChance();

        // evening all of them out
        for(SlotVariant variant : slotVariants) variant.setChance(variant.getChance() / sum);

        // calculating ranges
        double min = 0;
        for(SlotVariant variant : slotVariants) {
            variant.setMinRange(min);
            min += variant.getChance();
            variant.setMaxRange(min);
        }
    }

    public SlotOutcome roll(int howMuch) {
        return roll(howMuch, null, 0);
    }

    public SlotOutcome roll(int howMuch, SlotOutcome previous) {
        return roll(howMuch, previous, 0);
    }

    public SlotOutcome roll(int howMuch, int salt) {
        return roll(howMuch, null, salt);
    }

    public SlotOutcome roll(int howMuch, SlotOutcome previous, int salt) {
        Random random = new Random(Instant.now().toEpochMilli() + salt);
        List<SlotVariant> reelResults = new ArrayList<>();

        // Rolling the reels
        for(int i = 0; i < reelCount; i++) {
            double result = random.nextDouble();

            SlotVariant selectedVariant = null;
            for(int index = 0; index < slotVariants.size(); index++) {
                SlotVariant variant = slotVariants.get(index);
                if(result >= variant.getMinRange() && result < variant.getMaxRange()) {
                    selectedVariant = variant;
                }
            }

            if(selectedVariant == null) selectedVariant = slotVariants.get(0);

            reelResults.add(selectedVariant);
        }

        SlotCombination selectedCombination = null;

        // Finding patterns
        for(SlotCombination combination : slotCombinations) {
            List<SlotVariant> candidate = new ArrayList<>(reelResults);

            if(combination.getVariant().startsWith("*")) {
                String type = combination.getVariant().substring(1);

                candidate.removeIf(slotVariant -> !slotVariant.getType().equals(type));
                if(candidate.size() == combination.getAmountOf()) selectedCombination = combination;
            } else {
                candidate.removeIf(slotVariant -> !slotVariant.getCharacter().equals(combination.getVariant()));
                if(candidate.size() == combination.getAmountOf()) selectedCombination = combination;
            }
        }

        int streak = 0;

        if(previous != null) streak = previous.getStreak();

        int reward = 0;
        if(selectedCombination != null) {
            if(previous != null && selectedCombination.equals(previous.getCombination())) streak += 1;
            else streak = 0;

            streak = Math.min(2, streak);

            reward = (int) Math.floor(selectedCombination.getReward() * ((double)howMuch / price) * ((double)(streak) / 0.39 + 1) * ((double)howMuch / 500 * 0.2 + 1 ));
        } else streak = 0;

        return new SlotOutcome(selectedCombination, reelResults, howMuch, reward, streak);
    }

    public SlotSimulationOutcome simulate(int bet, int runCount, int money) throws IOException {
        SlotOutcome previous = null;
        int currentMoney = money;

        Random random = new Random();

        //FileWriter writer = new FileWriter("simulations/"+Instant.now().toEpochMilli()+".csv");

        int runs = 0;
        for(int i = 0; i < runCount; i++) {
            //logger.info("i - " + i + " got:" + currentMoney + " betting:"+bet);
            if(currentMoney < bet) {
                runs = i;
                break;
            }

            SlotOutcome roll = roll(bet, previous, i * random.nextInt(1000));
            currentMoney -= roll.getPaid();
            currentMoney += roll.getEarned();

            //writer.write(i+","+currentMoney+","+roll.getStreak()+","+roll.getEarned()+","+roll.toString()+"\n");

            //logger.info(roll.toString());

            previous = roll;
            runs++;
        }

        //writer.close();

        return new SlotSimulationOutcome(bet, runs, currentMoney);
    }

    public double simulateWithAverage(int bet, int startingMoney, int amount, int runCount) throws IOException {
        int sum = 0;
        for(int i = 0; i < amount; i++) {
            SlotSimulationOutcome outcome = simulate(bet, runCount, startingMoney);
            sum += outcome.amountOfMoney;
        }
        return (double) sum / amount;
    }

    public SlotRoller() {
        evenOutVariants();
    }
}
