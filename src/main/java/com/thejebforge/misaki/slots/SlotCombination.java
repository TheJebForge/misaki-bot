package com.thejebforge.misaki.slots;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SlotCombination {
    private String variant;
    private int amountOf;
    private int reward;
}
