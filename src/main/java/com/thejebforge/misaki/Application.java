package com.thejebforge.misaki;

import com.thejebforge.misaki.runtime.MainBotProcess;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class Application extends SpringBootServletInitializer {
    static MainBotProcess process;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
