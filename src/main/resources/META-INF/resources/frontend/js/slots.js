var machine = [];

window.setupReels = () => {
    for(let i = 0; i < 3; i++) {
        machine.push(new SlotMachine(document.getElementById(`reel${i}`), { active: 0, delay: 500, inViewport: false}));
    }
}

window.spin = (results) => {
    for(let i = 0; i < results.length; i++) {
        machine[i].randomize = () => { return results[i]; };
        machine[i].shuffle(2 + i, i == (results.length - 1) ? () => { document.getElementById("reelsView").$server.spinComplete(); } : null);
    }
}